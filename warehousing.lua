local rundir = arg[0]:gsub("warehousing.lua", "")
print(rundir)
package.path = package.path .. ';' .. arg[0]:gsub("warehousing", "?")
local inifile = require("LIP")
dofile(rundir .. "Serializer.lua")

-- Path to Eagle dynamics Scripts/Database
db_path = arg[1] .. "/"

-- "generate" to create a sample config, "update" to actually write a new warehouse file
local action = arg[2]

if action == 'update' then
    -- The warehouse file to modify
    dofile(arg[3])
    -- location of the config to use.
    current_config_path = arg[4]
    warehouse_output = arg[5]
elseif action == 'generate' then
    -- The location to save the generated config
    config_output = arg[3]
    -- location of current config if you want to reuse values from it.
    -- Useful if ED updates the DB and you want to generate a new config without losing existing values
    current_config_path = arg[4]
end


db = {}
wstype_containers = {}
wstype_bombs	  = {}
wstype_missiles	  = {}

db = {};
db.objectIconByType = {};

dofile(db_path.."wsTypes.lua");
dofile(db_path.."Weapons/weapons_table.lua")
dofile(db_path.."db_weapons.lua");

local weapon_map = {}
local eqp_by_type = {}
local current_config = nil

if current_config_path then
    current_config = inifile.load(current_config_path)
end

for _, data in pairs(db.Weapons.Categories) do
    if not eqp_by_type[data.DisplayName] then eqp_by_type[data.DisplayName] = {} end
    for i, weapon_data in ipairs(data.Launchers) do
        local value = 200
        if current_config and current_config[data.DisplayName] and current_config[data.DisplayName][weapon_data.displayName] then
            value = current_config[data.DisplayName][weapon_data.displayName]
        end
        eqp_by_type[data.DisplayName][weapon_data.displayName] = value
        weapon_map[weapon_data.displayName] = weapon_data.attribute
    end
end

if action == 'generate' then
    print("Making new config")
    inifile.save(config_output, eqp_by_type)
end

if action == 'update' then
    print("Updating a warehouse file")
    for aid, items in pairs(warehouses.airports) do
        items.unlimitedMunitions = false
        items.weapons = {}
        for type, weapon in pairs(eqp_by_type) do
            for weapon_name, weapon_amount in pairs(weapon) do
                local attribute = weapon_map[weapon_name]
                table.insert(items.weapons, {['wsType'] = attribute, ['initialAmount'] = weapon_amount})
            end
        end
    end

    local wf = io.open(warehouse_output, "w")
    io.output(wf)
    io.write(mist.utils.serialize("warehouses", warehouses))
    wf:close()
end
