{ {
    CLSID = "{839A9F02-9F52-4a61-9E40-7A4A59975703}",
    DisplayName = "BOMBS",
    Launchers = { {
        CLSID = "{35B698AC-9FEF-4EC4-AD29-484A0085F62B}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "BETAB-500"
          } },
        Picture = "betab500.png",
        Weight = 430,
        attribute = { 4, 5, 37, 3 },
        displayName = "BetAB-500"
      }, {
        CLSID = "{BD289E34-DF84-4C5E-9220-4B14C346E79D}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "BETAB-500SP"
          } },
        Picture = "betab500shp.png",
        Weight = 424,
        attribute = { 4, 5, 37, 4 },
        displayName = "BetAB-500ShP"
      }, {
        CLSID = "{FB3CE165-BF07-4979-887C-92B87F13276B}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "FAB-100"
          } },
        Picture = "FAB100.png",
        Weight = 100,
        attribute = { 4, 5, 9, 5 },
        displayName = "FAB-100"
      }, {
        CLSID = "{0511E528-EA28-4caf-A212-00D1408DF10A}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "SAB-100"
          } },
        Picture = "sab100.png",
        Weight = 100,
        attribute = { 4, 5, 49, 63 },
        displayName = "SAB-100"
      }, {
        CLSID = "{3C612111-C7AD-476E-8A8E-2485812F4E5C}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "FAB-250-N1"
          } },
        Picture = "FAB250.png",
        Weight = 250,
        attribute = { 4, 5, 9, 6 },
        displayName = "FAB-250"
      }, {
        CLSID = "{37DCC01E-9E02-432F-B61D-10C166CA2798}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "FAB-500-N3"
          } },
        Picture = "FAB500.png",
        Weight = 506,
        attribute = { 4, 5, 9, 7 },
        displayName = "FAB-500 M62"
      }, {
        CLSID = "{40AA4ABE-D6EB-4CD6-AEFE-A1A0477B24AB}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "FAB-1500"
          } },
        Picture = "FAB1500.png",
        Weight = 1392,
        attribute = { 4, 5, 9, 9 },
        displayName = "FAB-1500 M54"
      }, {
        CLSID = "{BA565F89-2373-4A84-9502-A0E017D3A44A}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "KAB-500"
          } },
        Picture = "KAB500.png",
        Weight = 534,
        attribute = { 4, 5, 36, 11 },
        displayName = "KAB-500L"
      }, {
        CLSID = "{E2C426E3-8B10-4E09-B733-9CDC26520F48}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "KAB-500T"
          } },
        Picture = "kab500lpr.png",
        Weight = 560,
        attribute = { 4, 5, 36, 12 },
        displayName = "KAB-500kr"
      }, {
        CLSID = "{39821727-F6E2-45B3-B1F0-490CC8921D1E}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "KAB-1500"
          } },
        Picture = "KAB1500.png",
        Weight = 1560,
        attribute = { 4, 5, 36, 14 },
        displayName = "KAB-1500L"
      }, {
        CLSID = "{4203753F-8198-4E85-9924-6F8FF679F9FF}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "RBK_250_PTAB_25M_cassette"
          } },
        Picture = "RBK250.png",
        Weight = 273,
        attribute = { 4, 5, 38, 18 },
        displayName = "RBK-250 PTAB-2.5M"
      }, {
        CLSID = "{D5435F26-F120-4FA3-9867-34ACE562EF1B}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "RBK_500_255_PTAB_10_5_cassette"
          } },
        Picture = "RBK_500_255_PTAB_10_5_cassette.png",
        Weight = 427,
        attribute = { 4, 5, 38, 20 },
        displayName = "RBK-500-255 PTAB-10-5"
      }, {
        CLSID = "{90321C8E-7ED1-47D4-A160-E074D5ABD902}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "MK-81"
          } },
        Picture = "FAB100.png",
        Weight = 118,
        attribute = { 4, 5, 9, 30 },
        displayName = "Mk-81"
      }, {
        CLSID = "{BCE4E030-38E9-423E-98ED-24BE3DA87C32}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "MK-82"
          } },
        Picture = "mk82.png",
        Weight = 241,
        attribute = { 4, 5, 9, 31 },
        displayName = "Mk-82"
      }, {
        CLSID = "{7A44FF09-527C-4B7E-B42B-3F111CFE50FB}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "MK-83"
          } },
        Picture = "mk83.png",
        Weight = 447,
        attribute = { 4, 5, 9, 32 },
        displayName = "Mk-83"
      }, {
        CLSID = "{AB8B8299-F1CC-4359-89B5-2172E0CF4A5A}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "MK-84"
          } },
        Picture = "mk84.png",
        Weight = 894,
        attribute = { 4, 5, 9, 33 },
        displayName = "Mk-84"
      }, {
        CLSID = "{00F5DAC4-0466-4122-998F-B1A298E34113}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "M117"
          } },
        Picture = "KMGU2.png",
        Weight = 340,
        attribute = { 4, 5, 9, 34 },
        displayName = "M117"
      }, {
        CLSID = "{08164777-5E9C-4B08-B48E-5AA7AFB246E2}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "T-BL-755"
          } },
        Picture = "BL755.png",
        Weight = 264,
        attribute = { 4, 5, 38, 23 },
        displayName = "BL755"
      }, {
        CLSID = "{752B9781-F962-11d5-9190-00A0249B6F00}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "DURANDAL"
          } },
        Picture = "blu107.png",
        Weight = 185,
        attribute = { 4, 5, 37, 62 },
        displayName = "BLU-107"
      }, {
        CLSID = "{CBU_103}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "CBU-97"
          } },
        Picture = "CBU.png",
        Weight = 430,
        attribute = { 4, 5, 38, 88 },
        displayName = "CBU-103"
      }, {
        CLSID = "{CBU_105}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "CBU-97"
          } },
        Picture = "CBU.png",
        Weight = 417,
        attribute = { 4, 5, 38, 87 },
        displayName = "CBU-105"
      }, {
        CLSID = "{5335D97A-35A5-4643-9D9B-026C75961E52}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "CBU-97"
          } },
        Picture = "CBU.png",
        Weight = 417,
        attribute = { 4, 5, 38, 35 },
        displayName = "CBU-97"
      }, {
        CLSID = "{51F9AAE5-964F-4D21-83FB-502E3BFE5F8A}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "GBU-10"
          } },
        Picture = "GBU10.png",
        Required = { "{6C0D552F-570B-42ff-9F6D-F10D9C1D4E1C}", "{CAAC1CFD-6745-416B-AFA4-CB57414856D0}" },
        Weight = 1162,
        attribute = { 4, 5, 36, 36 },
        displayName = "GBU-10"
      }, {
        CLSID = "{DB769D48-67D7-42ED-A2BE-108D566C8B1E}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "GBU-12"
          } },
        Picture = "GBU12.png",
        Required = { "{6C0D552F-570B-42ff-9F6D-F10D9C1D4E1C}", "{CAAC1CFD-6745-416B-AFA4-CB57414856D0}" },
        Weight = 275,
        attribute = { 4, 5, 36, 38 },
        displayName = "GBU-12"
      }, {
        CLSID = "{0D33DDAE-524F-4A4E-B5B8-621754FE3ADE}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "GBU-16"
          } },
        Picture = "GBU16.png",
        Required = { "{6C0D552F-570B-42ff-9F6D-F10D9C1D4E1C}" },
        Weight = 564,
        attribute = { 4, 5, 36, 39 },
        displayName = "GBU-16"
      }, {
        CLSID = "{FAAFA032-8996-42BF-ADC4-8E2C86BCE536}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "GBU-15"
          } },
        Picture = "GBU16.png",
        Weight = 1140,
        attribute = { 4, 5, 36, 42 },
        displayName = "GBU-15"
      }, {
        CLSID = "{34759BBC-AF1E-4AEE-A581-498FF7A6EBCE}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "GBU-24"
          } },
        Picture = "GBU27.png",
        Required = { "{6C0D552F-570B-42ff-9F6D-F10D9C1D4E1C}" },
        Weight = 900,
        attribute = { 4, 5, 36, 41 },
        displayName = "GBU-24"
      }, {
        CLSID = "{EF0A9419-01D6-473B-99A3-BEBDB923B14D}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "GBU-27"
          } },
        Picture = "GBU27.png",
        Weight = 1200,
        attribute = { 4, 5, 36, 43 },
        displayName = "GBU-27"
      }, {
        CLSID = "{F06B775B-FC70-44B5-8A9F-5B5E2EB839C7}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "GBU-28"
          } },
        Picture = "GBU27.png",
        Weight = 2130,
        attribute = { 4, 5, 36, 48 },
        displayName = "GBU-28"
      }, {
        CLSID = "{GBU-31}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "GBU-31"
          } },
        Picture = "GBU31.png",
        Weight = 894,
        attribute = { 4, 5, 36, 85 },
        displayName = "GBU-31"
      }, {
        CLSID = "{GBU-31V3B}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "GBU31_V_3B_BLU109"
          } },
        Picture = "GBU-31V3B.png",
        Weight = 981,
        attribute = { 4, 5, 36, 92 },
        displayName = "GBU-31(V)3/B"
      }, {
        CLSID = "{GBU-38}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "GBU-38"
          } },
        Picture = "GBU38.png",
        Weight = 241,
        attribute = { 4, 5, 36, 86 },
        displayName = "GBU-38"
      }, {
        CLSID = "{BDU-50LD}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "BDU-50LD"
          } },
        Picture = "BDU-50LD.png",
        Weight = 232,
        attribute = { 4, 5, 9, 70 },
        displayName = "BDU-50LD"
      }, {
        CLSID = "{BDU-50HD}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "BDU-50HD"
          } },
        Picture = "BDU-50HD.png",
        Weight = 232,
        attribute = { 4, 5, 9, 71 },
        displayName = "BDU-50HD"
      }, {
        CLSID = "{BDU-50LGB}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "BDU-50LGB"
          } },
        Picture = "gbu12.png",
        Weight = 280,
        attribute = { 4, 5, 36, 72 },
        displayName = "BDU-50LGB"
      }, {
        CLSID = "{BDU-33}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "BDU-33"
          } },
        Picture = "bdu-33.png",
        Weight = 11,
        attribute = { 4, 5, 9, 69 },
        displayName = "BDU-33"
      }, {
        CLSID = "BRU-42_3*BDU-33",
        Count = 3,
        Cx_pil = 0.002,
        Elements = { {
            ShapeName = "BRU-42_LS"
          }, {
            DrawArgs = <1>{ { 1, 1 }, { 2, 1 } },
            ShapeName = "BDU-33",
            connector_name = "Point01"
          }, {
            DrawArgs = <table 1>,
            ShapeName = "BDU-33",
            connector_name = "Point02"
          }, {
            DrawArgs = <table 1>,
            ShapeName = "BDU-33",
            connector_name = "Point03"
          } },
        Picture = "BDU-33.png",
        Weight = 98,
        attribute = { 4, 5, 32, 114 },
        displayName = "3 BDU-33",
        wsTypeOfWeapon = { 4, 5, 9, 69 }
      }, {
        CLSID = "{CBU-87}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "CBU-97"
          } },
        Picture = "CBU.png",
        Weight = 430,
        attribute = { 4, 5, 38, 77 },
        displayName = "CBU-87"
      }, {
        CLSID = "{ADD3FAE1-EBF6-4EF9-8EFC-B36B5DDF1E6B}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "ROCKEYE"
          } },
        Picture = "Mk20.png",
        Required = { "{6C0D552F-570B-42ff-9F6D-F10D9C1D4E1C}" },
        Weight = 222,
        attribute = { 4, 5, 38, 45 },
        displayName = "Mk-20"
      }, {
        CLSID = "{C40A1E3A-DD05-40D9-85A4-217729E37FAE}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "AGM-62"
          } },
        Picture = "agm119.png",
        Required = { "{6C0D552F-570B-42ff-9F6D-F10D9C1D4E1C}" },
        Weight = 1088,
        attribute = { 4, 5, 36, 47 },
        displayName = "AGM-62"
      }, {
        CLSID = "{F99BEC1A-869D-4AC7-9730-FBA0E3B1F5FC}",
        Count = 6,
        Cx_pil = 0.00158,
        Elements = { <2>{
            ShapeName = "mbd3-u6-68"
          }, {
            Position = <3>{ 0.898147, -0.124806, 0.159743 },
            Rotation = <4>{ -40, 0, 0 },
            ShapeName = "FAB-100"
          }, {
            Position = <5>{ 0.898147, -0.122812, -0.160811 },
            Rotation = <6>{ 40, 0, 0 },
            ShapeName = "FAB-100"
          }, {
            Position = <7>{ -0.986493, -0.124806, 0.159743 },
            Rotation = <8>{ -40, 0, 0 },
            ShapeName = "FAB-100"
          }, {
            Position = <9>{ -0.986493, -0.122812, -0.160811 },
            Rotation = <10>{ 40, 0, 0 },
            ShapeName = "FAB-100"
          }, {
            Position = <11>{ 0.898015, -0.360123, 0 },
            ShapeName = "FAB-100"
          }, {
            Position = <12>{ -0.986625, -0.360123, 0 },
            ShapeName = "FAB-100"
          } },
        Picture = "FAB100.png",
        Weight = 660,
        attribute = { 4, 5, 32, 1 },
        displayName = "MER*6 FAB-100",
        wsTypeOfWeapon = { 4, 5, 9, 5 }
      }, {
        CLSID = "{53BE25A4-C86C-4571-9BC0-47D668349595}",
        Count = 6,
        Cx_pil = 0.00444,
        Elements = { <table 2>, {
            Position = <table 3>,
            Rotation = <table 4>,
            ShapeName = "FAB-250-N1"
          }, {
            Position = <table 5>,
            Rotation = <table 6>,
            ShapeName = "FAB-250-N1"
          }, {
            Position = <table 7>,
            Rotation = <table 8>,
            ShapeName = "FAB-250-N1"
          }, {
            Position = <table 9>,
            Rotation = <table 10>,
            ShapeName = "FAB-250-N1"
          }, {
            Position = <table 11>,
            ShapeName = "FAB-250-N1"
          }, {
            Position = <table 12>,
            ShapeName = "FAB-250-N1"
          } },
        Picture = "FAB250.png",
        Weight = 1560,
        attribute = { 4, 5, 32, 2 },
        displayName = "MER*6 FAB-250",
        wsTypeOfWeapon = { 4, 5, 9, 6 }
      }, {
        CLSID = "{E659C4BE-2CD8-4472-8C08-3F28ACB61A8A}",
        Count = 2,
        Cx_pil = 0.0028,
        Elements = { <table 2>, {
            Position = <table 11>,
            ShapeName = "FAB-250-N1"
          }, {
            Position = <table 12>,
            ShapeName = "FAB-250-N1"
          } },
        Picture = "FAB250.png",
        Weight = 550,
        attribute = { 4, 5, 32, 68 },
        displayName = "MER 6*2 FAB-250",
        wsTypeOfWeapon = { 4, 5, 9, 6 }
      }, {
        CLSID = "{3E35F8C1-052D-11d6-9191-00A0249B6F00}",
        Count = 4,
        Cx_pil = 0.005,
        Elements = { <table 2>, {
            Position = <table 3>,
            Rotation = <table 4>,
            ShapeName = "FAB-250-N1"
          }, {
            Position = <table 5>,
            Rotation = <table 6>,
            ShapeName = "FAB-250-N1"
          }, {
            Position = <table 7>,
            Rotation = <table 8>,
            ShapeName = "FAB-250-N1"
          }, {
            Position = <table 9>,
            Rotation = <table 10>,
            ShapeName = "FAB-250-N1"
          } },
        Picture = "FAB250.png",
        Weight = 1060,
        attribute = { 4, 5, 32, 84 },
        displayName = "MER 6*4 FAB-250",
        wsTypeOfWeapon = { 4, 5, 9, 6 }
      }, {
        CLSID = "{FA673F4C-D9E4-4993-AA7A-019A92F3C005}",
        Count = 6,
        Cx_pil = 0.00788,
        Elements = { <13>{
            ShapeName = "TU-22M3-MBD"
          }, {
            Position = <14>{ 0.75, -0.39, 0.14 },
            Rotation = <15>{ -40, 0, 0 },
            ShapeName = "FAB-500-N3"
          }, {
            Position = <16>{ 0.75, -0.39, -0.14 },
            Rotation = <17>{ 40, 0, 0 },
            ShapeName = "FAB-500-N3"
          }, {
            Position = <18>{ -3, -0.39, 0.14 },
            Rotation = <19>{ -40, 0, 0 },
            ShapeName = "FAB-500-N3"
          }, {
            Position = <20>{ -3, -0.39, -0.14 },
            Rotation = <21>{ 40, 0, 0 },
            ShapeName = "FAB-500-N3"
          }, {
            Position = <22>{ 0.75, -0.6, 0 },
            Rotation = <23>{},
            ShapeName = "FAB-500-N3"
          }, {
            Position = <24>{ -3, -0.6, 0 },
            Rotation = <25>{},
            ShapeName = "FAB-500-N3"
          } },
        Picture = "FAB500.png",
        Weight = 3060,
        attribute = { 4, 5, 32, 3 },
        displayName = "MER*6 FAB-500",
        wsTypeOfWeapon = { 4, 5, 9, 7 }
      }, {
        CLSID = "{F503C276-FE15-4C54-B310-17B50B735A84}",
        Count = 6,
        Cx_pil = 0.00788,
        Elements = { <table 13>, {
            Position = <table 14>,
            Rotation = <table 15>,
            ShapeName = "RBK_500_255_PTAB_10_5_cassette"
          }, {
            Position = <table 16>,
            Rotation = <table 17>,
            ShapeName = "RBK_500_255_PTAB_10_5_cassette"
          }, {
            Position = <table 18>,
            Rotation = <table 19>,
            ShapeName = "RBK_500_255_PTAB_10_5_cassette"
          }, {
            Position = <table 20>,
            Rotation = <table 21>,
            ShapeName = "RBK_500_255_PTAB_10_5_cassette"
          }, {
            Position = <table 22>,
            Rotation = <table 23>,
            ShapeName = "RBK_500_255_PTAB_10_5_cassette"
          }, {
            Position = <table 24>,
            Rotation = <table 25>,
            ShapeName = "RBK_500_255_PTAB_10_5_cassette"
          } },
        Picture = "RBK_500_255_PTAB_10_5_cassette.png",
        Weight = 3060,
        attribute = { 4, 5, 32, 43 },
        displayName = "MER*6 RBK-500-255 PTAB-10-5",
        wsTypeOfWeapon = { 4, 5, 38, 20 }
      }, {
        CLSID = "{6CDB6B36-7165-47D0-889F-6625FB333561}",
        Count = 6,
        Cx_pil = 0.007,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "MBD"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { -1.242, -0.415, 0 },
            ShapeName = "M117"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 1.19, -0.415, 0 },
            ShapeName = "M117"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, -1 } },
            Position = { -1.242, -0.266, 0.293 },
            ShapeName = "M117"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, 1 } },
            Position = { -1.242, -0.266, -0.293 },
            ShapeName = "M117"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, -1 } },
            Position = { 1.19, -0.266, 0.293 },
            ShapeName = "M117"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, 1 } },
            Position = { 1.19, -0.266, -0.293 },
            ShapeName = "M117"
          } },
        Picture = "RBK250.png",
        Weight = 2100,
        attribute = { 4, 5, 32, 12 },
        displayName = "MER*6 M117AB",
        wsTypeOfWeapon = { 4, 5, 9, 34 }
      }, {
        CLSID = "{1C97B4A0-AA3B-43A8-8EE7-D11071457185}",
        Count = 6,
        Cx_pil = 0.00544,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "MBD"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { -1.242, -0.415, 0 },
            ShapeName = "MK-82"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 1.19, -0.415, 0 },
            ShapeName = "MK-82"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, -1 } },
            Position = { -1.242, -0.266, 0.293 },
            ShapeName = "MK-82"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, 1 } },
            Position = { -1.242, -0.266, -0.293 },
            ShapeName = "MK-82"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, -1 } },
            Position = { 1.19, -0.266, 0.293 },
            ShapeName = "MK-82"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, 1 } },
            Position = { 1.19, -0.266, -0.293 },
            ShapeName = "MK-82"
          } },
        Picture = "mk82.png",
        Weight = 1506,
        attribute = { 4, 5, 32, 14 },
        displayName = "MER*6 Mk-82",
        wsTypeOfWeapon = { 4, 5, 9, 31 }
      }, {
        CLSID = "{3C7CD675-7D39-41C5-8735-0F4F537818A8}",
        Count = 6,
        Cx_pil = 0.0049,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "MBD"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { -1.242, -0.415, 0 },
            ShapeName = "ROCKEYE"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 1.19, -0.415, 0 },
            ShapeName = "ROCKEYE"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, -1 } },
            Position = { -1.242, -0.266, 0.293 },
            ShapeName = "ROCKEYE"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, 1 } },
            Position = { -1.242, -0.266, -0.293 },
            ShapeName = "ROCKEYE"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, -1 } },
            Position = { 1.19, -0.266, 0.293 },
            ShapeName = "ROCKEYE"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, 1 } },
            Position = { 1.19, -0.266, -0.293 },
            ShapeName = "ROCKEYE"
          } },
        Picture = "rockeye.png",
        Weight = 1392,
        attribute = { 4, 5, 32, 15 },
        displayName = "MER*6 Mk-20 Rockeye",
        wsTypeOfWeapon = { 4, 5, 38, 45 }
      }, {
        CLSID = "{752B9782-F962-11d5-9190-00A0249B6F00}",
        Count = 6,
        Cx_pil = 0.00988,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "MBD"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { -1.242, -0.415, 0 },
            ShapeName = "DURANDAL"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 1.19, -0.415, 0 },
            ShapeName = "DURANDAL"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, -1 } },
            Position = { -1.242, -0.266, 0.293 },
            ShapeName = "DURANDAL"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, 1 } },
            Position = { -1.242, -0.266, -0.293 },
            ShapeName = "DURANDAL"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, -1 } },
            Position = { 1.19, -0.266, 0.293 },
            ShapeName = "DURANDAL"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, 1 } },
            Position = { 1.19, -0.266, -0.293 },
            ShapeName = "DURANDAL"
          } },
        Picture = "blu107.png",
        Weight = 1800,
        attribute = { 4, 5, 32, 75 },
        displayName = "MER*6 BLU-107",
        wsTypeOfWeapon = { 4, 5, 37, 62 }
      }, {
        CLSID = "{A1E85991-B58E-4E92-AE91-DED6DC85B2E7}",
        Count = 3,
        Cx_pil = 0.00444,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "MBD-3"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, -0.415, 0 },
            ShapeName = "FAB-500-N3"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, -1 } },
            Position = { 0, -0.266, 0.293 },
            ShapeName = "FAB-500-N3"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, 1 } },
            Position = { 0, -0.266, -0.293 },
            ShapeName = "FAB-500-N3"
          } },
        Picture = "FAB500.png",
        Weight = 1560,
        attribute = { 4, 5, 32, 18 },
        displayName = "MER-3*3 FAB-500",
        wsTypeOfWeapon = { 4, 5, 9, 7 }
      }, {
        CLSID = "{005E70F5-C3EA-4E95-A148-C1044C42D845}",
        Count = 3,
        Cx_pil = 0.00508,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "MBD-3"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, -0.415, 0 },
            ShapeName = "BETAB-500"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, -1 } },
            Position = { 0, -0.266, 0.293 },
            ShapeName = "BETAB-500"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, 1 } },
            Position = { 0, -0.266, -0.293 },
            ShapeName = "BETAB-500"
          } },
        Picture = "betab500.png",
        Weight = 1566,
        attribute = { 4, 5, 32, 72 },
        displayName = "MER-3*3 BetAB-500",
        wsTypeOfWeapon = { 4, 5, 37, 3 }
      }, {
        CLSID = "{EAD9B2C1-F3BA-4A7B-A2A5-84E2AF8A1975}",
        Count = 3,
        Cx_pil = 0.00322,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "MBD-3"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, -0.415, 0 },
            ShapeName = "RBK_250_PTAB_25M_cassette"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, -1 } },
            Position = { 0, -0.266, 0.293 },
            ShapeName = "RBK_250_PTAB_25M_cassette"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, 1 } },
            Position = { 0, -0.266, -0.293 },
            ShapeName = "RBK_250_PTAB_25M_cassette"
          } },
        Picture = "RBK250.png",
        Weight = 885,
        attribute = { 4, 5, 32, 22 },
        displayName = "MER-3*3 RBK-250 PTAB-2.5M",
        wsTypeOfWeapon = { 4, 5, 38, 18 }
      }, {
        CLSID = "{CEE04106-B9AA-46B4-9CD1-CD3FDCF0CE78}",
        Count = 3,
        Cx_pil = 0.00158,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "MBD-3"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, -0.415, 0 },
            ShapeName = "FAB-100"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, -1 } },
            Position = { 0, -0.266, 0.293 },
            ShapeName = "FAB-100"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, 1 } },
            Position = { 0, -0.266, -0.293 },
            ShapeName = "FAB-100"
          } },
        Picture = "FAB100.png",
        Weight = 360,
        attribute = { 4, 5, 32, 16 },
        displayName = "MER-3*3 FAB-100",
        wsTypeOfWeapon = { 4, 5, 9, 5 }
      }, {
        CLSID = "{D109EE9C-A1B7-4F1C-8D87-631C293A1D26}",
        Count = 3,
        Cx_pil = 0.00322,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "MBD-3"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, -0.415, 0 },
            ShapeName = "FAB-250-N1"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, -1 } },
            Position = { 0, -0.266, 0.293 },
            ShapeName = "FAB-250-N1"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, 1 } },
            Position = { 0, -0.266, -0.293 },
            ShapeName = "FAB-250-N1"
          } },
        Picture = "FAB250.png",
        Weight = 810,
        attribute = { 4, 5, 32, 17 },
        displayName = "MER-3*3 FAB-250",
        wsTypeOfWeapon = { 4, 5, 9, 6 }
      }, {
        CLSID = "{919CE839-9390-4629-BAF7-229DE19B8523}",
        Count = 3,
        Cx_pil = 0.00544,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "MBD-3"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, -0.415, 0 },
            ShapeName = "RBK_500_255_PTAB_10_5_cassette"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, -1 } },
            Position = { 0, -0.266, 0.293 },
            ShapeName = "RBK_500_255_PTAB_10_5_cassette"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, 1 } },
            Position = { 0, -0.266, -0.293 },
            ShapeName = "RBK_500_255_PTAB_10_5_cassette"
          } },
        Picture = "RBK_500_255_PTAB_10_5_cassette.png",
        Weight = 1560,
        attribute = { 4, 5, 32, 23 },
        displayName = "MER-3*3 RBK-500-255 PTAB-10-5",
        wsTypeOfWeapon = { 4, 5, 38, 20 }
      }, {
        CLSID = "{82F90BEC-0E2E-4CE5-A66E-1E4ADA2B5D1E}",
        Count = 3,
        Cx_pil = 0.004,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "MBD-3"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, -0.415, 0 },
            ShapeName = "M117"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, -1 } },
            Position = { 0, -0.266, 0.293 },
            ShapeName = "M117"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, 1 } },
            Position = { 0, -0.266, -0.293 },
            ShapeName = "M117"
          } },
        Picture = "RBK250.png",
        Weight = 1060,
        attribute = { 4, 5, 32, 27 },
        displayName = "MER-3*3 M117AB",
        wsTypeOfWeapon = { 4, 5, 9, 34 }
      }, {
        CLSID = "{7B34E0BB-E427-4C2A-A61A-8407CE18B54D}",
        Count = 3,
        Cx_pil = 0.00205,
        Elements = { {
            ShapeName = "BRU-42_LS"
          }, {
            DrawArgs = <table 1>,
            ShapeName = "MK-81",
            connector_name = "Point01"
          }, {
            DrawArgs = <table 1>,
            ShapeName = "MK-81",
            connector_name = "Point02"
          }, {
            DrawArgs = <table 1>,
            ShapeName = "MK-81",
            connector_name = "Point03"
          } },
        Picture = "FAB250.png",
        Weight = 414,
        attribute = { 4, 5, 32, 28 },
        displayName = "MER-3*3 Mk-81",
        wsTypeOfWeapon = { 4, 5, 9, 30 }
      }, {
        CLSID = "{60CC734F-0AFA-4E2E-82B8-93B941AB11CF}",
        Count = 3,
        Cx_pil = 0.00322,
        Elements = { {
            ShapeName = "BRU-42_LS"
          }, {
            DrawArgs = <table 1>,
            ShapeName = "MK-82",
            connector_name = "Point01"
          }, {
            DrawArgs = <table 1>,
            ShapeName = "MK-82",
            connector_name = "Point02"
          }, {
            DrawArgs = <table 1>,
            ShapeName = "MK-82",
            connector_name = "Point03"
          } },
        Picture = "mk82.png",
        Weight = 783,
        attribute = { 4, 5, 32, 29 },
        displayName = "3 Mk-82",
        wsTypeOfWeapon = { 4, 5, 9, 31 }
      }, {
        CLSID = "{BRU-42_3*Mk-82AIR}",
        Count = 3,
        Cx_pil = 0.00075,
        Elements = { {
            ShapeName = "BRU-42_LS"
          }, {
            DrawArgs = <table 1>,
            ShapeName = "MK-82AIR",
            connector_name = "Point01"
          }, {
            DrawArgs = <table 1>,
            ShapeName = "MK-82AIR",
            connector_name = "Point02"
          }, {
            DrawArgs = <table 1>,
            ShapeName = "MK-82AIR",
            connector_name = "Point03"
          } },
        Picture = "mk82.png",
        Weight = 783,
        attribute = { 4, 5, 32, 131 },
        displayName = "3 Mk-82AIR",
        wsTypeOfWeapon = { 4, 5, 9, 75 }
      }, {
        CLSID = "{B83CB620-5BBE-4BEA-910C-EB605A327EF9}",
        Count = 3,
        Cx_pil = 0.00295,
        Elements = { {
            ShapeName = "BRU-42_LS"
          }, {
            DrawArgs = <table 1>,
            ShapeName = "ROCKEYE",
            connector_name = "Point01"
          }, {
            DrawArgs = <table 1>,
            ShapeName = "ROCKEYE",
            connector_name = "Point02"
          }, {
            DrawArgs = <table 1>,
            ShapeName = "ROCKEYE",
            connector_name = "Point03"
          } },
        Picture = "Mk20.png",
        Weight = 726,
        attribute = { 4, 5, 32, 30 },
        displayName = "3 Mk-20 Rockeye",
        wsTypeOfWeapon = { 4, 5, 38, 45 }
      }, {
        CLSID = "{88D49E04-78DF-4F08-B47E-B81247A9E3C5}",
        Count = 3,
        Cx_pil = 0.00508,
        Elements = { {
            ShapeName = "BRU-42_LS"
          }, {
            DrawArgs = <table 1>,
            ShapeName = "GBU-16",
            connector_name = "Point01"
          }, {
            DrawArgs = <table 1>,
            ShapeName = "GBU-16",
            connector_name = "Point02"
          }, {
            DrawArgs = <table 1>,
            ShapeName = "GBU-16",
            connector_name = "Point03"
          } },
        Picture = "GBU16.png",
        Weight = 666,
        attribute = { 4, 5, 32, 31 },
        displayName = "3 GBU-16",
        wsTypeOfWeapon = { 4, 5, 36, 39 }
      }, {
        CLSID = "{5A1AC2B4-CA4B-4D09-A1AF-AC52FBC4B60B}",
        Count = 4,
        Cx_pil = 0.001,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "MBD-2-67U"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.701, -0.088, -0.107 },
            Rotation = { 35, 0, 0 },
            ShapeName = "FAB-100"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { -0.595, -0.088, -0.107 },
            Rotation = { 35, 0, 0 },
            ShapeName = "FAB-100"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.701, -0.088, 0.107 },
            Rotation = { -35, 0, 0 },
            ShapeName = "FAB-100"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { -0.595, -0.088, 0.107 },
            Rotation = { -35, 0, 0 },
            ShapeName = "FAB-100"
          } },
        Picture = "FAB100.png",
        Weight = 465,
        attribute = { 4, 5, 32, 32 },
        displayName = "MBD-2-67U - 4 FAB-100",
        wsTypeOfWeapon = { 4, 5, 9, 5 }
      }, {
        CLSID = "{5F1C54C0-0ABD-4868-A883-B52FF9FCB422}",
        Count = 9,
        Cx_pil = 0.0027,
        Elements = { <table 13>, {
            Position = <table 14>,
            Rotation = <table 15>,
            ShapeName = "FAB-100"
          }, {
            Position = <table 16>,
            Rotation = <table 17>,
            ShapeName = "FAB-100"
          }, {
            Position = <26>{ -1.25, -0.39, 0.14 },
            Rotation = <27>{ -40, 0, 0 },
            ShapeName = "FAB-100"
          }, {
            Position = <28>{ -1.25, -0.39, -0.14 },
            Rotation = <29>{ 40, 0, 0 },
            ShapeName = "FAB-100"
          }, {
            Position = <table 18>,
            Rotation = <table 19>,
            ShapeName = "FAB-100"
          }, {
            Position = <table 20>,
            Rotation = <table 21>,
            ShapeName = "FAB-100"
          }, {
            Position = <table 22>,
            Rotation = <table 23>,
            ShapeName = "FAB-100"
          }, {
            Position = <30>{ -1.25, -0.6, 0 },
            Rotation = <31>{},
            ShapeName = "FAB-100"
          }, {
            Position = <table 24>,
            Rotation = <table 25>,
            ShapeName = "FAB-100"
          } },
        Picture = "FAB100.png",
        Weight = 960,
        attribute = { 4, 5, 32, 34 },
        displayName = "MER*9 FAB-100",
        wsTypeOfWeapon = { 4, 5, 9, 5 }
      }, {
        CLSID = "{E1AAE713-5FC3-4CAA-9FF5-3FDCFB899E33}",
        Count = 9,
        Cx_pil = 0.00766,
        Elements = { <table 13>, {
            Position = <table 14>,
            Rotation = <table 15>,
            ShapeName = "FAB-250-N1"
          }, {
            Position = <table 16>,
            Rotation = <table 17>,
            ShapeName = "FAB-250-N1"
          }, {
            Position = <table 26>,
            Rotation = <table 27>,
            ShapeName = "FAB-250-N1"
          }, {
            Position = <table 28>,
            Rotation = <table 29>,
            ShapeName = "FAB-250-N1"
          }, {
            Position = <table 18>,
            Rotation = <table 19>,
            ShapeName = "FAB-250-N1"
          }, {
            Position = <table 20>,
            Rotation = <table 21>,
            ShapeName = "FAB-250-N1"
          }, {
            Position = <table 22>,
            Rotation = <table 23>,
            ShapeName = "FAB-250-N1"
          }, {
            Position = <table 30>,
            Rotation = <table 31>,
            ShapeName = "FAB-250-N1"
          }, {
            Position = <table 24>,
            Rotation = <table 25>,
            ShapeName = "FAB-250-N1"
          } },
        Picture = "FAB250.png",
        Weight = 2310,
        attribute = { 4, 5, 32, 35 },
        displayName = "MER*9 FAB-250",
        wsTypeOfWeapon = { 4, 5, 9, 6 }
      }, {
        CLSID = "{BF83E8FD-E7A2-40D2-9608-42E13AFE2193}",
        Count = 9,
        Cx_pil = 0.0082,
        Elements = { <table 13>, {
            Position = <table 14>,
            Rotation = <table 15>,
            ShapeName = "RBK_250_PTAB_25M_cassette"
          }, {
            Position = <table 16>,
            Rotation = <table 17>,
            ShapeName = "RBK_250_PTAB_25M_cassette"
          }, {
            Position = <table 26>,
            Rotation = <table 27>,
            ShapeName = "RBK_250_PTAB_25M_cassette"
          }, {
            Position = <table 28>,
            Rotation = <table 29>,
            ShapeName = "RBK_250_PTAB_25M_cassette"
          }, {
            Position = <table 18>,
            Rotation = <table 19>,
            ShapeName = "RBK_250_PTAB_25M_cassette"
          }, {
            Position = <table 20>,
            Rotation = <table 21>,
            ShapeName = "RBK_250_PTAB_25M_cassette"
          }, {
            Position = <table 22>,
            Rotation = <table 23>,
            ShapeName = "RBK_250_PTAB_25M_cassette"
          }, {
            Position = <table 30>,
            Rotation = <table 31>,
            ShapeName = "RBK_250_PTAB_25M_cassette"
          }, {
            Position = <table 24>,
            Rotation = <table 25>,
            ShapeName = "RBK_250_PTAB_25M_cassette"
          } },
        Picture = "RBK250.png",
        Weight = 2535,
        attribute = { 4, 5, 32, 38 },
        displayName = "MER*9 RBK-250 PTAB-2.5M",
        wsTypeOfWeapon = { 4, 5, 38, 18 }
      }, {
        CLSID = "{0D945D78-542C-4E9B-9A17-9B5008CC8D39}",
        Count = 6,
        Cx_pil = 0.00988,
        Elements = { <table 13>, {
            Position = <table 14>,
            Rotation = <table 15>,
            ShapeName = "FAB-500-N3"
          }, {
            Position = <table 16>,
            Rotation = <table 17>,
            ShapeName = "FAB-500-N3"
          }, {
            Position = <table 18>,
            Rotation = <table 19>,
            ShapeName = "FAB-500-N3"
          }, {
            Position = <table 20>,
            Rotation = <table 21>,
            ShapeName = "FAB-500-N3"
          }, {
            Position = <table 22>,
            Rotation = <table 23>,
            ShapeName = "FAB-500-N3"
          }, {
            Position = <table 24>,
            Rotation = <table 25>,
            ShapeName = "FAB-500-N3"
          } },
        Picture = "FAB500.png",
        Weight = 3060,
        attribute = { 4, 5, 32, 39 },
        displayName = "MER*6 FAB-500",
        wsTypeOfWeapon = { 4, 5, 9, 7 }
      }, {
        CLSID = "{436C6FB9-8BF2-46B6-9DC4-F55ABF3CD1EC}",
        Count = 6,
        Cx_pil = 0.00988,
        Elements = { <table 13>, {
            Position = <table 14>,
            Rotation = <table 15>,
            ShapeName = "BETAB-500"
          }, {
            Position = <table 16>,
            Rotation = <table 17>,
            ShapeName = "BETAB-500"
          }, {
            Position = <table 18>,
            Rotation = <table 19>,
            ShapeName = "BETAB-500"
          }, {
            Position = <table 20>,
            Rotation = <table 21>,
            ShapeName = "BETAB-500"
          }, {
            Position = <table 22>,
            Rotation = <table 23>,
            ShapeName = "BETAB-500"
          }, {
            Position = <table 24>,
            Rotation = <table 25>,
            ShapeName = "BETAB-500"
          } },
        Picture = "betab500.png",
        Weight = 3060,
        attribute = { 4, 5, 32, 40 },
        displayName = "MER*6 BetAB-500",
        wsTypeOfWeapon = { 4, 5, 37, 3 }
      }, {
        CLSID = "{E96E1EDD-FF3F-47CF-A959-576C3B682955}",
        Count = 6,
        Cx_pil = 0.00988,
        Elements = { <table 13>, {
            Position = <table 14>,
            Rotation = <table 15>,
            ShapeName = "BETAB-500SP"
          }, {
            Position = <table 16>,
            Rotation = <table 17>,
            ShapeName = "BETAB-500SP"
          }, {
            Position = <table 18>,
            Rotation = <table 19>,
            ShapeName = "BETAB-500SP"
          }, {
            Position = <table 20>,
            Rotation = <table 21>,
            ShapeName = "BETAB-500SP"
          }, {
            Position = <table 22>,
            Rotation = <table 23>,
            ShapeName = "BETAB-500SP"
          }, {
            Position = <table 24>,
            Rotation = <table 25>,
            ShapeName = "BETAB-500SP"
          } },
        Picture = "betab500shp.png",
        Weight = 3060,
        attribute = { 4, 5, 32, 41 },
        displayName = "MER*6 BetAB-500SP",
        wsTypeOfWeapon = { 4, 5, 37, 4 }
      }, {
        CLSID = "{4D459A95-59C0-462F-8A57-34E80697F38B}",
        Count = 6,
        Cx_pil = 0.00788,
        Elements = { <table 2>, {
            Position = <table 3>,
            Rotation = <table 4>,
            ShapeName = "RBK_500_255_PTAB_10_5_cassette"
          }, {
            Position = <table 5>,
            Rotation = <table 6>,
            ShapeName = "RBK_500_255_PTAB_10_5_cassette"
          }, {
            Position = <table 7>,
            Rotation = <table 8>,
            ShapeName = "RBK_500_255_PTAB_10_5_cassette"
          }, {
            Position = <table 9>,
            Rotation = <table 10>,
            ShapeName = "RBK_500_255_PTAB_10_5_cassette"
          }, {
            Position = <table 11>,
            ShapeName = "RBK_500_255_PTAB_10_5_cassette"
          }, {
            Position = <table 12>,
            ShapeName = "RBK_500_255_PTAB_10_5_cassette"
          } },
        Picture = "RBK_500_255_PTAB_10_5_cassette.png",
        Weight = 3060,
        attribute = { 4, 5, 32, 8 },
        displayName = "MER*6 RBK-500-255 PTAB-10-5",
        wsTypeOfWeapon = { 4, 5, 38, 20 }
      }, {
        CLSID = "{7C5F0F5F-0A0B-46E8-937C-8922303E39A8}",
        Count = 2,
        Cx_pil = 0.0037,
        Elements = { <table 13>, {
            Position = <table 26>,
            Rotation = <table 27>,
            ShapeName = "FAB-1500"
          }, {
            Position = <table 28>,
            Rotation = <table 29>,
            ShapeName = "FAB-1500"
          } },
        Picture = "FAB1500.png",
        Weight = 3100,
        attribute = { 4, 5, 32, 71 },
        displayName = "MER*2 FAB-1500",
        wsTypeOfWeapon = { 4, 5, 9, 9 }
      }, {
        CLSID = "{D5D51E24-348C-4702-96AF-97A714E72697}",
        Count = 2,
        Cx_pil = 0.0007,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "MER2"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.132, -0.161, 0.298 },
            ShapeName = "MK-82"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.132, -0.161, -0.298 },
            ShapeName = "MK-82"
          } },
        Picture = "mk82.png",
        Weight = 200,
        attribute = { 4, 5, 32, 51 },
        displayName = "MER*2 MK-82",
        wsTypeOfWeapon = { 4, 5, 9, 31 }
      }, {
        CLSID = "{18617C93-78E7-4359-A8CE-D754103EDF63}",
        Count = 2,
        Cx_pil = 0.001,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "MER2"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.132, -0.161, 0.298 },
            ShapeName = "MK-83"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.132, -0.161, -0.298 },
            ShapeName = "MK-83"
          } },
        Picture = "FAB250.png",
        Weight = 200,
        attribute = { 4, 5, 32, 52 },
        displayName = "MER*2 MK-83",
        wsTypeOfWeapon = { 4, 5, 9, 32 }
      }, {
        CLSID = "{C535596E-F7D2-4301-8BB4-B1658BB87ED7}",
        Count = 2,
        Cx_pil = 0.001,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = ""
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 1.32, 0, 0 },
            ShapeName = "T-BL-755"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { -1.32, 0, 0 },
            ShapeName = "T-BL-755"
          } },
        Picture = "BL755.png",
        Weight = 200,
        attribute = { 4, 5, 32, 54 },
        displayName = "BL-755*2",
        wsTypeOfWeapon = { 4, 5, 38, 23 }
      }, {
        CLSID = "{0B9ABA77-93B8-45FC-9C63-82AFB2CB50A4}",
        Count = 2,
        Cx_pil = 0.001,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "MER2"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.132, -0.161, 0.298 },
            ShapeName = "ROCKEYE"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.132, -0.161, -0.298 },
            ShapeName = "ROCKEYE"
          } },
        Picture = "Mk20.png",
        Weight = 200,
        attribute = { 4, 5, 32, 55 },
        displayName = "2 Mk-20 Rockeye",
        wsTypeOfWeapon = { 4, 5, 38, 45 }
      }, {
        CLSID = "{E79759F7-C622-4AA4-B1EF-37639A34D924}",
        Count = 6,
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "ROCKEYE"
          } },
        Picture = "rockeye.png",
        Weight = 1332,
        attribute = { 4, 5, 38, 45 },
        displayName = "Mk-20 Rockeye *6"
      }, {
        CLSID = "{02B81892-7E24-4795-84F9-B8110C641AF0}",
        Count = 4,
        Cx_pil = 0.005,
        Elements = { <table 2>, {
            Position = <table 3>,
            Rotation = <table 4>,
            ShapeName = "RBK_250_PTAB_25M_cassette"
          }, {
            Position = <table 5>,
            Rotation = <table 6>,
            ShapeName = "RBK_250_PTAB_25M_cassette"
          }, {
            Position = <table 7>,
            Rotation = <table 8>,
            ShapeName = "RBK_250_PTAB_25M_cassette"
          }, {
            Position = <table 9>,
            Rotation = <table 10>,
            ShapeName = "RBK_250_PTAB_25M_cassette"
          } },
        Picture = "RBK250.png",
        Weight = 1060,
        attribute = { 4, 5, 32, 80 },
        displayName = "MER*4 RBK-250 PTAB-2.5M",
        wsTypeOfWeapon = { 4, 5, 38, 18 }
      }, {
        CLSID = "{6A367BB4-327F-4A04-8D9E-6D86BDC98E7E}",
        Count = 4,
        Cx_pil = 0.005,
        Elements = { <table 2>, {
            Position = <table 3>,
            Rotation = <table 4>,
            ShapeName = "FAB-250-N1"
          }, {
            Position = <table 5>,
            Rotation = <table 6>,
            ShapeName = "FAB-250-N1"
          }, {
            Position = <table 7>,
            Rotation = <table 8>,
            ShapeName = "FAB-250-N1"
          }, {
            Position = <table 9>,
            Rotation = <table 10>,
            ShapeName = "FAB-250-N1"
          } },
        Picture = "FAB250.png",
        Weight = 1060,
        attribute = { 4, 5, 32, 81 },
        displayName = "MER*4 FAB-250",
        wsTypeOfWeapon = { 4, 5, 9, 6 }
      }, {
        CLSID = "{62BE78B1-9258-48AE-B882-279534C0D278}",
        Count = 2,
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "GBU-10"
          } },
        Picture = "GBU10.png",
        Weight = 1800,
        attribute = { 4, 5, 36, 36 },
        displayName = "GBU-10*2"
      }, {
        CLSID = "{89D000B0-0360-461A-AD83-FB727E2ABA98}",
        Count = 2,
        Cx_pil = 0.002,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "BRU-42_HS"
          }, {
            DrawArgs = <32>{ { 1, 1 }, { 2, 1 } },
            Position = { 0.3, -0.15, -0.15 },
            Rotation = { 45, 0, 0 },
            ShapeName = "GBU-12"
          }, {
            DrawArgs = <table 32>,
            Position = { 0.3, -0.37, 0 },
            ShapeName = "GBU-12"
          } },
        Picture = "GBU12.png",
        Weight = 601,
        attribute = { 4, 5, 32, 178 },
        displayName = "2xGBU-12",
        wsTypeOfWeapon = { 4, 5, 36, 38 }
      }, {
        CLSID = "{BRU-42_2xGBU-12_right}",
        Count = 2,
        Cx_pil = 0.002,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "BRU-42_HS"
          }, {
            DrawArgs = <33>{ { 1, 1 }, { 2, 1 } },
            Position = { 0.3, -0.15, 0.15 },
            Rotation = { -45, 0, 0 },
            ShapeName = "GBU-12"
          }, {
            DrawArgs = <table 33>,
            Position = { 0.3, -0.37, 0 },
            ShapeName = "GBU-12"
          } },
        Picture = "GBU12.png",
        Weight = 601,
        attribute = { 4, 5, 32, 178 },
        displayName = "2xGBU-12",
        wsTypeOfWeapon = { 4, 5, 36, 38 }
      }, {
        CLSID = "BRU-42_3*GBU-12",
        Count = 3,
        Cx_pil = 0.002,
        Elements = { {
            ShapeName = "BRU-42_LS"
          }, {
            DrawArgs = <table 1>,
            ShapeName = "GBU-12",
            connector_name = "Point01"
          }, {
            DrawArgs = <table 1>,
            ShapeName = "GBU-12",
            connector_name = "Point02"
          }, {
            DrawArgs = <table 1>,
            ShapeName = "GBU-12",
            connector_name = "Point03"
          } },
        Picture = "GBU12.png",
        Weight = 876,
        attribute = { 4, 5, 32, 127 },
        displayName = "3 GBU-12",
        wsTypeOfWeapon = { 4, 5, 36, 38 }
      }, {
        CLSID = "{EB969276-1922-4ED1-A5CB-18590F45D7FE}",
        Count = 2,
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "GBU-27"
          } },
        Picture = "GBU27.png",
        Weight = 1968,
        attribute = { 4, 5, 36, 43 },
        displayName = "GBU-27*2"
      }, {
        CLSID = "{D3ABF208-FA56-4D56-BB31-E0D931D57AE3}",
        Count = 28,
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "MK-84"
          } },
        Picture = "FAB250.png",
        Weight = 25032,
        attribute = { 4, 5, 9, 33 },
        displayName = "Mk 84*28"
      }, {
        CLSID = "{B8C99F40-E486-4040-B547-6639172A5D57}",
        Count = 4,
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "GBU-27"
          } },
        Picture = "GBU27.png",
        Weight = 3936,
        attribute = { 4, 5, 36, 43 },
        displayName = "GBU-27*4"
      }, {
        CLSID = "{72CAC282-AE18-490B-BD4D-35E7EE969E73}",
        Count = 51,
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "M117"
          } },
        Picture = "RBK250.png",
        Weight = 17340,
        attribute = { 4, 5, 9, 34 },
        displayName = "M117*51"
      }, {
        CLSID = "{B84DFE16-6AC7-4854-8F6D-34137892E166}",
        Count = 51,
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "MK-82"
          } },
        Picture = "mk82.png",
        Weight = 12291,
        attribute = { 4, 5, 9, 31 },
        displayName = "51 Mk-82"
      }, {
        CLSID = "MK_82*28",
        Count = 28,
        Elements = { {
            Position = { -0.9, 0.651, -0.31 }
          }, {
            Position = { 0.9, 0.651, 0 }
          }, {
            Position = { -0.9, 0.651, 0.31 }
          }, {
            Position = { -0.9, 0.434, -0.465 }
          }, {
            Position = { 0.9, 0.434, -0.155 }
          }, {
            Position = { -0.9, 0.434, 0.155 }
          }, {
            Position = { 0.9, 0.434, 0.465 }
          }, {
            Position = { -0.9, 0.217, -0.62 }
          }, {
            Position = { 0.9, 0.217, -0.31 }
          }, {
            Position = { -0.9, 0.217, 0 }
          }, {
            Position = { 0.9, 0.217, 0.31 }
          }, {
            Position = { -0.9, 0.217, 0.62 }
          }, {
            Position = { 0.9, 1.1102230246252e-016, -0.465 }
          }, {
            Position = { -0.9, 1.1102230246252e-016, -0.155 }
          }, {
            Position = { 0.9, 1.1102230246252e-016, 0.155 }
          }, {
            Position = { -0.9, 1.1102230246252e-016, 0.465 }
          }, {
            Position = { 0.9, -0.217, -0.62 }
          }, {
            Position = { -0.9, -0.217, -0.31 }
          }, {
            Position = { 0.9, -0.217, 0 }
          }, {
            Position = { -0.9, -0.217, 0.31 }
          }, {
            Position = { 0.9, -0.217, 0.62 }
          }, {
            Position = { -0.9, -0.434, -0.465 }
          }, {
            Position = { 0.9, -0.434, -0.155 }
          }, {
            Position = { -0.9, -0.434, 0.155 }
          }, {
            Position = { 0.9, -0.434, 0.465 }
          }, {
            Position = { 0.9, -0.651, -0.31 }
          }, {
            Position = { -0.9, -0.651, 0 }
          }, {
            Position = { 0.9, -0.651, 0.31 }
          } },
        Picture = "mk82.png",
        Weight = 6748,
        attribute = { 4, 5, 9, 31 },
        displayName = "Mk-82*28"
      }, {
        CLSID = "TEST_ROTARY_LAUNCHER_MK82",
        Count = 24,
        Elements = { {
            Position = { 0, -0.35, 0 },
            Rotation = { -0, 0, 0 }
          }, {
            Position = { 2.45, -0.35, 0 },
            Rotation = { -0, 0, 0 }
          }, {
            Position = { 4.9, -0.35, 0 },
            Rotation = { -0, 0, 0 }
          }, {
            Position = { 0, -0.24748737341529, 0.24748737341529 },
            Rotation = { -45.003314762674, 0, 0 }
          }, {
            Position = { 2.45, -0.24748737341529, 0.24748737341529 },
            Rotation = { -45.003314762674, 0, 0 }
          }, {
            Position = { 4.9, -0.24748737341529, 0.24748737341529 },
            Rotation = { -45.003314762674, 0, 0 }
          }, {
            Position = { 0, -2.1431318985079e-017, 0.35 },
            Rotation = { -90.006629525348, 0, 0 }
          }, {
            Position = { 2.45, -2.1431318985079e-017, 0.35 },
            Rotation = { -90.006629525348, 0, 0 }
          }, {
            Position = { 4.9, -2.1431318985079e-017, 0.35 },
            Rotation = { -90.006629525348, 0, 0 }
          }, {
            Position = { 0, 0.24748737341529, 0.24748737341529 },
            Rotation = { -135.00994428802, 0, 0 }
          }, {
            Position = { 2.45, 0.24748737341529, 0.24748737341529 },
            Rotation = { -135.00994428802, 0, 0 }
          }, {
            Position = { 4.9, 0.24748737341529, 0.24748737341529 },
            Rotation = { -135.00994428802, 0, 0 }
          }, {
            Position = { 0, 0.35, 4.2862637970157e-017 },
            Rotation = { -180.0132590507, 0, 0 }
          }, {
            Position = { 2.45, 0.35, 4.2862637970157e-017 },
            Rotation = { -180.0132590507, 0, 0 }
          }, {
            Position = { 4.9, 0.35, 4.2862637970157e-017 },
            Rotation = { -180.0132590507, 0, 0 }
          }, {
            Position = { 0, 0.24748737341529, -0.24748737341529 },
            Rotation = { -225.01657381337, 0, 0 }
          }, {
            Position = { 2.45, 0.24748737341529, -0.24748737341529 },
            Rotation = { -225.01657381337, 0, 0 }
          }, {
            Position = { 4.9, 0.24748737341529, -0.24748737341529 },
            Rotation = { -225.01657381337, 0, 0 }
          }, {
            Position = { 0, 6.4293956955236e-017, -0.35 },
            Rotation = { -270.01988857604, 0, 0 }
          }, {
            Position = { 2.45, 6.4293956955236e-017, -0.35 },
            Rotation = { -270.01988857604, 0, 0 }
          }, {
            Position = { 4.9, 6.4293956955236e-017, -0.35 },
            Rotation = { -270.01988857604, 0, 0 }
          }, {
            Position = { 0, -0.24748737341529, -0.24748737341529 },
            Rotation = { -315.02320333872, 0, 0 }
          }, {
            Position = { 2.45, -0.24748737341529, -0.24748737341529 },
            Rotation = { -315.02320333872, 0, 0 }
          }, {
            Position = { 4.9, -0.24748737341529, -0.24748737341529 },
            Rotation = { -315.02320333872, 0, 0 }
          } },
        Picture = "mk82.png",
        Weight = 6748,
        attribute = { 4, 5, 9, 31 },
        displayName = "TEST_ROTARY_LAUNCHER_MK82"
      }, {
        CLSID = "B-1B_Mk-84*8",
        Count = 8,
        Elements = { {
            Position = { 0, -0.35, 0 },
            Rotation = { -0, 0, 0 }
          }, {
            Position = { 0, -0.24748737341529, 0.24748737341529 },
            Rotation = { -45.003314762674, 0, 0 }
          }, {
            Position = { 0, -2.1431318985079e-017, 0.35 },
            Rotation = { -90.006629525348, 0, 0 }
          }, {
            Position = { 0, 0.24748737341529, 0.24748737341529 },
            Rotation = { -135.00994428802, 0, 0 }
          }, {
            Position = { 0, 0.35, 4.2862637970157e-017 },
            Rotation = { -180.0132590507, 0, 0 }
          }, {
            Position = { 0, 0.24748737341529, -0.24748737341529 },
            Rotation = { -225.01657381337, 0, 0 }
          }, {
            Position = { 0, 6.4293956955236e-017, -0.35 },
            Rotation = { -270.01988857604, 0, 0 }
          }, {
            Position = { 0, -0.24748737341529, -0.24748737341529 },
            Rotation = { -315.02320333872, 0, 0 }
          } },
        Picture = "mk84.png",
        Weight = 7152,
        attribute = { 4, 5, 9, 33 },
        displayName = "Mk-84*8"
      }, {
        CLSID = "GBU-31*8",
        Count = 8,
        Elements = { {
            Position = { 0, -0.35, 0 },
            Rotation = { -0, 0, 0 }
          }, {
            Position = { 0, -0.24748737341529, 0.24748737341529 },
            Rotation = { -45.003314762674, 0, 0 }
          }, {
            Position = { 0, -2.1431318985079e-017, 0.35 },
            Rotation = { -90.006629525348, 0, 0 }
          }, {
            Position = { 0, 0.24748737341529, 0.24748737341529 },
            Rotation = { -135.00994428802, 0, 0 }
          }, {
            Position = { 0, 0.35, 4.2862637970157e-017 },
            Rotation = { -180.0132590507, 0, 0 }
          }, {
            Position = { 0, 0.24748737341529, -0.24748737341529 },
            Rotation = { -225.01657381337, 0, 0 }
          }, {
            Position = { 0, 6.4293956955236e-017, -0.35 },
            Rotation = { -270.01988857604, 0, 0 }
          }, {
            Position = { 0, -0.24748737341529, -0.24748737341529 },
            Rotation = { -315.02320333872, 0, 0 }
          } },
        Picture = "GBU31.png",
        Weight = 7152,
        attribute = { 4, 5, 36, 85 },
        displayName = "GBU-31*8"
      }, {
        CLSID = "GBU-31V3B*8",
        Count = 8,
        Elements = { {
            Position = { 0, -0.35, 0 },
            Rotation = { -0, 0, 0 }
          }, {
            Position = { 0, -0.24748737341529, 0.24748737341529 },
            Rotation = { -45.003314762674, 0, 0 }
          }, {
            Position = { 0, -2.1431318985079e-017, 0.35 },
            Rotation = { -90.006629525348, 0, 0 }
          }, {
            Position = { 0, 0.24748737341529, 0.24748737341529 },
            Rotation = { -135.00994428802, 0, 0 }
          }, {
            Position = { 0, 0.35, 4.2862637970157e-017 },
            Rotation = { -180.0132590507, 0, 0 }
          }, {
            Position = { 0, 0.24748737341529, -0.24748737341529 },
            Rotation = { -225.01657381337, 0, 0 }
          }, {
            Position = { 0, 6.4293956955236e-017, -0.35 },
            Rotation = { -270.01988857604, 0, 0 }
          }, {
            Position = { 0, -0.24748737341529, -0.24748737341529 },
            Rotation = { -315.02320333872, 0, 0 }
          } },
        Picture = "GBU-31V3B.png",
        Weight = 7848,
        attribute = { 4, 5, 36, 92 },
        displayName = "GBU-31(V)3/B*8"
      }, {
        CLSID = "GBU-38*16",
        Count = 16,
        Elements = { {
            Position = { -0.9, 0.651, -0.31 }
          }, {
            Position = { 0.9, 0.651, 0 }
          }, {
            Position = { -0.9, 0.651, 0.31 }
          }, {
            Position = { -0.9, 0.434, -0.465 }
          }, {
            Position = { 0.9, 0.434, -0.155 }
          }, {
            Position = { -0.9, 0.434, 0.155 }
          }, {
            Position = { 0.9, 0.434, 0.465 }
          }, {
            Position = { -0.9, 0.217, -0.62 }
          }, {
            Position = { 0.9, 0.217, -0.31 }
          }, {
            Position = { -0.9, 0.217, 0 }
          }, {
            Position = { 0.9, 0.217, 0.31 }
          }, {
            Position = { -0.9, 0.217, 0.62 }
          }, {
            Position = { 0.9, 1.1102230246252e-016, -0.465 }
          }, {
            Position = { -0.9, 1.1102230246252e-016, -0.155 }
          }, {
            Position = { 0.9, 1.1102230246252e-016, 0.155 }
          }, {
            Position = { -0.9, 1.1102230246252e-016, 0.465 }
          }, {
            Position = { 0.9, -0.217, -0.62 }
          }, {
            Position = { -0.9, -0.217, -0.31 }
          }, {
            Position = { 0.9, -0.217, 0 }
          }, {
            Position = { -0.9, -0.217, 0.31 }
          }, {
            Position = { 0.9, -0.217, 0.62 }
          }, {
            Position = { -0.9, -0.434, -0.465 }
          }, {
            Position = { 0.9, -0.434, -0.155 }
          }, {
            Position = { -0.9, -0.434, 0.155 }
          }, {
            Position = { 0.9, -0.434, 0.465 }
          }, {
            Position = { 0.9, -0.651, -0.31 }
          }, {
            Position = { -0.9, -0.651, 0 }
          }, {
            Position = { 0.9, -0.651, 0.31 }
          } },
        Picture = "GBU38.png",
        Weight = 3856,
        attribute = { 4, 5, 36, 86 },
        displayName = "GBU-38*16"
      }, {
        CLSID = "CBU87*10",
        Count = 10,
        Elements = { {
            Position = { -0.9, 0.315, -0.45 }
          }, {
            Position = { 0.9, 0.315, 0 }
          }, {
            Position = { -0.9, 0.315, 0.45 }
          }, {
            Position = { -0.9, 0, -0.675 }
          }, {
            Position = { 0.9, 0, -0.225 }
          }, {
            Position = { -0.9, 0, 0.225 }
          }, {
            Position = { 0.9, 0, 0.675 }
          }, {
            Position = { 0.9, -0.315, -0.45 }
          }, {
            Position = { -0.9, -0.315, 0 }
          }, {
            Position = { 0.9, -0.315, 0.45 }
          } },
        Picture = "CBU.png",
        Weight = 4300,
        attribute = { 4, 5, 38, 77 },
        displayName = "CBU-87*10"
      }, {
        CLSID = "CBU97*10",
        Count = 10,
        Elements = { {
            Position = { -0.9, 0.315, -0.45 }
          }, {
            Position = { 0.9, 0.315, 0 }
          }, {
            Position = { -0.9, 0.315, 0.45 }
          }, {
            Position = { -0.9, 0, -0.675 }
          }, {
            Position = { 0.9, 0, -0.225 }
          }, {
            Position = { -0.9, 0, 0.225 }
          }, {
            Position = { 0.9, 0, 0.675 }
          }, {
            Position = { 0.9, -0.315, -0.45 }
          }, {
            Position = { -0.9, -0.315, 0 }
          }, {
            Position = { 0.9, -0.315, 0.45 }
          } },
        Picture = "CBU.png",
        Weight = 4170,
        attribute = { 4, 5, 38, 35 },
        displayName = "CBU-97*10"
      }, {
        CLSID = "{027563C9-D87E-4A85-B317-597B510E3F03}",
        Count = 6,
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "MK-82"
          } },
        Picture = "FAB250.png",
        Weight = 1446,
        attribute = { 4, 5, 9, 31 },
        displayName = "6 Mk-82"
      }, {
        CLSID = "{ACADB374-6D6C-45A0-BA7C-B22B2E108AE4}",
        Count = 18,
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "ROCKEYE"
          } },
        Picture = "rockeye.png",
        Weight = 3996,
        attribute = { 4, 5, 38, 45 },
        displayName = "Mk 20*18"
      }, {
        CLSID = "{F092B80C-BB54-477E-9408-66DEEF740008}",
        Count = 18,
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "MK-84"
          } },
        Picture = "FAB250.png",
        Weight = 16092,
        attribute = { 4, 5, 9, 33 },
        displayName = "Mk 84*18"
      }, {
        CLSID = "{BDAD04AA-4D4A-4E51-B958-180A89F963CF}",
        Count = 33,
        Elements = { {
            Position = { 2.5, 2.3, -0.5 },
            ShapeName = "FAB-250-N1"
          }, {
            Position = { 2.5, 2.3, 0 },
            ShapeName = "FAB-250-N1"
          }, {
            Position = { 2.5, 2.3, 0.5 },
            ShapeName = "FAB-250-N1"
          }, {
            Position = { 2.5, 1.8, -0.5 },
            ShapeName = "FAB-250-N1"
          }, {
            Position = { 2.5, 1.8, 0 },
            ShapeName = "FAB-250-N1"
          }, {
            Position = { 2.5, 1.8, 0.5 },
            ShapeName = "FAB-250-N1"
          }, {
            Position = { 2.5, 1.3, -0.5 },
            ShapeName = "FAB-250-N1"
          }, {
            Position = { 2.5, 1.3, 0 },
            ShapeName = "FAB-250-N1"
          }, {
            Position = { 2.5, 1.3, 0.5 },
            ShapeName = "FAB-250-N1"
          }, {
            Position = { 2.5, 0.8, -0.5 },
            ShapeName = "FAB-250-N1"
          }, {
            Position = { 2.5, 0.8, 0 },
            ShapeName = "FAB-250-N1"
          }, {
            Position = { 2.5, 0.8, 0.5 },
            ShapeName = "FAB-250-N1"
          }, {
            Position = { 2.5, 0.3, -0.5 },
            ShapeName = "FAB-250-N1"
          }, {
            Position = { 2.5, 0.3, 0 },
            ShapeName = "FAB-250-N1"
          }, {
            Position = { 2.5, 0.3, 0.5 },
            ShapeName = "FAB-250-N1"
          }, {
            Position = { 0.2, 2.3, -0.5 },
            ShapeName = "FAB-250-N1"
          }, {
            Position = { 0.2, 2.3, 0 },
            ShapeName = "FAB-250-N1"
          }, {
            Position = { 0.2, 2.3, 0.5 },
            ShapeName = "FAB-250-N1"
          }, {
            Position = { 0.2, 1.8, -0.5 },
            ShapeName = "FAB-250-N1"
          }, {
            Position = { 0.2, 1.8, 0 },
            ShapeName = "FAB-250-N1"
          }, {
            Position = { 0.2, 1.8, 0.5 },
            ShapeName = "FAB-250-N1"
          }, {
            Position = { 0.2, 1.3, -0.5 },
            ShapeName = "FAB-250-N1"
          }, {
            Position = { 0.2, 1.3, 0 },
            ShapeName = "FAB-250-N1"
          }, {
            Position = { 0.2, 1.3, 0.5 },
            ShapeName = "FAB-250-N1"
          }, {
            Position = { 0.2, 0.8, -0.5 },
            ShapeName = "FAB-250-N1"
          }, {
            Position = { 0.2, 0.8, 0 },
            ShapeName = "FAB-250-N1"
          }, {
            Position = { 0.2, 0.8, 0.5 },
            ShapeName = "FAB-250-N1"
          }, {
            Position = { 0.2, 0.3, -0.5 },
            ShapeName = "FAB-250-N1"
          }, {
            Position = { 0.2, 0.3, 0 },
            ShapeName = "FAB-250-N1"
          }, {
            Position = { 0.2, 0.3, 0.5 },
            ShapeName = "FAB-250-N1"
          }, {
            Position = { -2.1, 2.3, -0.5 },
            ShapeName = "FAB-250-N1"
          }, {
            Position = { -2.1, 2.3, 0 },
            ShapeName = "FAB-250-N1"
          }, {
            Position = { -2.1, 2.3, 0.5 },
            ShapeName = "FAB-250-N1"
          } },
        Picture = "FAB250.png",
        Weight = 8250,
        attribute = { 4, 5, 9, 6 },
        displayName = "FAB-250*33"
      }, {
        CLSID = "{AD5E5863-08FC-4283-B92C-162E2B2BD3FF}",
        Count = 33,
        Elements = { {
            Position = { 2.5, 2.3, -0.5 },
            ShapeName = "FAB-500-N3"
          }, {
            Position = { 2.5, 2.3, 0 },
            ShapeName = "FAB-500-N3"
          }, {
            Position = { 2.5, 2.3, 0.5 },
            ShapeName = "FAB-500-N3"
          }, {
            Position = { 2.5, 1.8, -0.5 },
            ShapeName = "FAB-500-N3"
          }, {
            Position = { 2.5, 1.8, 0 },
            ShapeName = "FAB-500-N3"
          }, {
            Position = { 2.5, 1.8, 0.5 },
            ShapeName = "FAB-500-N3"
          }, {
            Position = { 2.5, 1.3, -0.5 },
            ShapeName = "FAB-500-N3"
          }, {
            Position = { 2.5, 1.3, 0 },
            ShapeName = "FAB-500-N3"
          }, {
            Position = { 2.5, 1.3, 0.5 },
            ShapeName = "FAB-500-N3"
          }, {
            Position = { 2.5, 0.8, -0.5 },
            ShapeName = "FAB-500-N3"
          }, {
            Position = { 2.5, 0.8, 0 },
            ShapeName = "FAB-500-N3"
          }, {
            Position = { 2.5, 0.8, 0.5 },
            ShapeName = "FAB-500-N3"
          }, {
            Position = { 2.5, 0.3, -0.5 },
            ShapeName = "FAB-500-N3"
          }, {
            Position = { 2.5, 0.3, 0 },
            ShapeName = "FAB-500-N3"
          }, {
            Position = { 2.5, 0.3, 0.5 },
            ShapeName = "FAB-500-N3"
          }, {
            Position = { 0.2, 2.3, -0.5 },
            ShapeName = "FAB-500-N3"
          }, {
            Position = { 0.2, 2.3, 0 },
            ShapeName = "FAB-500-N3"
          }, {
            Position = { 0.2, 2.3, 0.5 },
            ShapeName = "FAB-500-N3"
          }, {
            Position = { 0.2, 1.8, -0.5 },
            ShapeName = "FAB-500-N3"
          }, {
            Position = { 0.2, 1.8, 0 },
            ShapeName = "FAB-500-N3"
          }, {
            Position = { 0.2, 1.8, 0.5 },
            ShapeName = "FAB-500-N3"
          }, {
            Position = { 0.2, 1.3, -0.5 },
            ShapeName = "FAB-500-N3"
          }, {
            Position = { 0.2, 1.3, 0 },
            ShapeName = "FAB-500-N3"
          }, {
            Position = { 0.2, 1.3, 0.5 },
            ShapeName = "FAB-500-N3"
          }, {
            Position = { 0.2, 0.8, -0.5 },
            ShapeName = "FAB-500-N3"
          }, {
            Position = { 0.2, 0.8, 0 },
            ShapeName = "FAB-500-N3"
          }, {
            Position = { 0.2, 0.8, 0.5 },
            ShapeName = "FAB-500-N3"
          }, {
            Position = { 0.2, 0.3, -0.5 },
            ShapeName = "FAB-500-N3"
          }, {
            Position = { 0.2, 0.3, 0 },
            ShapeName = "FAB-500-N3"
          }, {
            Position = { 0.2, 0.3, 0.5 },
            ShapeName = "FAB-500-N3"
          }, {
            Position = { -2.1, 2.3, -0.5 },
            ShapeName = "FAB-500-N3"
          }, {
            Position = { -2.1, 2.3, 0 },
            ShapeName = "FAB-500-N3"
          }, {
            Position = { -2.1, 2.3, 0.5 },
            ShapeName = "FAB-500-N3"
          } },
        Picture = "FAB500.png",
        Weight = 16500,
        attribute = { 4, 5, 9, 7 },
        displayName = "FAB-500*33"
      }, {
        CLSID = "{B0241BD2-5628-47E0-954C-A8675B7E698E}",
        Count = 24,
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "FAB-250-N1"
          } },
        Picture = "FAB250.png",
        Weight = 6000,
        attribute = { 4, 5, 9, 6 },
        displayName = "FAB-250*24"
      }, {
        CLSID = "{26D2AF37-B0DF-4AB6-9D61-A150FF58A37B}",
        Count = 6,
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "FAB-500-N3"
          } },
        Picture = "FAB500.png",
        Weight = 3000,
        attribute = { 4, 5, 9, 7 },
        displayName = "FAB-500*6"
      }, {
        CLSID = "{E70446B7-C7E6-4B95-B685-DEA10CAD1A0E}",
        Count = 12,
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "FAB-500-N3"
          } },
        Picture = "FAB500.png",
        Weight = 6000,
        attribute = { 4, 5, 9, 7 },
        displayName = "FAB-500*12"
      }, {
        CLSID = "{639DB5DD-CB7E-4E42-AC75-2112BC397B97}",
        Count = 3,
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "FAB-1500"
          } },
        Picture = "FAB1500.png",
        Weight = 4500,
        attribute = { 4, 5, 9, 9 },
        displayName = "FAB-1500*3"
      }, {
        CLSID = "{D9179118-E42F-47DE-A483-A6C2EA7B4F38}",
        Count = 6,
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "FAB-1500"
          } },
        Picture = "FAB1500.png",
        Weight = 9000,
        attribute = { 4, 5, 9, 9 },
        displayName = "FAB-1500*6"
      }, {
        CLSID = "{2B7BDB38-4F45-43F9-BE02-E7B3141F3D24}",
        Count = 6,
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "BETAB-500"
          } },
        Picture = "betab500.png",
        Weight = 2868,
        attribute = { 4, 5, 37, 3 },
        displayName = "BetAB-500*6"
      }, {
        CLSID = "{D6A0441E-6794-4FEB-87F7-E68E2290DFAB}",
        Count = 1,
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "BETAB-500"
          } },
        Picture = "betab500.png",
        Weight = 478,
        attribute = { 4, 5, 37, 3 },
        displayName = "BetAB-500*12"
      }, {
        CLSID = "{585D626E-7F42-4073-AB70-41E728C333E2}",
        Count = 12,
        Cx_pil = 0.005,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "B52-MBD_M117"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { -3.084, -0.131, 0 },
            ShapeName = "MK-82"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { -0.867, -0.131, 0 },
            ShapeName = "MK-82"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 1.349, -0.131, 0 },
            ShapeName = "MK-82"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 3.566, -0.131, 0 },
            ShapeName = "MK-82"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, -1 } },
            Position = { -3.084, -0.037, 0.141 },
            ShapeName = "MK-82"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, 1 } },
            Position = { -3.084, -0.037, -0.141 },
            ShapeName = "MK-82"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, -1 } },
            Position = { -0.867, -0.037, 0.141 },
            ShapeName = "MK-82"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, 1 } },
            Position = { -0.867, -0.037, -0.141 },
            ShapeName = "MK-82"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, -1 } },
            Position = { 1.349, -0.037, 0.141 },
            ShapeName = "MK-82"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, 1 } },
            Position = { 1.349, -0.037, -0.141 },
            ShapeName = "MK-82"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, -1 } },
            Position = { 3.566, -0.037, 0.141 },
            ShapeName = "MK-82"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, 1 } },
            Position = { 3.566, -0.037, -0.141 },
            ShapeName = "MK-82"
          } },
        Picture = "mk82.png",
        Weight = 3000,
        attribute = { 4, 5, 32, 74 },
        displayName = "MER*12 Mk-82",
        wsTypeOfWeapon = { 4, 5, 9, 31 }
      }, {
        CLSID = "{574EDEDF-20DE-4942-B2A2-B2EDFD621562}",
        Count = 12,
        Cx_pil = 0.005,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "B52-MBD_M117"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { -3.084, -0.131, 0 },
            ShapeName = "M117"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { -0.867, -0.131, 0 },
            ShapeName = "M117"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 1.349, -0.131, 0 },
            ShapeName = "M117"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 3.566, -0.131, 0 },
            ShapeName = "M117"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, -1 } },
            Position = { -3.084, -0.037, 0.141 },
            ShapeName = "M117"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, 1 } },
            Position = { -3.084, -0.037, -0.141 },
            ShapeName = "M117"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, -1 } },
            Position = { -0.867, -0.037, 0.141 },
            ShapeName = "M117"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, 1 } },
            Position = { -0.867, -0.037, -0.141 },
            ShapeName = "M117"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, -1 } },
            Position = { 1.349, -0.037, 0.141 },
            ShapeName = "M117"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, 1 } },
            Position = { 1.349, -0.037, -0.141 },
            ShapeName = "M117"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, -1 } },
            Position = { 3.566, -0.037, 0.141 },
            ShapeName = "M117"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, 1 } },
            Position = { 3.566, -0.037, -0.141 },
            ShapeName = "M117"
          } },
        Picture = "KMGU2.png",
        Weight = 4250,
        attribute = { 4, 5, 32, 76 },
        displayName = "MER*12 M117",
        wsTypeOfWeapon = { 4, 5, 9, 34 }
      }, {
        CLSID = "{696CFFC4-0BDE-42A8-BE4B-0BE3D9DD723C}",
        Count = 9,
        Cx_pil = 0.005,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "B52-MBD_MK84"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { -3.853, -0.134, 0 },
            ShapeName = "MK-84"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { -0.052, -0.134, 0 },
            ShapeName = "MK-84"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 3.774, -0.134, 0 },
            ShapeName = "MK-84"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, -1 } },
            Position = { -3.853, -0.038, 0.147 },
            ShapeName = "MK-84"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, 1 } },
            Position = { -3.853, -0.038, -0.147 },
            ShapeName = "MK-84"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, -1 } },
            Position = { -0.052, -0.038, 0.147 },
            ShapeName = "MK-84"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, 1 } },
            Position = { -0.052, -0.038, -0.147 },
            ShapeName = "MK-84"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, -1 } },
            Position = { 3.774, -0.038, 0.147 },
            ShapeName = "MK-84"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, 1 } },
            Position = { 3.774, -0.038, -0.147 },
            ShapeName = "MK-84"
          } },
        Picture = "FAB250.png",
        Weight = 8100,
        attribute = { 4, 5, 32, 78 },
        displayName = "HSAB*9 Mk-84",
        wsTypeOfWeapon = { 4, 5, 9, 33 }
      }, {
        CLSID = "{4CD2BB0F-5493-44EF-A927-9760350F7BA1}",
        Count = 9,
        Cx_pil = 0.005,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "B52-MBD_MK84"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { -3.853, -0.134, 0 },
            ShapeName = "ROCKEYE"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { -0.052, -0.134, 0 },
            ShapeName = "ROCKEYE"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 3.774, -0.134, 0 },
            ShapeName = "ROCKEYE"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, -1 } },
            Position = { -3.853, -0.038, 0.147 },
            ShapeName = "ROCKEYE"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, 1 } },
            Position = { -3.853, -0.038, -0.147 },
            ShapeName = "ROCKEYE"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, -1 } },
            Position = { -0.052, -0.038, 0.147 },
            ShapeName = "ROCKEYE"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, 1 } },
            Position = { -0.052, -0.038, -0.147 },
            ShapeName = "ROCKEYE"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, -1 } },
            Position = { 3.774, -0.038, 0.147 },
            ShapeName = "ROCKEYE"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, 1 } },
            Position = { 3.774, -0.038, -0.147 },
            ShapeName = "ROCKEYE"
          } },
        Picture = "rockeye.png",
        Weight = 2050,
        attribute = { 4, 5, 32, 79 },
        displayName = "HSAB*9 Mk-20 Rockeye",
        wsTypeOfWeapon = { 4, 5, 38, 45 }
      }, {
        CLSID = "{6C47D097-83FF-4FB2-9496-EAB36DDF0B05}",
        Count = 27,
        Elements = { {
            Position = { 2.5, 1.5, -0.5 },
            ShapeName = "MK-82"
          }, {
            Position = { 2.5, 1.5, 0 },
            ShapeName = "MK-82"
          }, {
            Position = { 2.5, 1.5, 0.5 },
            ShapeName = "MK-82"
          }, {
            Position = { 2.5, 1, -0.5 },
            ShapeName = "MK-82"
          }, {
            Position = { 2.5, 1, 0 },
            ShapeName = "MK-82"
          }, {
            Position = { 2.5, 1, 0.5 },
            ShapeName = "MK-82"
          }, {
            Position = { 2.5, 0.5, -0.5 },
            ShapeName = "MK-82"
          }, {
            Position = { 2.5, 0.5, 0 },
            ShapeName = "MK-82"
          }, {
            Position = { 2.5, 0.5, 0.5 },
            ShapeName = "MK-82"
          }, {
            Position = { 0.2, 1.5, -0.5 },
            ShapeName = "MK-82"
          }, {
            Position = { 0.2, 1.5, 0 },
            ShapeName = "MK-82"
          }, {
            Position = { 0.2, 1.5, 0.5 },
            ShapeName = "MK-82"
          }, {
            Position = { 0.2, 1, -0.5 },
            ShapeName = "MK-82"
          }, {
            Position = { 0.2, 1, 0 },
            ShapeName = "MK-82"
          }, {
            Position = { 0.2, 1, 0.5 },
            ShapeName = "MK-82"
          }, {
            Position = { 0.2, 0.5, -0.5 },
            ShapeName = "MK-82"
          }, {
            Position = { 0.2, 0.5, 0 },
            ShapeName = "MK-82"
          }, {
            Position = { 0.2, 0.5, 0.5 },
            ShapeName = "MK-82"
          }, {
            Position = { -2.1, 1.5, -0.5 },
            ShapeName = "MK-82"
          }, {
            Position = { -2.1, 1.5, 0 },
            ShapeName = "MK-82"
          }, {
            Position = { -2.1, 1.5, 0.5 },
            ShapeName = "MK-82"
          }, {
            Position = { -2.1, 1, -0.5 },
            ShapeName = "MK-82"
          }, {
            Position = { -2.1, 1, 0 },
            ShapeName = "MK-82"
          }, {
            Position = { -2.1, 1, 0.5 },
            ShapeName = "MK-82"
          }, {
            Position = { -2.1, 0.5, -0.5 },
            ShapeName = "MK-82"
          }, {
            Position = { -2.1, 0.5, 0 },
            ShapeName = "MK-82"
          }, {
            Position = { -2.1, 0.5, 0.5 },
            ShapeName = "MK-82"
          } },
        Picture = "mk82.png",
        Weight = 6507,
        attribute = { 4, 5, 9, 31 },
        displayName = "27 Mk-82"
      }, {
        CLSID = "{Mk82AIR}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "Mk-82AIR"
          } },
        Picture = "mk82AIR.png",
        Weight = 232,
        attribute = { 4, 5, 9, 75 },
        displayName = "Mk-82AIR"
      }, {
        CLSID = "{Mk82SNAKEYE}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "MK-82_Snakeye"
          } },
        Picture = "mk82AIR.png",
        Weight = 232,
        attribute = { 4, 5, 9, 79 },
        displayName = "Mk-82 SnakeEye"
      }, {
        CLSID = "{B58F99BA-5480-4572-8602-28B0449F5260}",
        Count = 27,
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "M117"
          } },
        Picture = "RBK250.png",
        Weight = 9180,
        attribute = { 4, 5, 9, 34 },
        displayName = "M117*27"
      }, {
        CLSID = "{29A828E2-C6BB-11d8-9897-000476191836}",
        Count = 4,
        Cx_pil = 0.001,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "MBD-2-67U"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.701, -0.088, -0.107 },
            Rotation = { 35, 0, 0 },
            ShapeName = "FAB-100"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { -0.595, -0.088, -0.107 },
            Rotation = { 35, 0, 0 },
            ShapeName = "FAB-100"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.701, -0.088, 0.107 },
            Rotation = { -35, 0, 0 },
            ShapeName = "FAB-100"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { -0.595, -0.088, 0.107 },
            Rotation = { -35, 0, 0 },
            ShapeName = "FAB-100"
          } },
        Picture = "FAB100.png",
        Weight = 465,
        attribute = { 4, 5, 32, 32 },
        displayName = "MBD-2-67U - 4 FAB-100",
        wsTypeOfWeapon = { 4, 5, 9, 5 }
      }, {
        CLSID = "{96A7F676-F956-404A-AD04-F33FB2C74884}",
        Count = 96,
        Cx_pil = 0.00167,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "KMGU-2"
          } },
        Picture = "KMGU2.png",
        Weight = 520,
        attribute = { 4, 5, 32, 94 },
        displayName = "KMGU-2 - 96 AO-2.5RT",
        kind_of_shipping = 2,
        wsTypeOfWeapon = { 4, 5, 9, 65 }
      }, {
        CLSID = "{96A7F676-F956-404A-AD04-F33FB2C74881}",
        Count = 96,
        Cx_pil = 0.00167,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "KMGU-2"
          } },
        Picture = "KMGU2.png",
        Weight = 520,
        attribute = { 4, 5, 32, 95 },
        displayName = "KMGU-2 - 96 PTAB-2.5KO",
        kind_of_shipping = 2,
        wsTypeOfWeapon = { 4, 5, 9, 66 }
      }, {
        CLSID = "{CAE48299-A294-4bad-8EE6-89EFC5DCDF00}",
        Count = 8,
        Cx_pil = 0.001,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "SUU-25"
          } },
        Picture = "L005.png",
        Weight = 130,
        attribute = { 4, 5, 32, 85 },
        displayName = "SUU-25 * 8 LUU-2",
        wsTypeOfWeapon = { 4, 5, 49, 64 }
      }, {
        CLSID = "{BRU-42_LS_3*SUU-25_8*LUU-2}",
        Count = 24,
        Cx_pil = 0.001,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "BRU-42_LS_3_SUU-25"
          }, {
            Position = { -0.3, -0.1, 0 }
          }, {
            Position = { -0.3, -0.18, -0.08 }
          }, {
            Position = { -0.3, -0.26, 0 }
          }, {
            Position = { -0.3, -0.18, 0.08 }
          }, {
            Position = { -0.3, -0.1, 0 }
          }, {
            pos = { -0.3, -0.18, -0.08 }
          }, {
            Position = { -0.3, -0.26, 0 }
          }, {
            Position = { -0.3, -0.18, 0.08 }
          } },
        Picture = "L005.png",
        Weight = 490,
        attribute = { 4, 5, 32, 144 },
        displayName = "3 SUU-25 * 8 LUU-2",
        wsTypeOfWeapon = { 4, 5, 49, 64 }
      }, {
        CLSID = "{AN-M64}",
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "AN-M64"
          } },
        Picture = "M64.png",
        Weight = 227,
        attribute = { 4, 5, 9, 90 },
        displayName = "AN-M64"
      }, {
        CLSID = "{7AEC222D-C523-425e-B714-719C0D1EB14D}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "RBK_500_PTAB_1M_cassette"
          } },
        Picture = "RBK_500_SPBE_D_cassette.png",
        Weight = 427,
        attribute = { 4, 5, 38, 91 },
        displayName = "RBK-500 PTAB-1M"
      }, {
        CLSID = "{MER-5E_MK82x5}",
        Count = 5,
        Cx_pil = 0.00544,
        Elements = { {
            IsAdapter = true,
            ShapeName = "MER-5E"
          }, {
            ShapeName = "Mk-82",
            connector_name = "Point_Pilon_01"
          }, {
            ShapeName = "Mk-82",
            connector_name = "Point_Pilon_02"
          }, {
            ShapeName = "Mk-82",
            connector_name = "Point_Pilon_03"
          }, {
            ShapeName = "Mk-82",
            connector_name = "Point_Pilon_04"
          }, {
            ShapeName = "Mk-82",
            connector_name = "Point_Pilon_05"
          } },
        Picture = "mk82.png",
        Weight = 1295.7,
        attribute = { 4, 5, 32, 179 },
        displayName = "5 Mk-82",
        wsTypeOfWeapon = { 4, 5, 9, 31 }
      }, {
        CLSID = "{MER-5E_Mk82SNAKEYEx5}",
        Count = 5,
        Cx_pil = 0.00544,
        Elements = { {
            IsAdapter = true,
            ShapeName = "MER-5E"
          }, {
            ShapeName = "MK-82_Snakeye",
            connector_name = "Point_Pilon_01"
          }, {
            ShapeName = "MK-82_Snakeye",
            connector_name = "Point_Pilon_02"
          }, {
            ShapeName = "MK-82_Snakeye",
            connector_name = "Point_Pilon_03"
          }, {
            ShapeName = "MK-82_Snakeye",
            connector_name = "Point_Pilon_04"
          }, {
            ShapeName = "MK-82_Snakeye",
            connector_name = "Point_Pilon_05"
          } },
        Picture = "mk82AIR.png",
        Weight = 1250.7,
        attribute = { 4, 5, 32, 179 },
        displayName = "5 Mk-82 SnakeEye",
        wsTypeOfWeapon = { 4, 5, 9, 79 }
      }, {
        CLSID = "{CBU-52B}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "SUU-30H"
          } },
        Picture = "CBU.png",
        Weight = 356,
        attribute = { 4, 5, 38, 93 },
        displayName = "CBU-52B"
      } },
    Name = "BOMBS"
  }, {
    CLSID = "{3AB1001C-D1FD-4862-91DC-AD97A56EA01A}",
    DisplayName = "MISSILES",
    Launchers = { {
        CLSID = "TOW",
        Elements = {},
        attribute = { 4, 4, 8, 130 },
        displayName = "BGM-71 TOW"
      }, {
        CLSID = "9M14",
        Elements = {},
        attribute = { 4, 4, 11, 127 },
        displayName = "AT-3 SAGGER"
      }, {
        CLSID = "9M133",
        Elements = {},
        attribute = { 4, 4, 11, 153 },
        displayName = "AT-14 KORNET"
      }, {
        CLSID = "9╠111",
        Elements = {},
        attribute = { 4, 4, 11, 128 },
        displayName = "AT-4 SPIGOT"
      }, {
        CLSID = "9╠117",
        Elements = {},
        attribute = { 4, 4, 11, 129 },
        displayName = "AT-10 SABBER"
      }, {
        CLSID = "REFLEX_9╠119",
        Elements = {},
        attribute = { 4, 4, 11, 156 },
        displayName = "9M119 Reflex"
      }, {
        CLSID = "SVIR_9╠119",
        Elements = {},
        attribute = { 4, 4, 11, 157 },
        displayName = "9M119 Svir"
      }, {
        CLSID = "9M311",
        Elements = {},
        attribute = { 4, 4, 34, 90 },
        displayName = "SA-19 GRISON"
      }, {
        CLSID = "9╠38",
        Elements = {},
        attribute = { 4, 4, 34, 87 },
        displayName = "SA-11 GADFLY"
      }, {
        CLSID = "9╠37",
        Elements = {},
        attribute = { 4, 4, 34, 88 },
        displayName = "SA-13 GOPHER"
      }, {
        CLSID = "9M331",
        Elements = {},
        attribute = { 4, 4, 34, 89 },
        displayName = "SA-15 GAUNTLET"
      }, {
        CLSID = "9M31",
        Elements = {},
        attribute = { 4, 4, 34, 86 },
        displayName = "SA-9 GASKIN"
      }, {
        CLSID = "9M33",
        Elements = {},
        attribute = { 4, 4, 34, 85 },
        displayName = "SA-8 GECKO"
      }, {
        CLSID = "MIM_72",
        Elements = {},
        attribute = { 4, 4, 34, 137 },
        displayName = "M48 CHAPARRAL"
      }, {
        CLSID = "MIM_104",
        Elements = {},
        attribute = { 4, 4, 34, 92 },
        displayName = "M901 PATRIOT"
      }, {
        CLSID = "ROLAND",
        Elements = {},
        attribute = { 4, 4, 34, 99 },
        displayName = "ROLAND"
      }, {
        CLSID = "SEASPARROW",
        Elements = {},
        attribute = { 4, 4, 34, 28 },
        displayName = "SEASPARROW"
      }, {
        CLSID = "5V55",
        Elements = {},
        attribute = { 4, 4, 34, 80 },
        displayName = "5V55"
      }, {
        CLSID = "48N6E2",
        Elements = {},
        attribute = { 4, 4, 34, 81 },
        displayName = "48N6E2"
      }, {
        CLSID = "3M45",
        Elements = {},
        attribute = { 4, 4, 11, 120, "Anti-Ship missiles" },
        displayName = "SS-N-19 SHIPWRECK"
      }, {
        CLSID = "4╩80",
        Elements = {},
        attribute = { 4, 4, 11, 119, "Anti-Ship missiles" },
        displayName = "SS-N-12 SANDBOX"
      }, {
        CLSID = "SM2",
        Elements = {},
        attribute = { 4, 4, 34, 79 },
        displayName = "SM2"
      }, {
        CLSID = "AGM_84",
        Elements = {},
        attribute = { 4, 4, 11, 126 },
        displayName = "AGM-84 HARPOON"
      }, {
        CLSID = "BGM_109",
        Elements = {},
        attribute = { 4, 4, 11, 125 },
        displayName = "BGM-109 TOMAHAWK"
      }, {
        CLSID = "FIM_92",
        Elements = {},
        attribute = { 4, 4, 34, 93 },
        displayName = "STINGER"
      }, {
        CLSID = "9╠39",
        Elements = {},
        attribute = { 4, 4, 34, 91 },
        displayName = "SA-18 GROUSE"
      }, {
        CLSID = "{12429ECF-03F0-4DF6-BCBD-5D38B6343DE1}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "X-22"
          } },
        NatoName = "(AS-4A)",
        Picture = "kh22.png",
        Weight = 6800,
        attribute = { 4, 4, 8, 41, "Anti-Ship missiles" },
        displayName = "Kh-22N"
      }, {
        CLSID = "{9F390892-E6F9-42C9-B84E-1136A881DCB2}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "X-23L"
          } },
        NatoName = "(AS-7)",
        Picture = "KAB500.png",
        Weight = 288,
        attribute = { 4, 4, 8, 73 },
        displayName = "Kh-23L"
      }, {
        CLSID = "{3468C652-E830-4E73-AFA9-B5F260AB7C3D}",
        Count = 1,
        Cx_pil = 0.001,
        Elements = <34>{ {
            Position = { 0, 0, 0 },
            ShapeName = "AKU-58"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { -0.482, -0.237, 0 },
            ShapeName = "X-29L"
          } },
        Picture = "kh29L.png",
        Weight = 747,
        attribute = <35>{ 4, 4, 32, 93 },
        displayName = "Kh-29L",
        ejectImpulse = 2000,
        wsTypeOfWeapon = <36>{ 4, 4, 8, 49 }
      }, {
        CLSID = "{D4A8D9B9-5C45-42e7-BBD2-0E54F8308432}",
        Count = 1,
        Cx_pil = 0.001,
        Elements = <table 34>,
        Picture = "kh29L.png",
        Weight = 747,
        attribute = <table 35>,
        displayName = "Kh-29L",
        ejectImpulse = 2000,
        wsTypeOfWeapon = <table 36>
      }, {
        CLSID = "{B4FC81C9-B861-4E87-BBDC-A1158E648EBF}",
        Count = 1,
        Cx_pil = 0.001,
        Elements = <37>{ {
            Position = { 0, 0, 0 },
            ShapeName = "AKU-58"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { -0.482, -0.237, 0 },
            ShapeName = "X-29T"
          } },
        Picture = "kh29T.png",
        Weight = 760,
        attribute = <38>{ 4, 4, 32, 92 },
        displayName = "Kh-29T",
        ejectImpulse = 2000,
        wsTypeOfWeapon = <39>{ 4, 4, 8, 75 }
      }, {
        CLSID = "{601C99F7-9AF3-4ed7-A565-F8B8EC0D7AAC}",
        Count = 1,
        Cx_pil = 0.001,
        Elements = <table 37>,
        Picture = "kh29T.png",
        Weight = 760,
        attribute = <table 38>,
        displayName = "Kh-29T",
        ejectImpulse = 2000,
        wsTypeOfWeapon = <table 39>
      }, {
        CLSID = "{FE382A68-8620-4AC0-BDF5-709BFE3977D7}",
        Count = 1,
        Cx_pil = 0.001,
        Elements = <40>{ {
            Position = { 0, 0, 0 },
            ShapeName = "AKU-58"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { -0.428, -0.234, 0 },
            ShapeName = "X-58"
          } },
        NatoName = "(AS-11)",
        Picture = "kh58u.png",
        Weight = 730,
        attribute = <41>{ 4, 4, 32, 91 },
        displayName = "Kh-58U",
        ejectImpulse = 2000,
        wsTypeOfWeapon = <42>{ 4, 4, 8, 46 }
      }, {
        CLSID = "{B5CA9846-776E-4230-B4FD-8BCC9BFB1676}",
        Count = 1,
        Cx_pil = 0.001,
        Elements = <table 40>,
        NatoName = "(AS-11)",
        Picture = "kh58u.png",
        Weight = 730,
        attribute = <table 41>,
        displayName = "Kh-58U",
        ejectImpulse = 2000,
        wsTypeOfWeapon = <table 42>
      }, {
        CLSID = "{4D13E282-DF46-4B23-864A-A9423DFDE504}",
        Count = 1,
        Cx_pil = 0.001,
        Elements = <43>{ {
            Position = { 0, 0, 0 },
            ShapeName = "AKU-58"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.245, -0.237, 0 },
            ShapeName = "X-31"
          } },
        NatoName = "(AS-17)",
        Picture = "kh31a.png",
        Weight = 690,
        attribute = <44>{ 4, 4, 32, 96 },
        displayName = "Kh-31A",
        wsTypeOfWeapon = <45>{ 4, 4, 8, 53 }
      }, {
        CLSID = "{4D13E282-DF46-4B23-864A-A9423DFDE50A}",
        Count = 1,
        Cx_pil = 0.001,
        Elements = <table 43>,
        NatoName = "(AS-17)",
        Picture = "kh31a.png",
        Weight = 690,
        attribute = <table 44>,
        displayName = "Kh-31A",
        wsTypeOfWeapon = <table 45>
      }, {
        CLSID = "{D8F2C90B-887B-4B9E-9FE2-996BC9E9AF03}",
        Count = 1,
        Cx_pil = 0.001,
        Elements = <46>{ {
            Position = { 0, 0, 0 },
            ShapeName = "AKU-58"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.245, -0.237, 0 },
            ShapeName = "X-31"
          } },
        NatoName = "(AS-17)",
        Picture = "kh31p.png",
        Weight = 690,
        attribute = <47>{ 4, 4, 32, 97 },
        displayName = "Kh-31P",
        wsTypeOfWeapon = <48>{ 4, 4, 8, 76 }
      }, {
        CLSID = "{D8F2C90B-887B-4B9E-9FE2-996BC9E9AF0A}",
        Count = 1,
        Cx_pil = 0.001,
        Elements = <table 46>,
        NatoName = "(AS-17)",
        Picture = "kh31p.png",
        Weight = 690,
        attribute = <table 47>,
        displayName = "Kh-31P",
        wsTypeOfWeapon = <table 48>
      }, {
        CLSID = "{6DADF342-D4BA-4D8A-B081-BA928C4AF86D}",
        Count = 1,
        Cx_pil = 0.001,
        Elements = <49>{ {
            ShapeName = "APU-68"
          }, {
            Position = { 0, -0.135, 0 },
            ShapeName = "X-25ML"
          } },
        Picture = "kh25ml.png",
        Weight = 360,
        attribute = <50>{ 4, 4, 32, 99 },
        displayName = "Kh-25ML",
        wsTypeOfWeapon = <51>{ 4, 4, 8, 45 }
      }, {
        CLSID = "{79D73885-0801-45a9-917F-C90FE1CE3DFC}",
        Count = 1,
        Cx_pil = 0.001,
        Elements = <table 49>,
        Picture = "kh25ml.png",
        Weight = 360,
        attribute = <table 50>,
        displayName = "Kh-25ML",
        wsTypeOfWeapon = <table 51>
      }, {
        CLSID = "{E86C5AA5-6D49-4F00-AD2E-79A62D6DDE26}",
        Count = 1,
        Cx_pil = 0.001,
        Elements = <52>{ {
            ShapeName = "APU-68"
          }, {
            Position = { 0, -0.135, 0 },
            ShapeName = "X-25MP"
          } },
        Picture = "kh25mpu.png",
        Weight = 370,
        attribute = <53>{ 4, 4, 32, 100 },
        displayName = "Kh-25MPU",
        wsTypeOfWeapon = <54>{ 4, 4, 8, 47 }
      }, {
        CLSID = "{752AF1D2-EBCC-4bd7-A1E7-2357F5601C70}",
        Count = 1,
        Cx_pil = 0.001,
        Elements = <table 52>,
        Picture = "kh25mpu.png",
        Weight = 370,
        attribute = <table 53>,
        displayName = "Kh-25MPU",
        wsTypeOfWeapon = <table 54>
      }, {
        CLSID = "{292960BB-6518-41AC-BADA-210D65D5073C}",
        Count = 1,
        Cx_pil = 0.001,
        Elements = { {
            ShapeName = "APU-68"
          }, {
            Position = { 0, -0.135, 0 },
            ShapeName = "X-25MR"
          } },
        NatoName = "(AS-12)",
        Picture = "kh25mr.png",
        Weight = 360,
        attribute = { 4, 4, 32, 170 },
        displayName = "Kh-25MR",
        wsTypeOfWeapon = { 4, 4, 8, 74 }
      }, {
        CLSID = "{2234F529-1D57-4496-8BB0-0150F9BDBBD2}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "X-35"
          } },
        NatoName = "(AS-20)",
        Picture = "kh35.png",
        Required = { "{F4920E62-A99A-11d8-9897-000476191836}" },
        Weight = 480,
        attribute = { 4, 4, 8, 55 },
        displayName = "Kh-35"
      }, {
        CLSID = "{3F26D9C5-5CC3-4E42-BC79-82FAA54E9F26}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "X-41"
          } },
        NatoName = "(SS-N-22)",
        Picture = "kh41.png",
        Weight = 4500,
        attribute = { 4, 4, 8, 56, "Anti-Ship missiles" },
        displayName = "Kh-41"
      }, {
        CLSID = "{40AB87E8-BEFB-4D85-90D9-B2753ACF9514}",
        Count = 1,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "AKU-58"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { -0.428, -0.234, 0 },
            ShapeName = "X-59M"
          } },
        NatoName = "(AS-18)",
        Picture = "kh59m.png",
        Weight = 850,
        attribute = { 4, 4, 32, 171, "Cruise missiles" },
        displayName = "Kh-59M",
        wsTypeOfWeapon = { 4, 4, 8, 54 }
      }, {
        CLSID = "{BADAF2DE-68B5-472A-8AAC-35BAEFF6B4A1}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "X-65"
          } },
        NatoName = "(AS-15B)",
        Picture = "kh65.png",
        Weight = 1250,
        attribute = { 4, 4, 8, 51, "Cruise missiles" },
        displayName = "Kh-65"
      }, {
        CLSID = "{F789E86A-EE2E-4E6B-B81E-D5E5F903B6ED}",
        Count = 8,
        Cx_pil = 0.001,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "9M120"
          }, {
            Position = { 0, -0.235, -0.263 }
          }, {
            Position = { 0, -0.235, -0.116 }
          }, {
            Position = { 0, -0.235, 0.116 }
          }, {
            Position = { 0, -0.235, 0.263 }
          }, {
            Position = { 0, -0.474, -0.263 }
          }, {
            Position = { 0, -0.474, -0.116 }
          }, {
            Position = { 0, -0.474, 0.116 }
          }, {
            Position = { 0, -0.474, 0.263 }
          } },
        NatoName = "(AT-16)",
        Picture = "APU8.png",
        Weight = 404,
        attribute = { 4, 4, 32, 47 },
        displayName = "APU-8 - 8 9A4172 Vikhr",
        wsTypeOfWeapon = { 4, 4, 8, 58 }
      }, {
        CLSID = "{A6FD14D3-6D30-4C85-88A7-8D17BEE120E2}",
        Count = 6,
        Cx_pil = 0.001,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "APU-6"
          }, {
            Position = { 0, -0.328, 0.262 }
          }, {
            Position = { 0, -0.328, 0.103 }
          }, {
            Position = { 0, -0.146, -0.17 }
          }, {
            Position = { 0, -0.146, 0.17 }
          }, {
            Position = { 0, -0.328, -0.262 }
          }, {
            Position = { 0, -0.328, -0.103 }
          } },
        NatoName = "(AT-16)",
        Picture = "APU6.png",
        Weight = 330,
        attribute = { 4, 4, 32, 86 },
        displayName = "APU-6 - 6 9A4172 Vikhr",
        wsTypeOfWeapon = { 4, 4, 8, 58 }
      }, {
        CLSID = "{B919B0F4-7C25-455E-9A02-CEA51DB895E3}",
        Count = 2,
        Cx_pil = 0.001,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "9M114-PILON"
          }, {
            Position = { 1, 0.05, -0.225 },
            Rotation = { 0, 0, 1 },
            ShapeName = ""
          }, {
            Position = { 1, 0.05, 0.225 },
            Rotation = { 0, 0, 1 },
            ShapeName = ""
          } },
        NatoName = "AT-6",
        Picture = "apu2.png",
        Weight = 230,
        attribute = { 4, 4, 32, 60 },
        displayName = "9M114 Shturm-V - 2",
        wsTypeOfWeapon = { 4, 4, 8, 48 }
      }, {
        CLSID = "{3EA17AB0-A805-4D9E-8732-4CE00CB00F17}",
        Count = 4,
        Cx_pil = 0.0018,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "TOW-PILON"
          } },
        Picture = "apu4.png",
        Weight = 250,
        attribute = { 4, 4, 32, 64 },
        displayName = "BGM-71D Tow * 4",
        wsTypeOfWeapon = { 4, 4, 8, 130 }
      }, {
        CLSID = "{E6747967-B1F0-4C77-977B-AB2E6EB0C102}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "T-ALARM"
          } },
        Picture = "ALARM.png",
        Weight = 268,
        attribute = { 4, 4, 8, 72 },
        displayName = "ALARM"
      }, {
        CLSID = "{07BE2D19-0E48-4B0B-91DA-5F6C8F9E3C75}",
        Count = 2,
        Cx_pil = 0.001,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = ""
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 1.32, 0, 0 },
            ShapeName = "T-ALARM"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { -1.32, 0, 0 },
            ShapeName = "T-ALARM"
          } },
        Picture = "ALARM.png",
        Weight = 200,
        attribute = { 4, 4, 32, 56 },
        displayName = "ALARM*2",
        wsTypeOfWeapon = { 4, 4, 8, 72 }
      }, {
        CLSID = "{3E6B632D-65EB-44D2-9501-1C2D04515404}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "AGM-45"
          } },
        Picture = "agm45.png",
        Weight = 177,
        attribute = { 4, 4, 8, 60 },
        displayName = "AGM-45B"
      }, {
        CLSID = "{69DC8AE7-8F77-427B-B8AA-B19D3F478B65}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "AGM-65B"
          } },
        Picture = "agm65.png",
        Weight = 360,
        attribute = { 4, 4, 8, 61 },
        displayName = "AGM-65K"
      }, {
        CLSID = "{69DC8AE7-8F77-427B-B8AA-B19D3F478B66}",
        Count = 1,
        Cx_pil = 0.0009765625,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "LAU-117"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.18, -0.078, 0 },
            ShapeName = "AGM-65K"
          } },
        Picture = "agm65.png",
        Weight = 419,
        adapter_type = { 4, 15, 47, 97 },
        attribute = { 4, 4, 32, 108 },
        displayName = "LAU-117,AGM-65K",
        wsTypeOfWeapon = <55>{ 4, 4, 8, 61 }
      }, {
        CLSID = "{D7670BC7-881B-4094-906C-73879CF7EB28}",
        Count = 2,
        Cx_pil = 0.003840125,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "LAU-88"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.29, -0.31, 0 },
            Rotation = { 0, 0, 0 },
            ShapeName = "AGM-65K"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.29, -0.085, -0.275 },
            Rotation = { 90, 0, 0 },
            ShapeName = "AGM-65K"
          } },
        Picture = "agm65.png",
        Weight = 931,
        adapter_type = { 4, 15, 47, 4 },
        attribute = { 4, 4, 32, 65 },
        displayName = "LAU-88,AGM-65K*2",
        kind_of_shipping = 1,
        wsTypeOfWeapon = <table 55>
      }, {
        CLSID = "{D7670BC7-881B-4094-906C-73879CF7EB27}",
        Count = 2,
        Cx_pil = 0.003840125,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "LAU-88"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.29, -0.31, 0 },
            Rotation = { 0, 0, 0 },
            ShapeName = "AGM-65K"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.29, -0.085, 0.275 },
            Rotation = { -90, 0, 0 },
            ShapeName = "AGM-65K"
          } },
        Picture = "agm65.png",
        Weight = 931,
        adapter_type = { 4, 15, 47, 4 },
        attribute = { 4, 4, 32, 105 },
        displayName = "LAU-88,AGM-65K*2",
        kind_of_shipping = 1,
        wsTypeOfWeapon = <table 55>
      }, {
        CLSID = "{907D835F-E650-4154-BAFD-C656882555C0}",
        Count = 3,
        Cx_pil = 0.0048166875,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "LAU-88"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.29, -0.31, 0 },
            Rotation = { 0, 0, 0 },
            ShapeName = "AGM-65K"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.29, -0.085, 0.275 },
            Rotation = { -90, 0, 0 },
            ShapeName = "AGM-65K"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.29, -0.085, -0.275 },
            Rotation = { 90, 0, 0 },
            ShapeName = "AGM-65K"
          } },
        Picture = "agm65.png",
        Weight = 1291,
        adapter_type = { 4, 15, 47, 4 },
        attribute = { 4, 4, 32, 33 },
        displayName = "LAU-88,AGM-65K*3",
        kind_of_shipping = 1,
        wsTypeOfWeapon = <table 55>
      }, {
        CLSID = "LAU_117_CATM_65K",
        Count = 1,
        Cx_pil = 0.0009765625,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "LAU-117"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.18, -0.078, 0 },
            ShapeName = "CATM-65K"
          } },
        Picture = "agm65.png",
        Weight = 419,
        adapter_type = { 4, 15, 47, 97 },
        attribute = { 4, 4, 32, 141 },
        displayName = "LAU-117,CATM-65K",
        wsTypeOfWeapon = { 4, 4, 101, 142 }
      }, {
        CLSID = "{444BA8AE-82A7-4345-842E-76154EFCCA47}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "AGM-65D"
          } },
        Picture = "agm65.png",
        Weight = 218,
        attribute = { 4, 4, 8, 77 },
        displayName = "AGM-65D"
      }, {
        CLSID = "{444BA8AE-82A7-4345-842E-76154EFCCA46}",
        Count = 1,
        Cx_pil = 0.0009765625,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "LAU-117"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.18, -0.078, 0 },
            ShapeName = "AGM-65D"
          } },
        Picture = "agm65.png",
        Weight = 277,
        adapter_type = { 4, 15, 47, 97 },
        attribute = { 4, 4, 32, 109 },
        displayName = "LAU-117,AGM-65D",
        wsTypeOfWeapon = <56>{ 4, 4, 8, 77 }
      }, {
        CLSID = "LAU_88_AGM_65D_ONE",
        Count = 1,
        Cx_pil = 0.0028635625,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "LAU-88"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.29, -0.31, 0 },
            Rotation = { 0, 0, 0 },
            ShapeName = "AGM-65D"
          } },
        Picture = "agm65.png",
        Weight = 429,
        adapter_type = { 4, 15, 47, 4 },
        attribute = { 4, 4, 32, 145 },
        displayName = "LAU-88,AGM-65D*1",
        kind_of_shipping = 1,
        wsTypeOfWeapon = <table 56>
      }, {
        CLSID = "{E6A6262A-CA08-4B3D-B030-E1A993B98452}",
        Count = 2,
        Cx_pil = 0.003840125,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "LAU-88"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.29, -0.31, 0 },
            Rotation = { 0, 0, 0 },
            ShapeName = "AGM-65D"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.29, -0.085, -0.275 },
            Rotation = { 90, 0, 0 },
            ShapeName = "AGM-65D"
          } },
        Picture = "agm65.png",
        Weight = 647,
        adapter_type = { 4, 15, 47, 4 },
        attribute = { 4, 4, 32, 66 },
        displayName = "LAU-88,AGM-65D*2",
        kind_of_shipping = 1,
        wsTypeOfWeapon = <table 56>
      }, {
        CLSID = "{E6A6262A-CA08-4B3D-B030-E1A993B98453}",
        Count = 2,
        Cx_pil = 0.003840125,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "LAU-88"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.29, -0.31, 0 },
            Rotation = { 0, 0, 0 },
            ShapeName = "AGM-65D"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.29, -0.085, 0.275 },
            Rotation = { -90, 0, 0 },
            ShapeName = "AGM-65D"
          } },
        Picture = "agm65.png",
        Weight = 647,
        adapter_type = { 4, 15, 47, 4 },
        attribute = { 4, 4, 32, 106 },
        displayName = "LAU-88,AGM-65D*2",
        kind_of_shipping = 1,
        wsTypeOfWeapon = <table 56>
      }, {
        CLSID = "{DAC53A2F-79CA-42FF-A77A-F5649B601308}",
        Count = 3,
        Cx_pil = 0.0048166875,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "LAU-88"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.29, -0.31, 0 },
            Rotation = { 0, 0, 0 },
            ShapeName = "AGM-65D"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.29, -0.085, 0.275 },
            Rotation = { -90, 0, 0 },
            ShapeName = "AGM-65D"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.29, -0.085, -0.275 },
            Rotation = { 90, 0, 0 },
            ShapeName = "AGM-65D"
          } },
        Picture = "agm65.png",
        Weight = 865,
        adapter_type = { 4, 15, 47, 4 },
        attribute = { 4, 4, 32, 48 },
        displayName = "LAU-88,AGM-65D*3",
        kind_of_shipping = 1,
        wsTypeOfWeapon = <table 56>
      }, {
        CLSID = "LAU_117_TGM_65D",
        Count = 1,
        Cx_pil = 0.0009765625,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "LAU-117"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.18, -0.078, 0 },
            ShapeName = "TGM-65D"
          } },
        Picture = "agm65.png",
        Weight = 277,
        adapter_type = { 4, 15, 47, 97 },
        attribute = { 4, 4, 32, 138 },
        displayName = "LAU-117,TGM-65D",
        wsTypeOfWeapon = { 4, 4, 101, 141 }
      }, {
        CLSID = "{F16A4DE0-116C-4A71-97F0-2CF85B0313EF}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "AGM-65E"
          } },
        Picture = "agm65.png",
        Weight = 286,
        attribute = { 4, 4, 8, 70 },
        displayName = "AGM-65E"
      }, {
        CLSID = "{F16A4DE0-116C-4A71-97F0-2CF85B0313EC}",
        Count = 1,
        Cx_pil = 0.0009765625,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "LAU-117"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.18, -0.078, 0 },
            ShapeName = "AGM-65E"
          } },
        Picture = "agm65.png",
        Weight = 345,
        adapter_type = { 4, 15, 47, 97 },
        attribute = { 4, 4, 32, 110 },
        displayName = "LAU-117,AGM-65E",
        wsTypeOfWeapon = <57>{ 4, 4, 8, 70 }
      }, {
        CLSID = "{2CC29C7A-E863-411C-8A6E-BD6F0E730548}",
        Count = 2,
        Cx_pil = 0.003840125,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "LAU-88"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.29, -0.31, 0 },
            Rotation = { 0, 0, 0 },
            ShapeName = "AGM-65E"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.29, -0.085, -0.275 },
            Rotation = { 90, 0, 0 },
            ShapeName = "AGM-65E"
          } },
        Picture = "agm65.png",
        Weight = 783,
        adapter_type = { 4, 15, 47, 4 },
        attribute = { 4, 4, 32, 104 },
        displayName = "LAU-88,AGM-65E*2",
        kind_of_shipping = 1,
        wsTypeOfWeapon = <table 57>
      }, {
        CLSID = "{2CC29C7A-E863-411C-8A6E-BD6F0E730547}",
        Count = 2,
        Cx_pil = 0.003840125,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "LAU-88"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.29, -0.31, 0 },
            Rotation = { 0, 0, 0 },
            ShapeName = "AGM-65E"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.29, -0.085, 0.275 },
            Rotation = { -90, 0, 0 },
            ShapeName = "AGM-65E"
          } },
        Picture = "agm65.png",
        Weight = 783,
        adapter_type = { 4, 15, 47, 4 },
        attribute = { 4, 4, 32, 107 },
        displayName = "LAU-88,AGM-65E*2",
        kind_of_shipping = 1,
        wsTypeOfWeapon = <table 57>
      }, {
        CLSID = "{71AAB9B8-81C1-4925-BE50-1EF8E9899271}",
        Count = 3,
        Cx_pil = 0.0048166875,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "LAU-88"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.29, -0.31, 0 },
            Rotation = { 0, 0, 0 },
            ShapeName = "AGM-65E"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.29, -0.085, 0.275 },
            Rotation = { -90, 0, 0 },
            ShapeName = "AGM-65E"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.29, -0.085, -0.275 },
            Rotation = { 90, 0, 0 },
            ShapeName = "AGM-65E"
          } },
        Picture = "agm65.png",
        Weight = 1069,
        adapter_type = { 4, 15, 47, 4 },
        attribute = { 4, 4, 32, 49 },
        displayName = "LAU-88,AGM-65E*3",
        kind_of_shipping = 1,
        wsTypeOfWeapon = <table 57>
      }, {
        CLSID = "LAU_117_AGM_65H",
        Count = 1,
        Cx_pil = 0.0009765625,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "LAU-117"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.18, -0.078, 0 },
            ShapeName = "AGM-65H"
          } },
        Picture = "agm65.png",
        Weight = 267,
        adapter_type = { 4, 15, 47, 97 },
        attribute = { 4, 4, 32, 125 },
        displayName = "LAU-117,AGM-65H",
        wsTypeOfWeapon = <58>{ 4, 4, 8, 138 }
      }, {
        CLSID = "LAU_88_AGM_65H",
        Count = 1,
        Cx_pil = 0.0028635625,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "LAU-88"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.29, -0.31, 0 },
            Rotation = { 0, 0, 0 },
            ShapeName = "AGM-65H"
          } },
        Picture = "agm65.png",
        Weight = 419,
        adapter_type = { 4, 15, 47, 4 },
        attribute = { 4, 4, 32, 134 },
        displayName = "LAU-88,AGM-65H*1",
        kind_of_shipping = 1,
        wsTypeOfWeapon = <table 58>
      }, {
        CLSID = "LAU_88_AGM_65H_2_R",
        Count = 2,
        Cx_pil = 0.003840125,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "LAU-88"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.29, -0.31, 0 },
            Rotation = { 0, 0, 0 },
            ShapeName = "AGM-65H"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.29, -0.085, 0.275 },
            Rotation = { -90, 0, 0 },
            ShapeName = "AGM-65H"
          } },
        Picture = "agm65.png",
        Weight = 627,
        adapter_type = { 4, 15, 47, 4 },
        attribute = { 4, 4, 32, 136 },
        displayName = "LAU-88,AGM-65H*2",
        kind_of_shipping = 1,
        wsTypeOfWeapon = <table 58>
      }, {
        CLSID = "LAU_88_AGM_65H_2_L",
        Count = 2,
        Cx_pil = 0.003840125,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "LAU-88"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.29, -0.31, 0 },
            Rotation = { 0, 0, 0 },
            ShapeName = "AGM-65H"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.29, -0.085, -0.275 },
            Rotation = { 90, 0, 0 },
            ShapeName = "AGM-65H"
          } },
        Picture = "agm65.png",
        Weight = 627,
        adapter_type = { 4, 15, 47, 4 },
        attribute = { 4, 4, 32, 135 },
        displayName = "LAU-88,AGM-65H*2",
        kind_of_shipping = 1,
        wsTypeOfWeapon = <table 58>
      }, {
        CLSID = "LAU_88_AGM_65H_3",
        Count = 3,
        Cx_pil = 0.0048166875,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "LAU-88"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.29, -0.31, 0 },
            Rotation = { 0, 0, 0 },
            ShapeName = "AGM-65H"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.29, -0.085, 0.275 },
            Rotation = { -90, 0, 0 },
            ShapeName = "AGM-65H"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.29, -0.085, -0.275 },
            Rotation = { 90, 0, 0 },
            ShapeName = "AGM-65H"
          } },
        Picture = "agm65.png",
        Weight = 835,
        adapter_type = { 4, 15, 47, 4 },
        attribute = { 4, 4, 32, 137 },
        displayName = "LAU-88,AGM-65H*3",
        kind_of_shipping = 1,
        wsTypeOfWeapon = <table 58>
      }, {
        CLSID = "TGM_65H",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "AGM-65H"
          } },
        Picture = "agm65.png",
        Weight = 208,
        attribute = { 4, 4, 101, 154 },
        displayName = "TGM-65H"
      }, {
        CLSID = "LAU_117_TGM_65H",
        Count = 1,
        Cx_pil = 0.0009765625,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "LAU-117"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.18, -0.078, 0 },
            ShapeName = "TGM-65H"
          } },
        Picture = "agm65.png",
        Weight = 267,
        adapter_type = { 4, 15, 47, 97 },
        attribute = { 4, 4, 32, 140 },
        displayName = "LAU-117,TGM-65H",
        wsTypeOfWeapon = { 4, 4, 101, 154 }
      }, {
        CLSID = "LAU_117_TGM_65G",
        Count = 1,
        Cx_pil = 0.0009765625,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "LAU-117"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.18, -0.078, 0 },
            ShapeName = "TGM-65G"
          } },
        Picture = "agm65.png",
        Weight = 360,
        adapter_type = { 4, 15, 47, 97 },
        attribute = { 4, 4, 32, 139 },
        displayName = "LAU-117,TGM-65G",
        wsTypeOfWeapon = { 4, 4, 101, 140 }
      }, {
        CLSID = "LAU_117_AGM_65G",
        Count = 1,
        Cx_pil = 0.0009765625,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "LAU-117"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.18, -0.078, 0 },
            ShapeName = "AGM-65G"
          } },
        Picture = "agm65.png",
        Weight = 360,
        adapter_type = { 4, 15, 47, 97 },
        attribute = { 4, 4, 32, 126 },
        displayName = "LAU-117,AGM-65G",
        wsTypeOfWeapon = { 4, 4, 8, 139 }
      }, {
        CLSID = "{8B7CADF9-4954-46B3-8CFB-93F2F5B90B03}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "AGM-84"
          } },
        Picture = "agm84a.png",
        Weight = 661.5,
        attribute = { 4, 4, 8, 62, "Anti-Ship missiles" },
        displayName = "AGM-84A"
      }, {
        CLSID = "{AF42E6DF-9A60-46D8-A9A0-1708B241AADB}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "AGM-84E"
          } },
        Picture = "agm84a.png",
        Required = { "{6C0D552F-570B-42ff-9F6D-F10D9C1D4E1C}" },
        Weight = 628,
        attribute = { 4, 4, 8, 63, "Cruise missiles" },
        displayName = "AGM-84E"
      }, {
        CLSID = "{769A15DF-6AFB-439F-9B24-5B7A45C59D16}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "AGM-86"
          } },
        Picture = "AGM86.png",
        Weight = 1950,
        attribute = { 4, 4, 8, 64, "Cruise missiles" },
        displayName = "AGM-86C"
      }, {
        CLSID = "{B06DD79A-F21E-4EB9-BD9D-AB3844618C9C}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "AGM-88"
          } },
        Picture = "agm88.png",
        Required = { "{8C3F26A2-FA0F-11d5-9190-00A0249B6F00}" },
        Weight = 361,
        attribute = { 4, 4, 8, 65 },
        displayName = "AGM-88C"
      }, {
        CLSID = "AGM114x2_OH_58",
        Count = 2,
        Cx_pil = 0.001,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "M272_AGM114"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.149, -0.174, 0.1572 },
            ShapeName = "AGM114"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.149, -0.174, -0.1572 },
            ShapeName = "AGM114"
          } },
        Picture = "agm114.png",
        Weight = 250,
        attribute = { 4, 4, 32, 119 },
        displayName = "AGM-114K * 2",
        wsTypeOfWeapon = { 4, 4, 8, 59 }
      }, {
        CLSID = "{88D18A5E-99C8-4B04-B40B-1C02F2018B6E}",
        Count = 4,
        Cx_pil = 0.00208,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "M299_AGM114"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.063, -0.1679, 0.1572 },
            ShapeName = "AGM114"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.063, -0.1679, -0.1572 },
            ShapeName = "AGM114"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.063, -0.498, 0.1572 },
            ShapeName = "AGM114"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0.063, -0.498, -0.1572 },
            ShapeName = "AGM114"
          } },
        Picture = "agm114.png",
        Weight = 250,
        attribute = { 4, 4, 32, 59 },
        displayName = "AGM-114K * 4",
        wsTypeOfWeapon = { 4, 4, 8, 39 }
      }, {
        CLSID = "{0180F983-C14A-11d8-9897-000476191836}",
        Count = 1,
        Cx_pil = 0.001,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "C-25PU"
          }, {
            DrawArgs = { { 2, 1 } },
            Position = { -0.129395, -0.247116, 0 },
            ShapeName = "S-25L",
            connector_name = "tube_1"
          } },
        NatoName = "(S-25)",
        Picture = "S25L.png",
        Weight = 500,
        attribute = { 4, 4, 32, 87 },
        displayName = "S-25L",
        wsTypeOfWeapon = { 4, 4, 8, 133 }
      }, {
        CLSID = "{7B8DCEB4-820B-4015-9B48-1028A4195692}",
        Elements = { {
            DrawArgs = { { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "penquin"
          } },
        Picture = "AGM119.png",
        Weight = 300,
        attribute = { 4, 4, 8, 40 },
        displayName = "AGM-119B Penguin"
      }, {
        CLSID = "{7210496B-7B81-4B52-80D6-8529ECF847CD}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "T-KORMORAN"
          } },
        Picture = "S25.png",
        Weight = 660,
        attribute = { 4, 4, 8, 78, "Anti-Ship missiles" },
        displayName = "Kormoran"
      }, {
        CLSID = "{1461CD18-429A-42A9-A21F-4C621ECD4573}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "T-SEA-EAGLE"
          } },
        Picture = "SeaEagle.png",
        Weight = 600,
        attribute = { 4, 4, 8, 66, "Anti-Ship missiles" },
        displayName = "Sea Eagle"
      }, {
        CLSID = "{CD9417DF-455F-4176-A5A2-8C58D61AA00B}",
        Count = 8,
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "X-65"
          } },
        Picture = "kh65.png",
        Weight = 10000,
        attribute = { 4, 4, 8, 51 },
        displayName = "Kh-65*8"
      }, {
        CLSID = "{0290F5DE-014A-4BB1-9843-D717749B1DED}",
        Count = 6,
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "X-65"
          } },
        Picture = "kh65.png",
        Weight = 7500,
        attribute = { 4, 4, 8, 51 },
        displayName = "Kh-65*6"
      }, {
        CLSID = "{C42EE4C3-355C-4B83-8B22-B39430B8F4AE}",
        Count = 6,
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "X-35"
          } },
        Picture = "kh35.png",
        Weight = 2880,
        attribute = { 4, 4, 8, 55 },
        displayName = "Kh-35*6"
      }, {
        CLSID = "{9BCC2A2B-5708-4860-B1F1-053A18442067}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "AGM-154"
          } },
        Picture = "AGM154.png",
        Required = { "{6C0D552F-570B-42ff-9F6D-F10D9C1D4E1C}", "{CAAC1CFD-6745-416B-AFA4-CB57414856D0}" },
        Weight = 484,
        attribute = { 4, 4, 8, 132, "Cruise missiles" },
        displayName = "AGM-154C"
      }, {
        CLSID = "{AABA1A14-78A1-4E85-94DD-463CF75BD9E4}",
        Count = 4,
        Elements = { {
            Position = { 0, -0.35, 0 },
            Rotation = { -0, 0, 0 }
          }, {
            Position = { 0, -2.1431318985079e-017, 0.35 },
            Rotation = { -90.006629525348, 0, 0 }
          }, {
            Position = { 0, 0.35, 4.2862637970157e-017 },
            Rotation = { -180.0132590507, 0, 0 }
          }, {
            Position = { 0, 6.4293956955236e-017, -0.35 },
            Rotation = { -270.01988857604, 0, 0 }
          } },
        Picture = "AGM154.png",
        Weight = 2560,
        attribute = { 4, 4, 8, 132 },
        displayName = "AGM-154C*4"
      }, {
        CLSID = "{22906569-A97F-404B-BA4F-D96DBF94D05E}",
        Count = 20,
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "AGM-86"
          } },
        Picture = "AGM86.png",
        Weight = 39000,
        attribute = { 4, 4, 8, 64 },
        displayName = "AGM-86C*20"
      }, {
        CLSID = "{46ACDCF8-5451-4E26-BDDB-E78D5830E93C}",
        Count = 8,
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "AGM-84"
          } },
        Picture = "agm84a.png",
        Weight = 5292,
        attribute = { 4, 4, 8, 62 },
        displayName = "AGM-84A*8"
      }, {
        CLSID = "{8DCAF3A3-7FCF-41B8-BB88-58DEDA878EDE}",
        Count = 8,
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "AGM-86"
          } },
        Picture = "AGM86.png",
        Weight = 15600,
        attribute = { 4, 4, 8, 64 },
        displayName = "AGM-86C*8"
      }, {
        CLSID = "{45447F82-01B5-4029-A572-9AAD28AF0275}",
        Count = 6,
        Cx_pil = 0.000681,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "B52-MBD_AGM86"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { -2.096, 0.138, 0 },
            ShapeName = "AGM-86"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 4.277, 0.138, 0 },
            ShapeName = "AGM-86"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, -1 } },
            Position = { -2.096, 0.847, 0.838 },
            ShapeName = "AGM-86"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, 1 } },
            Position = { -2.096, 0.847, -0.838 },
            ShapeName = "AGM-86"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, -1 } },
            Position = { 4.277, 0.847, 0.838 },
            ShapeName = "AGM-86"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, 1 } },
            Position = { 4.277, 0.847, -0.838 },
            ShapeName = "AGM-86"
          } },
        Picture = "AGM86.png",
        Weight = 11760,
        attribute = { 4, 4, 32, 67 },
        displayName = "MER*6 AGM-86C",
        wsTypeOfWeapon = { 4, 4, 8, 64 }
      }, {
        CLSID = "{2234F529-1D57-4496-8BB0-0150F9BDBBD3}",
        Count = 1,
        Cx_pil = 0.001,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "AKU-58"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { -0.43, -0.219, 0 },
            ShapeName = "X-35"
          } },
        NatoName = "(AS-20)",
        Picture = "kh35.png",
        Required = { "{F4920E62-A99A-11d8-9897-000476191836}" },
        Weight = 570,
        attribute = { 4, 4, 32, 98 },
        displayName = "Kh-35",
        wsTypeOfWeapon = { 4, 4, 8, 55 }
      }, {
        CLSID = "{B06DD79A-F21E-4EB9-BD9D-AB3844618C93}",
        Count = 1,
        Cx_pil = 0.001,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "lau-118a"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, -0.17, 0 },
            ShapeName = "AGM-88"
          } },
        Picture = "agm88.png",
        Required = { "{8C3F26A2-FA0F-11d5-9190-00A0249B6F00}" },
        Weight = 361,
        attribute = { 4, 4, 32, 89 },
        displayName = "AGM-88C",
        wsTypeOfWeapon = { 4, 4, 8, 65 }
      }, {
        CLSID = "{3E6B632D-65EB-44D2-9501-1C2D04515405}",
        Count = 1,
        Cx_pil = 0.001,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "lau-118a"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, -0.17, 0 },
            ShapeName = "AGM-45"
          } },
        Picture = "agm45.png",
        Weight = 177,
        attribute = { 4, 4, 32, 90 },
        displayName = "AGM-45B",
        wsTypeOfWeapon = { 4, 4, 8, 60 }
      }, {
        CLSID = "{ee368869-c35a-486a-afe7-284beb7c5d52}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "AGM114"
          } },
        Picture = "agm114.png",
        Weight = 65,
        attribute = { 4, 4, 8, 59 },
        displayName = "AGM-114K"
      }, {
        CLSID = "{57232979-8B0F-4db7-8D9A-55197E06B0F5}",
        Count = 8,
        Cx_pil = 0.002,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "9K114_Shturm"
          } },
        NatoName = "AT-6",
        Picture = "apu8.png",
        Weight = 300,
        attribute = { 4, 4, 32, 113 },
        displayName = "9M114 Shturm-V x 8",
        wsTypeOfWeapon = { 4, 4, 8, 48 }
      }, {
        CLSID = "BGM-109B",
        Elements = {},
        attribute = { 4, 4, 11, 125, "Cruise missiles" },
        displayName = "BGM-109B"
      } },
    Name = "MISSILES"
  }, {
    CLSID = "{4C8373AA-83C3-44d1-8C20-35E1C5F850F1}",
    DisplayName = "ROCKETS",
    Launchers = { {
        CLSID = "{637334E4-AB5A-47C0-83A6-51B7F1DF3CD5}",
        Count = 32,
        Cx_pil = 0.00196533203125,
        Elements = { {
            IsAdapter = true,
            ShapeName = "UB-32M1"
          } },
        Picture = "UB32.png",
        Weight = 275,
        attribute = { 4, 7, 32, 2 },
        displayName = "UB-32A - 32 S-5KO",
        wsTypeOfWeapon = { 4, 7, 33, 31 }
      }, {
        CLSID = "{F72F47E5-C83A-4B85-96ED-D3E46671EE9A}",
        Count = 20,
        Cx_pil = 0.00146484375,
        Elements = { {
            IsAdapter = true,
            ShapeName = "B-20"
          } },
        Picture = "B8V20A.png",
        Weight = 363.5,
        attribute = { 4, 7, 32, 6 },
        displayName = "B-8M1 - 20 S-8KOM",
        wsTypeOfWeapon = { 4, 7, 33, 32 }
      }, {
        CLSID = "{FC56DF80-9B09-44C5-8976-DCFAFF219062}",
        Count = 5,
        Cx_pil = 0.00159912109375,
        Elements = { {
            IsAdapter = true,
            ShapeName = "UB-13"
          } },
        Picture = "B13.png",
        Weight = 510,
        attribute = { 4, 7, 32, 1 },
        displayName = "B-13L - 5 S-13 OF",
        wsTypeOfWeapon = { 4, 7, 33, 33 }
      }, {
        CLSID = "{1FA14DEA-8CDB-45AD-88A8-EC068DF1E65A}",
        Elements = { {
            Position = { 0, -0.14, 0 },
            ShapeName = "C-24"
          } },
        Picture = "RBK250.png",
        Weight = 235,
        attribute = { 4, 7, 33, 34 },
        displayName = "S-24B"
      }, {
        CLSID = "{3858707D-F5D5-4bbb-BDD8-ABB0530EBC7C}",
        Count = 1,
        Cx_pil = 0.001,
        Elements = { {
            IsAdapter = true,
            ShapeName = "APU-7"
          }, {
            Position = { 0.37, -0.24, 0 },
            ShapeName = "C-24"
          } },
        Picture = "RBK250.png",
        Weight = 295,
        attribute = { 4, 4, 32, 101 },
        displayName = "S-24B",
        wsTypeOfWeapon = { 4, 7, 33, 34 }
      }, {
        CLSID = "{A0648264-4BC0-4EE8-A543-D119F6BA4257}",
        Count = 1,
        Cx_pil = 0.001708984375,
        Elements = { {
            DrawArgs = { { 3, 0.5 } },
            ShapeName = "C-25PU"
          }, {
            DrawArgs = { { 2, 1 } },
            Position = { -0.129395, -0.247116, 0 },
            ShapeName = "c-25",
            connector_name = "tube_1"
          } },
        Picture = "S25.png",
        Weight = 495,
        attribute = { 4, 7, 32, 7 },
        displayName = "S-25 OFM",
        wsTypeOfWeapon = { 4, 7, 33, 35 }
      }, {
        CLSID = "{F3EFE0AB-E91A-42D8-9CA2-B63C91ED570A}",
        Count = 4,
        Cx_pil = 0.001708984375,
        Elements = { {
            IsAdapter = true,
            ShapeName = "LAU-10"
          }, {
            ShapeName = "zuni",
            connector_name = "tube_01"
          }, {
            ShapeName = "zuni",
            connector_name = "tube_02"
          }, {
            ShapeName = "zuni",
            connector_name = "tube_03"
          }, {
            ShapeName = "zuni",
            connector_name = "tube_04"
          } },
        Picture = "LAU10.png",
        Weight = 440,
        attribute = { 4, 7, 32, 8 },
        displayName = "LAU-10 - 4 ZUNI MK 71",
        wsTypeOfWeapon = { 4, 7, 33, 37 }
      }, {
        CLSID = "{FD90A1DC-9147-49FA-BF56-CB83EF0BD32B}",
        Count = 19,
        Cx_pil = 0.001708984375,
        Elements = { {
            IsAdapter = true,
            ShapeName = "LAU-61"
          }, {
            ShapeName = "hydra_m151he",
            connector_name = "tube_01"
          }, {
            ShapeName = "hydra_m151he",
            connector_name = "tube_02"
          }, {
            ShapeName = "hydra_m151he",
            connector_name = "tube_03"
          }, {
            ShapeName = "hydra_m151he",
            connector_name = "tube_04"
          }, {
            ShapeName = "hydra_m151he",
            connector_name = "tube_05"
          }, {
            ShapeName = "hydra_m151he",
            connector_name = "tube_06"
          }, {
            ShapeName = "hydra_m151he",
            connector_name = "tube_07"
          }, {
            ShapeName = "hydra_m151he",
            connector_name = "tube_08"
          }, {
            ShapeName = "hydra_m151he",
            connector_name = "tube_09"
          }, {
            ShapeName = "hydra_m151he",
            connector_name = "tube_10"
          }, {
            ShapeName = "hydra_m151he",
            connector_name = "tube_11"
          }, {
            ShapeName = "hydra_m151he",
            connector_name = "tube_12"
          }, {
            ShapeName = "hydra_m151he",
            connector_name = "tube_13"
          }, {
            ShapeName = "hydra_m151he",
            connector_name = "tube_14"
          }, {
            ShapeName = "hydra_m151he",
            connector_name = "tube_15"
          }, {
            ShapeName = "hydra_m151he",
            connector_name = "tube_16"
          }, {
            ShapeName = "hydra_m151he",
            connector_name = "tube_17"
          }, {
            ShapeName = "hydra_m151he",
            connector_name = "tube_18"
          }, {
            ShapeName = "hydra_m151he",
            connector_name = "tube_19"
          } },
        Picture = "LAU61.png",
        Weight = 290.59,
        attribute = { 4, 7, 32, 9 },
        displayName = "LAU-61 - 19 2.75' rockets MK151 HE",
        wsTypeOfWeapon = { 4, 7, 33, 145 }
      }, {
        CLSID = "{A76344EB-32D2-4532-8FA2-0C1BDC00747E}",
        Count = 57,
        Cx_pil = 0.0029296875,
        Elements = { {
            IsAdapter = true,
            ShapeName = "MBD-3-LAU-61"
          } },
        Picture = "LAU61.png",
        Weight = 922.57,
        attribute = { 4, 7, 32, 33 },
        displayName = "LAU-61*3 - 57 2.75' rockets MK151 (HE)",
        wsTypeOfWeapon = { 4, 7, 33, 145 }
      }, {
        CLSID = "{FC85D2ED-501A-48ce-9863-49D468DDD5FC}",
        Count = 7,
        Cx_pil = 0.00146484375,
        Elements = { {
            IsAdapter = true,
            ShapeName = "LAU-68"
          }, {
            ShapeName = "hydra_m151he",
            connector_name = "tube_01"
          }, {
            ShapeName = "hydra_m151he",
            connector_name = "tube_02"
          }, {
            ShapeName = "hydra_m151he",
            connector_name = "tube_03"
          }, {
            ShapeName = "hydra_m151he",
            connector_name = "tube_04"
          }, {
            ShapeName = "hydra_m151he",
            connector_name = "tube_05"
          }, {
            ShapeName = "hydra_m151he",
            connector_name = "tube_06"
          }, {
            ShapeName = "hydra_m151he",
            connector_name = "tube_07"
          } },
        Picture = "LAU68.png",
        Weight = 105.5,
        attribute = { 4, 7, 32, 105 },
        displayName = "LAU-68 - 7 2.75' rockets MK1 (Practice)",
        wsTypeOfWeapon = { 4, 7, 33, 144 }
      }, {
        CLSID = "{174C6E6D-0C3D-42ff-BCB3-0853CB371F5C}",
        Count = 7,
        Cx_pil = 0.00146484375,
        Elements = { {
            IsAdapter = true,
            ShapeName = "LAU-68"
          }, {
            ShapeName = "hydra_m151he",
            connector_name = "tube_01"
          }, {
            ShapeName = "hydra_m151he",
            connector_name = "tube_02"
          }, {
            ShapeName = "hydra_m151he",
            connector_name = "tube_03"
          }, {
            ShapeName = "hydra_m151he",
            connector_name = "tube_04"
          }, {
            ShapeName = "hydra_m151he",
            connector_name = "tube_05"
          }, {
            ShapeName = "hydra_m151he",
            connector_name = "tube_06"
          }, {
            ShapeName = "hydra_m151he",
            connector_name = "tube_07"
          } },
        Picture = "LAU68.png",
        Weight = 103.4,
        attribute = { 4, 7, 32, 106 },
        displayName = "LAU-68 - 7 2.75' rockets MK5 (HE)",
        wsTypeOfWeapon = { 4, 7, 33, 145 }
      }, {
        CLSID = "{65396399-9F5C-4ec3-A7D2-5A8F4C1D90C4}",
        Count = 7,
        Cx_pil = 0.00146484375,
        Elements = { {
            IsAdapter = true,
            ShapeName = "LAU-68"
          }, {
            ShapeName = "hydra_m156",
            connector_name = "tube_01"
          }, {
            ShapeName = "hydra_m156",
            connector_name = "tube_02"
          }, {
            ShapeName = "hydra_m156",
            connector_name = "tube_03"
          }, {
            ShapeName = "hydra_m156",
            connector_name = "tube_04"
          }, {
            ShapeName = "hydra_m156",
            connector_name = "tube_05"
          }, {
            ShapeName = "hydra_m156",
            connector_name = "tube_06"
          }, {
            ShapeName = "hydra_m156",
            connector_name = "tube_07"
          } },
        Picture = "LAU68.png",
        Weight = 105.5,
        attribute = { 4, 7, 32, 107 },
        displayName = "LAU-68 - 7 2.75' rockets MK61 (Practice)",
        wsTypeOfWeapon = { 4, 7, 33, 146 }
      }, {
        CLSID = "{A021F29D-18AB-4d3e-985C-FC9C60E35E9E}",
        Count = 7,
        Cx_pil = 0.00146484375,
        Elements = { {
            IsAdapter = true,
            ShapeName = "LAU-68"
          }, {
            ShapeName = "hydra_m151he",
            connector_name = "tube_01"
          }, {
            ShapeName = "hydra_m151he",
            connector_name = "tube_02"
          }, {
            ShapeName = "hydra_m151he",
            connector_name = "tube_03"
          }, {
            ShapeName = "hydra_m151he",
            connector_name = "tube_04"
          }, {
            ShapeName = "hydra_m151he",
            connector_name = "tube_05"
          }, {
            ShapeName = "hydra_m151he",
            connector_name = "tube_06"
          }, {
            ShapeName = "hydra_m151he",
            connector_name = "tube_07"
          } },
        Picture = "LAU68.png",
        Weight = 114.53,
        attribute = { 4, 7, 32, 108 },
        displayName = "LAU-68 - 7 2.75' rockets M151 (HE)",
        wsTypeOfWeapon = { 4, 7, 33, 147 }
      }, {
        CLSID = "{4F977A2A-CD25-44df-90EF-164BFA2AE72F}",
        Count = 7,
        Cx_pil = 0.00146484375,
        Elements = { {
            IsAdapter = true,
            ShapeName = "LAU-68"
          }, {
            ShapeName = "hydra_m156",
            connector_name = "tube_01"
          }, {
            ShapeName = "hydra_m156",
            connector_name = "tube_02"
          }, {
            ShapeName = "hydra_m156",
            connector_name = "tube_03"
          }, {
            ShapeName = "hydra_m156",
            connector_name = "tube_04"
          }, {
            ShapeName = "hydra_m156",
            connector_name = "tube_05"
          }, {
            ShapeName = "hydra_m156",
            connector_name = "tube_06"
          }, {
            ShapeName = "hydra_m156",
            connector_name = "tube_07"
          } },
        Picture = "LAU68.png",
        Weight = 115.79,
        attribute = { 4, 7, 32, 109 },
        displayName = "LAU-68 - 7 2.75' rockets M156(WP)",
        wsTypeOfWeapon = { 4, 7, 33, 148 }
      }, {
        CLSID = "{1F7136CB-8120-4e77-B97B-945FF01FB67C}",
        Count = 7,
        Cx_pil = 0.00146484375,
        Elements = { {
            IsAdapter = true,
            ShapeName = "LAU-68"
          }, {
            ShapeName = "hydra_m156",
            connector_name = "tube_01"
          }, {
            ShapeName = "hydra_m156",
            connector_name = "tube_02"
          }, {
            ShapeName = "hydra_m156",
            connector_name = "tube_03"
          }, {
            ShapeName = "hydra_m156",
            connector_name = "tube_04"
          }, {
            ShapeName = "hydra_m156",
            connector_name = "tube_05"
          }, {
            ShapeName = "hydra_m156",
            connector_name = "tube_06"
          }, {
            ShapeName = "hydra_m156",
            connector_name = "tube_07"
          } },
        Picture = "LAU68.png",
        Weight = 114.53,
        attribute = { 4, 7, 32, 110 },
        displayName = "LAU-68 - 7 2.75' rockets WTU1B (Practice)",
        wsTypeOfWeapon = { 4, 7, 33, 149 }
      }, {
        CLSID = "{0877B74B-5A00-4e61-BA8A-A56450BA9E27}",
        Count = 7,
        Cx_pil = 0.00146484375,
        Elements = { {
            IsAdapter = true,
            ShapeName = "LAU-68"
          }, {
            ShapeName = "hydra_m156",
            connector_name = "tube_01"
          }, {
            ShapeName = "hydra_m156",
            connector_name = "tube_02"
          }, {
            ShapeName = "hydra_m156",
            connector_name = "tube_03"
          }, {
            ShapeName = "hydra_m156",
            connector_name = "tube_04"
          }, {
            ShapeName = "hydra_m156",
            connector_name = "tube_05"
          }, {
            ShapeName = "hydra_m156",
            connector_name = "tube_06"
          }, {
            ShapeName = "hydra_m156",
            connector_name = "tube_07"
          } },
        Picture = "LAU68.png",
        Weight = 114.53,
        attribute = { 4, 7, 32, 111 },
        displayName = "LAU-68 - 7 2.75' rockets M274 (Practice smoke)",
        wsTypeOfWeapon = { 4, 7, 33, 150 }
      }, {
        CLSID = "{647C5F26-BDD1-41e6-A371-8DE1E4CC0E94}",
        Count = 7,
        Cx_pil = 0.00146484375,
        Elements = { {
            IsAdapter = true,
            ShapeName = "LAU-68"
          }, {
            ShapeName = "Hydra_M278",
            connector_name = "tube_01"
          }, {
            ShapeName = "Hydra_M278",
            connector_name = "tube_02"
          }, {
            ShapeName = "Hydra_M278",
            connector_name = "tube_03"
          }, {
            ShapeName = "Hydra_M278",
            connector_name = "tube_04"
          }, {
            ShapeName = "Hydra_M278",
            connector_name = "tube_05"
          }, {
            ShapeName = "Hydra_M278",
            connector_name = "tube_06"
          }, {
            ShapeName = "Hydra_M278",
            connector_name = "tube_07"
          } },
        Picture = "LAU68.png",
        Weight = 120.13,
        attribute = { 4, 7, 32, 112 },
        displayName = "LAU-68 - 7 2.75' rockets M257 (Parachute illumination)",
        wsTypeOfWeapon = { 4, 7, 33, 151 }
      }, {
        CLSID = "{D22C2D63-E5C9-4247-94FB-5E8F3DE22B71}",
        Count = 7,
        Cx_pil = 0.00146484375,
        Elements = { {
            IsAdapter = true,
            ShapeName = "LAU-131"
          } },
        Picture = "LAU131.png",
        Weight = 93.27,
        attribute = { 4, 7, 32, 114 },
        displayName = "LAU-131 - 7 2.75' rockets Mk1 (Practice)",
        wsTypeOfWeapon = { 4, 7, 33, 144 }
      }, {
        CLSID = "{319293F2-392C-4617-8315-7C88C22AF7C4}",
        Count = 7,
        Cx_pil = 0.00146484375,
        Elements = { {
            IsAdapter = true,
            ShapeName = "LAU-131"
          } },
        Picture = "LAU131.png",
        Weight = 91.17,
        attribute = { 4, 7, 32, 115 },
        displayName = "LAU-131 - 7 2.75' rockets MK5 (HE)",
        wsTypeOfWeapon = { 4, 7, 33, 145 }
      }, {
        CLSID = "{1CA5E00B-D545-4ff9-9B53-5970E292F14D}",
        Count = 7,
        Cx_pil = 0.00146484375,
        Elements = { {
            IsAdapter = true,
            ShapeName = "LAU-131"
          } },
        Picture = "LAU131.png",
        Weight = 93.27,
        attribute = { 4, 7, 32, 116 },
        displayName = "LAU-131 - 7 2.75' rockets MK61 (Practice)",
        wsTypeOfWeapon = { 4, 7, 33, 146 }
      }, {
        CLSID = "{69926055-0DA8-4530-9F2F-C86B157EA9F6}",
        Count = 7,
        Cx_pil = 0.00146484375,
        Elements = { {
            IsAdapter = true,
            ShapeName = "LAU-131"
          } },
        Picture = "LAU131.png",
        Weight = 102.3,
        attribute = { 4, 7, 32, 117 },
        displayName = "LAU-131 - 7 2.75' rockets M151 (HE)",
        wsTypeOfWeapon = { 4, 7, 33, 147 }
      }, {
        CLSID = "{2AF2EC3F-9065-4de5-93E1-1739C9A71EF7}",
        Count = 7,
        Cx_pil = 0.00146484375,
        Elements = { {
            IsAdapter = true,
            ShapeName = "LAU-131"
          } },
        Picture = "LAU131.png",
        Weight = 103.56,
        attribute = { 4, 7, 32, 118 },
        displayName = "LAU-131 - 7 2.75' rockets M156 (WP)",
        wsTypeOfWeapon = { 4, 7, 33, 148 }
      }, {
        CLSID = "{DDCE7D70-5313-4181-8977-F11018681662}",
        Count = 7,
        Cx_pil = 0.00146484375,
        Elements = { {
            IsAdapter = true,
            ShapeName = "LAU-131"
          } },
        Picture = "LAU131.png",
        Weight = 102.3,
        attribute = { 4, 7, 32, 119 },
        displayName = "LAU-131 - 7 2.75' rockets WTU1B (Practice)",
        wsTypeOfWeapon = { 4, 7, 33, 149 }
      }, {
        CLSID = "{DAD45FE5-CFF0-4a2b-99D4-5D044D3BC22F}",
        Count = 7,
        Cx_pil = 0.00146484375,
        Elements = { {
            IsAdapter = true,
            ShapeName = "LAU-131"
          } },
        Picture = "LAU131.png",
        Weight = 107.9,
        attribute = { 4, 7, 32, 121 },
        displayName = "LAU-131 - 7 2.75' rockets M257 (Parachute illumination)",
        wsTypeOfWeapon = { 4, 7, 33, 151 }
      }, {
        CLSID = "{6D6D5C07-2A90-4a68-9A74-C5D0CFFB05D9}",
        Count = 7,
        Cx_pil = 0.00146484375,
        Elements = { {
            IsAdapter = true,
            ShapeName = "LAU-131"
          } },
        Picture = "LAU131.png",
        Weight = 102.3,
        attribute = { 4, 7, 32, 120 },
        displayName = "LAU-131 - 7 2.75' rockets M274 (Practice smoke)",
        wsTypeOfWeapon = { 4, 7, 33, 150 }
      }, {
        CLSID = "{443364AE-D557-488e-9499-45EDB3BA6730}",
        Count = 21,
        Cx_pil = 0.00244140625,
        Elements = { {
            IsAdapter = true,
            ShapeName = "BRU-42_LS_(LAU-68)"
          } },
        Picture = "LAU68.png",
        Weight = 367.3,
        attribute = { 4, 7, 32, 124 },
        displayName = "LAU-68*3 - 7 2.75' rockets MK1 (Practice)",
        wsTypeOfWeapon = { 4, 7, 33, 144 }
      }, {
        CLSID = "{9BC82B3D-FE70-4910-B2B7-3E54EFE73262}",
        Count = 21,
        Cx_pil = 0.00244140625,
        Elements = { {
            IsAdapter = true,
            ShapeName = "BRU-42_LS_(LAU-68)"
          } },
        Picture = "LAU68.png",
        Weight = 361,
        attribute = { 4, 7, 32, 125 },
        displayName = "LAU-68*3 - 7 2.75' rockets MK5 (HE)",
        wsTypeOfWeapon = { 4, 7, 33, 145 }
      }, {
        CLSID = "{C0FA251E-B645-4ce5-926B-F4BC20822F8B}",
        Count = 21,
        Cx_pil = 0.00244140625,
        Elements = { {
            IsAdapter = true,
            ShapeName = "BRU-42_LS_(LAU-68)"
          } },
        Picture = "LAU68.png",
        Weight = 367.3,
        attribute = { 4, 7, 32, 126 },
        displayName = "LAU-68*3 - 7 2.75' rockets MK61 (Practice)",
        wsTypeOfWeapon = { 4, 7, 33, 146 }
      }, {
        CLSID = "{64329ED9-B14C-4c0b-A923-A3C911DA1527}",
        Count = 21,
        Cx_pil = 0.00244140625,
        Elements = { {
            IsAdapter = true,
            ShapeName = "BRU-42_LS_(LAU-68)"
          } },
        Picture = "LAU68.png",
        Weight = 394.39,
        attribute = { 4, 7, 32, 127 },
        displayName = "LAU-68*3 - 7 2.75' rockets M151 (HE)",
        wsTypeOfWeapon = { 4, 7, 33, 147 }
      }, {
        CLSID = "{C2593383-3CA8-4b18-B73D-0E750BCA1C85}",
        Count = 21,
        Cx_pil = 0.00244140625,
        Elements = { {
            IsAdapter = true,
            ShapeName = "BRU-42_LS_(LAU-68)"
          } },
        Picture = "LAU68.png",
        Weight = 398.17,
        attribute = { 4, 7, 32, 128 },
        displayName = "LAU-68*3 - 7 2.75' rockets M156 (WP)",
        wsTypeOfWeapon = { 4, 7, 33, 148 }
      }, {
        CLSID = "{A1853B38-2160-4ffe-B7E9-9BF81E6C3D77}",
        Count = 21,
        Cx_pil = 0.00244140625,
        Elements = { {
            IsAdapter = true,
            ShapeName = "BRU-42_LS_(LAU-68)"
          } },
        Picture = "LAU68.png",
        Weight = 394.39,
        attribute = { 4, 7, 32, 129 },
        displayName = "LAU-68*3 - 7 2.75' rockets WTU1B (Practice)",
        wsTypeOfWeapon = { 4, 7, 33, 149 }
      }, {
        CLSID = "{4C044B08-886B-46c8-9B1F-AB05B3ED9C1D}",
        Count = 21,
        Cx_pil = 0.00244140625,
        Elements = { {
            IsAdapter = true,
            ShapeName = "BRU-42_LS_(LAU-68)"
          } },
        Picture = "LAU68.png",
        Weight = 394.39,
        attribute = { 4, 7, 32, 130 },
        displayName = "LAU-68*3 - 7 2.75' rockets M274 (Practice smoke)",
        wsTypeOfWeapon = { 4, 7, 33, 150 }
      }, {
        CLSID = "{E6966004-A525-4f47-AF94-BCFEDF8FDBDA}",
        Count = 21,
        Cx_pil = 0.00244140625,
        Elements = { {
            IsAdapter = true,
            ShapeName = "BRU-42_LS_(LAU-68)"
          } },
        Picture = "LAU68.png",
        Weight = 411.19,
        attribute = { 4, 7, 32, 131 },
        displayName = "LAU-68*3 - 7 2.75' rockets M257 (Parachute illumination)",
        wsTypeOfWeapon = { 4, 7, 33, 151 }
      }, {
        CLSID = "LAU_131x3_HYDRA_70_MK1",
        Count = 21,
        Cx_pil = 0.00244140625,
        Elements = { {
            IsAdapter = true,
            ShapeName = "BRU-42_LS_(LAU-131)"
          } },
        Picture = "LAU131.png",
        Weight = 330.61,
        attribute = { 4, 7, 32, 133 },
        displayName = "LAU-131*3 - 7 2.75' rockets MK1 (Practice)",
        wsTypeOfWeapon = { 4, 7, 33, 144 }
      }, {
        CLSID = "LAU_131x3_HYDRA_70_MK5",
        Count = 21,
        Cx_pil = 0.00244140625,
        Elements = { {
            IsAdapter = true,
            ShapeName = "BRU-42_LS_(LAU-131)"
          } },
        Picture = "LAU131.png",
        Weight = 324.31,
        attribute = { 4, 7, 32, 134 },
        displayName = "LAU-131*3 - 7 2.75' rockets MK5 (HE)",
        wsTypeOfWeapon = { 4, 7, 33, 145 }
      }, {
        CLSID = "LAU_131x3_HYDRA_70_MK61",
        Count = 21,
        Cx_pil = 0.00244140625,
        Elements = { {
            IsAdapter = true,
            ShapeName = "BRU-42_LS_(LAU-131)"
          } },
        Picture = "LAU131.png",
        Weight = 330.61,
        attribute = { 4, 7, 32, 135 },
        displayName = "LAU-131*3 - 7 2.75' rockets MK61 (Practice)",
        wsTypeOfWeapon = { 4, 7, 33, 146 }
      }, {
        CLSID = "LAU_131x3_HYDRA_70_M151",
        Count = 21,
        Cx_pil = 0.00244140625,
        Elements = { {
            IsAdapter = true,
            ShapeName = "BRU-42_LS_(LAU-131)"
          } },
        Picture = "LAU131.png",
        Weight = 357.7,
        attribute = { 4, 7, 32, 136 },
        displayName = "LAU-131*3 - 7 2.75' rockets M151 (HE)",
        wsTypeOfWeapon = { 4, 7, 33, 147 }
      }, {
        CLSID = "LAU_131x3_HYDRA_70_M156",
        Count = 21,
        Cx_pil = 0.00244140625,
        Elements = { {
            IsAdapter = true,
            ShapeName = "BRU-42_LS_(LAU-131)"
          } },
        Picture = "LAU131.png",
        Weight = 361.48,
        attribute = { 4, 7, 32, 137 },
        displayName = "LAU-131*3 - 7 2.75' rockets M156 (WP)",
        wsTypeOfWeapon = { 4, 7, 33, 148 }
      }, {
        CLSID = "LAU_131x3_HYDRA_70_WTU1B",
        Count = 21,
        Cx_pil = 0.00244140625,
        Elements = { {
            IsAdapter = true,
            ShapeName = "BRU-42_LS_(LAU-131)"
          } },
        Picture = "LAU131.png",
        Weight = 357.7,
        attribute = { 4, 7, 32, 138 },
        displayName = "LAU-131*3 - 7 2.75' rockets WTU1B (Practice)",
        wsTypeOfWeapon = { 4, 7, 33, 149 }
      }, {
        CLSID = "LAU_131x3_HYDRA_70_M274",
        Count = 21,
        Cx_pil = 0.00244140625,
        Elements = { {
            IsAdapter = true,
            ShapeName = "BRU-42_LS_(LAU-131)"
          } },
        Picture = "LAU131.png",
        Weight = 357.7,
        attribute = { 4, 7, 32, 139 },
        displayName = "LAU-131*3 - 7 2.75' rockets M274 (Practice Smoke)",
        wsTypeOfWeapon = { 4, 7, 33, 150 }
      }, {
        CLSID = "LAU_131x3_HYDRA_70_M257",
        Count = 21,
        Cx_pil = 0.00244140625,
        Elements = { {
            IsAdapter = true,
            ShapeName = "BRU-42_LS_(LAU-131)"
          } },
        Picture = "LAU131.png",
        Weight = 374.5,
        attribute = { 4, 7, 32, 140 },
        displayName = "LAU-131*3 - 7 2.75' rockets M257 (Parachute illumination)",
        wsTypeOfWeapon = { 4, 7, 33, 151 }
      }, {
        CLSID = "{3DFB7320-AB0E-11d7-9897-000476191836}",
        Count = 20,
        Cx_pil = 0.00146484375,
        Elements = { {
            IsAdapter = true,
            ShapeName = "B-20"
          } },
        Picture = "B8V20A.png",
        Weight = 359.5,
        attribute = { 4, 7, 32, 68 },
        displayName = "B-8M1 - 20 S-8TsM",
        wsTypeOfWeapon = { 4, 7, 33, 30 }
      }, {
        CLSID = "B-8M1 - 20 S-8OFP2",
        Count = 20,
        Cx_pil = 0.00213134765625,
        Elements = { {
            IsAdapter = true,
            ShapeName = "B-20"
          } },
        Picture = "B8V20A.png",
        Weight = 471.5,
        attribute = { 4, 7, 32, 151 },
        displayName = "B-8M1 - 20 S-8OFP2",
        wsTypeOfWeapon = { 4, 7, 33, 155 }
      }, {
        CLSID = "{3DFB7321-AB0E-11d7-9897-000476191836}",
        Count = 19,
        Cx_pil = 0.00146484375,
        Elements = { {
            IsAdapter = true,
            ShapeName = "LAU-61"
          }, {
            ShapeName = "hydra_m156",
            connector_name = "tube_01"
          }, {
            ShapeName = "hydra_m156",
            connector_name = "tube_02"
          }, {
            ShapeName = "hydra_m156",
            connector_name = "tube_03"
          }, {
            ShapeName = "hydra_m156",
            connector_name = "tube_04"
          }, {
            ShapeName = "hydra_m156",
            connector_name = "tube_05"
          }, {
            ShapeName = "hydra_m156",
            connector_name = "tube_06"
          }, {
            ShapeName = "hydra_m156",
            connector_name = "tube_07"
          }, {
            ShapeName = "hydra_m156",
            connector_name = "tube_08"
          }, {
            ShapeName = "hydra_m156",
            connector_name = "tube_09"
          }, {
            ShapeName = "hydra_m156",
            connector_name = "tube_10"
          }, {
            ShapeName = "hydra_m156",
            connector_name = "tube_11"
          }, {
            ShapeName = "hydra_m156",
            connector_name = "tube_12"
          }, {
            ShapeName = "hydra_m156",
            connector_name = "tube_13"
          }, {
            ShapeName = "hydra_m156",
            connector_name = "tube_14"
          }, {
            ShapeName = "hydra_m156",
            connector_name = "tube_15"
          }, {
            ShapeName = "hydra_m156",
            connector_name = "tube_16"
          }, {
            ShapeName = "hydra_m156",
            connector_name = "tube_17"
          }, {
            ShapeName = "hydra_m156",
            connector_name = "tube_18"
          }, {
            ShapeName = "hydra_m156",
            connector_name = "tube_19"
          } },
        Picture = "LAU61.png",
        Weight = 294.01,
        attribute = { 4, 7, 32, 69 },
        displayName = "LAU-61 - 19 2.75' rockets MK156 WP",
        wsTypeOfWeapon = { 4, 7, 33, 148 }
      }, {
        CLSID = "M260_HYDRA",
        Count = 7,
        Cx_pil = 0.00146484375,
        Elements = { {
            IsAdapter = true,
            ShapeName = "OH-58D_Gorgona"
          } },
        Picture = "lau68.png",
        Weight = 112,
        attribute = { 4, 7, 32, 144 },
        displayName = "M260 - 7 2.75' rockets MK156",
        wsTypeOfWeapon = { 4, 7, 33, 145 }
      }, {
        CLSID = "M260_HYDRA_WP",
        Count = 7,
        Cx_pil = 0.00146484375,
        Elements = { {
            IsAdapter = true,
            ShapeName = "OH-58D_Gorgona"
          } },
        Picture = "lau68.png",
        Weight = 112,
        attribute = { 4, 7, 32, 146 },
        displayName = "M260 - 7 2.75' rockets MK156 WP",
        wsTypeOfWeapon = { 4, 7, 33, 148 }
      }, {
        CLSID = "{6A4B9E69-64FE-439a-9163-3A87FB6A4D81}",
        Count = 20,
        Cx_pil = 0.00146484375,
        Elements = { {
            IsAdapter = true,
            ShapeName = "B-8V20A"
          } },
        Picture = "B8V20A.png",
        Weight = 349,
        attribute = { 4, 7, 32, 98 },
        displayName = "B-8V20A - 20 S-8KOM",
        wsTypeOfWeapon = { 4, 7, 33, 32 }
      }, {
        CLSID = "B_8V20A_CM",
        Count = 20,
        Cx_pil = 0.00213134765625,
        Elements = { {
            IsAdapter = true,
            ShapeName = "B-8V20A"
          } },
        Picture = "B8V20A.png",
        Weight = 345,
        attribute = { 4, 7, 32, 148 },
        displayName = "B-8V20A - 20 S-8TsM",
        wsTypeOfWeapon = { 4, 7, 33, 30 }
      }, {
        CLSID = "B_8V20A_OFP2",
        Count = 20,
        Cx_pil = 0.00213134765625,
        Elements = { {
            IsAdapter = true,
            ShapeName = "B-8V20A"
          } },
        Picture = "B8V20A.png",
        Weight = 457,
        attribute = { 4, 7, 32, 149 },
        displayName = "B-8V20A - 20 S-8OFP2",
        wsTypeOfWeapon = { 4, 7, 33, 155 }
      }, {
        CLSID = "B_8V20A_OM",
        Count = 20,
        Cx_pil = 0.00213134765625,
        Elements = { {
            IsAdapter = true,
            ShapeName = "B-8V20A"
          } },
        Picture = "B8V20A.png",
        Weight = 365,
        attribute = { 4, 7, 32, 150 },
        displayName = "B-8V20A - 20 S-8OM",
        wsTypeOfWeapon = { 4, 7, 33, 158 }
      }, {
        CLSID = "XM158_MK1",
        Count = 7,
        Cx_pil = 0.00146484375,
        Elements = { {
            IsAdapter = true,
            ShapeName = "XM158"
          } },
        Picture = "lau68.png",
        Weight = 112,
        attribute = { 4, 7, 32, 162 },
        displayName = "XM158 - 7 2.75' rockets MK1 Practice",
        wsTypeOfWeapon = { 4, 7, 33, 144 }
      }, {
        CLSID = "XM158_MK5",
        Count = 7,
        Cx_pil = 0.00146484375,
        Elements = { {
            IsAdapter = true,
            ShapeName = "XM158"
          } },
        Picture = "lau68.png",
        Weight = 112,
        attribute = { 4, 7, 32, 163 },
        displayName = "XM158 - 7 2.75' rockets MK5 HE",
        wsTypeOfWeapon = { 4, 7, 33, 145 }
      }, {
        CLSID = "XM158_M151",
        Count = 7,
        Cx_pil = 0.00146484375,
        Elements = { {
            IsAdapter = true,
            ShapeName = "XM158"
          } },
        Picture = "lau68.png",
        Weight = 112,
        attribute = { 4, 7, 32, 164 },
        displayName = "XM158 - 7 2.75' rockets M151 HE",
        wsTypeOfWeapon = { 4, 7, 33, 147 }
      }, {
        CLSID = "XM158_M156",
        Count = 7,
        Cx_pil = 0.00146484375,
        Elements = { {
            IsAdapter = true,
            ShapeName = "XM158"
          } },
        Picture = "lau68.png",
        Weight = 112,
        attribute = { 4, 7, 32, 165 },
        displayName = "XM158 - 7 2.75' rockets M156 WP",
        wsTypeOfWeapon = { 4, 7, 33, 148 }
      }, {
        CLSID = "XM158_M274",
        Count = 7,
        Cx_pil = 0.00146484375,
        Elements = { {
            IsAdapter = true,
            ShapeName = "XM158"
          } },
        Picture = "lau68.png",
        Weight = 112,
        attribute = { 4, 7, 32, 166 },
        displayName = "XM158 - 7 2.75' rockets M274 Practice smoke",
        wsTypeOfWeapon = { 4, 7, 33, 150 }
      }, {
        CLSID = "XM158_M257",
        Count = 7,
        Cx_pil = 0.00146484375,
        Elements = { {
            IsAdapter = true,
            ShapeName = "XM158"
          } },
        Picture = "lau68.png",
        Weight = 112,
        attribute = { 4, 7, 32, 167 },
        displayName = "XM158 - 7 2.75' rockets M257 Parachute illumination",
        wsTypeOfWeapon = { 4, 7, 33, 151 }
      }, {
        CLSID = "M261_MK151",
        Count = 19,
        Cx_pil = 0.001708984375,
        Elements = { {
            IsAdapter = true,
            ShapeName = "M261"
          } },
        Picture = "LAU61.png",
        Weight = 234,
        attribute = { 4, 7, 32, 168 },
        displayName = "M261 - 19 2.75' rockets MK151 HE",
        wsTypeOfWeapon = { 4, 7, 33, 147 }
      }, {
        CLSID = "M261_MK156",
        Count = 19,
        Cx_pil = 0.001708984375,
        Elements = { {
            IsAdapter = true,
            ShapeName = "M261"
          } },
        Picture = "LAU61.png",
        Weight = 234,
        attribute = { 4, 7, 32, 169 },
        displayName = "M261 - 19 2.75' rockets MK156 WP",
        wsTypeOfWeapon = { 4, 7, 33, 148 }
      }, {
        CLSID = "LAU3_WP156",
        Count = 19,
        Cx_pil = 0.00146484375,
        Elements = { {
            IsAdapter = true,
            ShapeName = "LAU-3"
          }, {
            ShapeName = "Hydra_M156",
            connector_name = "tube_1"
          }, {
            ShapeName = "Hydra_M156",
            connector_name = "tube_2"
          }, {
            ShapeName = "Hydra_M156",
            connector_name = "tube_3"
          }, {
            ShapeName = "Hydra_M156",
            connector_name = "tube_4"
          }, {
            ShapeName = "Hydra_M156",
            connector_name = "tube_5"
          }, {
            ShapeName = "Hydra_M156",
            connector_name = "tube_6"
          }, {
            ShapeName = "Hydra_M156",
            connector_name = "tube_7"
          }, {
            ShapeName = "Hydra_M156",
            connector_name = "tube_8"
          }, {
            ShapeName = "Hydra_M156",
            connector_name = "tube_9"
          }, {
            ShapeName = "Hydra_M156",
            connector_name = "tube_10"
          }, {
            ShapeName = "Hydra_M156",
            connector_name = "tube_11"
          }, {
            ShapeName = "Hydra_M156",
            connector_name = "tube_12"
          }, {
            ShapeName = "Hydra_M156",
            connector_name = "tube_13"
          }, {
            ShapeName = "Hydra_M156",
            connector_name = "tube_14"
          }, {
            ShapeName = "Hydra_M156",
            connector_name = "tube_15"
          }, {
            ShapeName = "Hydra_M156",
            connector_name = "tube_16"
          }, {
            ShapeName = "Hydra_M156",
            connector_name = "tube_17"
          }, {
            ShapeName = "Hydra_M156",
            connector_name = "tube_18"
          }, {
            ShapeName = "Hydra_M156",
            connector_name = "tube_19"
          } },
        Picture = "LAU61.png",
        Weight = 234,
        attribute = { 4, 7, 32, 182 },
        displayName = "LAU-3 - 19 2.75' rockets MK156 WP",
        wsTypeOfWeapon = { 4, 7, 33, 148 }
      }, {
        CLSID = "LAU3_WP1B",
        Count = 19,
        Cx_pil = 0.00146484375,
        Elements = { {
            IsAdapter = true,
            ShapeName = "LAU-3"
          }, {
            ShapeName = "Hydra_TWU1B",
            connector_name = "tube_1"
          }, {
            ShapeName = "Hydra_TWU1B",
            connector_name = "tube_2"
          }, {
            ShapeName = "Hydra_TWU1B",
            connector_name = "tube_3"
          }, {
            ShapeName = "Hydra_TWU1B",
            connector_name = "tube_4"
          }, {
            ShapeName = "Hydra_TWU1B",
            connector_name = "tube_5"
          }, {
            ShapeName = "Hydra_TWU1B",
            connector_name = "tube_6"
          }, {
            ShapeName = "Hydra_TWU1B",
            connector_name = "tube_7"
          }, {
            ShapeName = "Hydra_TWU1B",
            connector_name = "tube_8"
          }, {
            ShapeName = "Hydra_TWU1B",
            connector_name = "tube_9"
          }, {
            ShapeName = "Hydra_TWU1B",
            connector_name = "tube_10"
          }, {
            ShapeName = "Hydra_TWU1B",
            connector_name = "tube_11"
          }, {
            ShapeName = "Hydra_TWU1B",
            connector_name = "tube_12"
          }, {
            ShapeName = "Hydra_TWU1B",
            connector_name = "tube_13"
          }, {
            ShapeName = "Hydra_TWU1B",
            connector_name = "tube_14"
          }, {
            ShapeName = "Hydra_TWU1B",
            connector_name = "tube_15"
          }, {
            ShapeName = "Hydra_TWU1B",
            connector_name = "tube_16"
          }, {
            ShapeName = "Hydra_TWU1B",
            connector_name = "tube_17"
          }, {
            ShapeName = "Hydra_TWU1B",
            connector_name = "tube_18"
          }, {
            ShapeName = "Hydra_TWU1B",
            connector_name = "tube_19"
          } },
        Picture = "LAU61.png",
        Weight = 234,
        attribute = { 4, 7, 32, 183 },
        displayName = "LAU-3 - 19 2.75' rockets WTU-1/B WP",
        wsTypeOfWeapon = { 4, 7, 33, 149 }
      }, {
        CLSID = "LAU3_WP61",
        Count = 19,
        Cx_pil = 0.00146484375,
        Elements = { {
            IsAdapter = true,
            ShapeName = "LAU-3"
          }, {
            ShapeName = "Hydra_Mk61",
            connector_name = "tube_1"
          }, {
            ShapeName = "Hydra_Mk61",
            connector_name = "tube_2"
          }, {
            ShapeName = "Hydra_Mk61",
            connector_name = "tube_3"
          }, {
            ShapeName = "Hydra_Mk61",
            connector_name = "tube_4"
          }, {
            ShapeName = "Hydra_Mk61",
            connector_name = "tube_5"
          }, {
            ShapeName = "Hydra_Mk61",
            connector_name = "tube_6"
          }, {
            ShapeName = "Hydra_Mk61",
            connector_name = "tube_7"
          }, {
            ShapeName = "Hydra_Mk61",
            connector_name = "tube_8"
          }, {
            ShapeName = "Hydra_Mk61",
            connector_name = "tube_9"
          }, {
            ShapeName = "Hydra_Mk61",
            connector_name = "tube_10"
          }, {
            ShapeName = "Hydra_Mk61",
            connector_name = "tube_11"
          }, {
            ShapeName = "Hydra_Mk61",
            connector_name = "tube_12"
          }, {
            ShapeName = "Hydra_Mk61",
            connector_name = "tube_13"
          }, {
            ShapeName = "Hydra_Mk61",
            connector_name = "tube_14"
          }, {
            ShapeName = "Hydra_Mk61",
            connector_name = "tube_15"
          }, {
            ShapeName = "Hydra_Mk61",
            connector_name = "tube_16"
          }, {
            ShapeName = "Hydra_Mk61",
            connector_name = "tube_17"
          }, {
            ShapeName = "Hydra_Mk61",
            connector_name = "tube_18"
          }, {
            ShapeName = "Hydra_Mk61",
            connector_name = "tube_19"
          } },
        Picture = "LAU61.png",
        Weight = 234,
        attribute = { 4, 7, 32, 184 },
        displayName = "LAU-3 - 19 2.75' rockets MK61 WP",
        wsTypeOfWeapon = { 4, 7, 33, 146 }
      }, {
        CLSID = "LAU3_HE5",
        Count = 19,
        Cx_pil = 0.00146484375,
        Elements = { {
            IsAdapter = true,
            ShapeName = "LAU-3"
          }, {
            ShapeName = "Hydra_Mk5",
            connector_name = "tube_1"
          }, {
            ShapeName = "Hydra_Mk5",
            connector_name = "tube_2"
          }, {
            ShapeName = "Hydra_Mk5",
            connector_name = "tube_3"
          }, {
            ShapeName = "Hydra_Mk5",
            connector_name = "tube_4"
          }, {
            ShapeName = "Hydra_Mk5",
            connector_name = "tube_5"
          }, {
            ShapeName = "Hydra_Mk5",
            connector_name = "tube_6"
          }, {
            ShapeName = "Hydra_Mk5",
            connector_name = "tube_7"
          }, {
            ShapeName = "Hydra_Mk5",
            connector_name = "tube_8"
          }, {
            ShapeName = "Hydra_Mk5",
            connector_name = "tube_9"
          }, {
            ShapeName = "Hydra_Mk5",
            connector_name = "tube_10"
          }, {
            ShapeName = "Hydra_Mk5",
            connector_name = "tube_11"
          }, {
            ShapeName = "Hydra_Mk5",
            connector_name = "tube_12"
          }, {
            ShapeName = "Hydra_Mk5",
            connector_name = "tube_13"
          }, {
            ShapeName = "Hydra_Mk5",
            connector_name = "tube_14"
          }, {
            ShapeName = "Hydra_Mk5",
            connector_name = "tube_15"
          }, {
            ShapeName = "Hydra_Mk5",
            connector_name = "tube_16"
          }, {
            ShapeName = "Hydra_Mk5",
            connector_name = "tube_17"
          }, {
            ShapeName = "Hydra_Mk5",
            connector_name = "tube_18"
          }, {
            ShapeName = "Hydra_Mk5",
            connector_name = "tube_19"
          } },
        Picture = "LAU61.png",
        Weight = 234,
        attribute = { 4, 7, 32, 185 },
        displayName = "LAU-3 - 19 2.75' rockets MK5 HEAT",
        wsTypeOfWeapon = { 4, 7, 33, 145 }
      }, {
        CLSID = "LAU3_HE151",
        Count = 19,
        Cx_pil = 0.00146484375,
        Elements = { {
            IsAdapter = true,
            ShapeName = "LAU-3"
          }, {
            ShapeName = "Hydra_M151",
            connector_name = "tube_1"
          }, {
            ShapeName = "Hydra_M151",
            connector_name = "tube_2"
          }, {
            ShapeName = "Hydra_M151",
            connector_name = "tube_3"
          }, {
            ShapeName = "Hydra_M151",
            connector_name = "tube_4"
          }, {
            ShapeName = "Hydra_M151",
            connector_name = "tube_5"
          }, {
            ShapeName = "Hydra_M151",
            connector_name = "tube_6"
          }, {
            ShapeName = "Hydra_M151",
            connector_name = "tube_7"
          }, {
            ShapeName = "Hydra_M151",
            connector_name = "tube_8"
          }, {
            ShapeName = "Hydra_M151",
            connector_name = "tube_9"
          }, {
            ShapeName = "Hydra_M151",
            connector_name = "tube_10"
          }, {
            ShapeName = "Hydra_M151",
            connector_name = "tube_11"
          }, {
            ShapeName = "Hydra_M151",
            connector_name = "tube_12"
          }, {
            ShapeName = "Hydra_M151",
            connector_name = "tube_13"
          }, {
            ShapeName = "Hydra_M151",
            connector_name = "tube_14"
          }, {
            ShapeName = "Hydra_M151",
            connector_name = "tube_15"
          }, {
            ShapeName = "Hydra_M151",
            connector_name = "tube_16"
          }, {
            ShapeName = "Hydra_M151",
            connector_name = "tube_17"
          }, {
            ShapeName = "Hydra_M151",
            connector_name = "tube_18"
          }, {
            ShapeName = "Hydra_M151",
            connector_name = "tube_19"
          } },
        Picture = "LAU61.png",
        Weight = 234,
        attribute = { 4, 7, 32, 186 },
        displayName = "LAU-3 - 19 2.75' rockets MK151 HE",
        wsTypeOfWeapon = { 4, 7, 33, 147 }
      }, {
        CLSID = "{TWIN_S25}",
        Count = 2,
        Cx_pil = 0.0004,
        Elements = { {
            IsAdapter = true,
            ShapeName = "su-27-twinpylon"
          }, {
            DrawArgs = { { 3, 0.5 } },
            connector_name = "S-25-L",
            payload_CLSID = "{A0648264-4BC0-4EE8-A543-D119F6BA4257}"
          }, {
            DrawArgs = { { 3, 0.5 } },
            connector_name = "S-25-R",
            payload_CLSID = "{A0648264-4BC0-4EE8-A543-D119F6BA4257}"
          } },
        JettisonSubmunitionOnly = true,
        Picture = "S25.png",
        Weight = 902,
        attribute = { 4, 7, 32, 181 },
        displayName = "2xS-25 OFM",
        wsTypeOfWeapon = { 4, 7, 33, 35 }
      }, {
        CLSID = "{HVAR}",
        Count = 1,
        Cx_pil = 0.0004,
        Elements = { {
            LocalOffsets = { 10, 10, 10 },
            ShapeName = "HVAR_rocket"
          } },
        Picture = "HVAR.png",
        Weight = 64,
        attribute = { 4, 7, 33, 159 },
        displayName = "HVAR"
      } },
    Name = "ROCKETS"
  }, {
    CLSID = "{3B8A5D2A-DD92-4776-BE4A-79C3EFB360EE}",
    DisplayName = "AIR-TO-AIR",
    Launchers = { {
        CLSID = "{4EDBA993-2E34-444C-95FB-549300BF7CAF}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "R-40R"
          } },
        NatoName = "(AA-6)",
        Picture = "R40R.png",
        Weight = 475,
        attribute = { 4, 4, 7, 7 },
        displayName = "R-40R"
      }, {
        CLSID = "{6980735A-44CC-4BB9-A1B5-591532F1DC69}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "R-24T"
          } },
        NatoName = "(AA-7)",
        Picture = "r24t.png",
        Weight = 215,
        attribute = { 4, 4, 7, 26 },
        displayName = "R-24T"
      }, {
        CLSID = "{CCF898C9-5BC7-49A4-9D1E-C3ED3D5166A1}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "R-24R"
          } },
        NatoName = "(AA-7)",
        Picture = "r24r.png",
        Weight = 215,
        attribute = { 4, 4, 7, 9 },
        displayName = "R-24R"
      }, {
        CLSID = "{9B25D316-0434-4954-868F-D51DB1A38DF0}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "R-27R"
          } },
        NatoName = "(AA-10)",
        Picture = "r27r.png",
        Weight = 253,
        attribute = { 4, 4, 7, 13 },
        displayName = "R-27R"
      }, {
        CLSID = "{E8069896-8435-4B90-95C0-01A03AE6E400}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "R-27ER"
          } },
        NatoName = "(AA-10)",
        Picture = "r27erem.png",
        Weight = 350,
        attribute = { 4, 4, 7, 14 },
        displayName = "R-27ER"
      }, {
        CLSID = "{88DAC840-9F75-4531-8689-B46E64E42E53}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "R-27T"
          } },
        NatoName = "(AA-10)",
        Picture = "r27t.png",
        Weight = 254,
        attribute = { 4, 4, 7, 15 },
        displayName = "R-27T"
      }, {
        CLSID = "{B79C379A-9E87-4E50-A1EE-7F7E29C2E87A}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "R-27ET"
          } },
        NatoName = "(AA-10)",
        Picture = "r27et.png",
        Weight = 343,
        attribute = { 4, 4, 7, 16 },
        displayName = "R-27ET"
      }, {
        CLSID = "{F1243568-8EF0-49D4-9CB5-4DA90D92BC1D}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "R-33"
          } },
        NatoName = "(AA-9)",
        Picture = "r33.png",
        Weight = 490,
        attribute = { 4, 4, 7, 11 },
        displayName = "R-33"
      }, {
        CLSID = "{5F26DBC2-FB43-4153-92DE-6BBCE26CB0FF}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "R-40T"
          } },
        NatoName = "(AA-6)",
        Picture = "R40T.png",
        Weight = 475,
        attribute = { 4, 4, 7, 27 },
        displayName = "R-40T"
      }, {
        CLSID = "{682A481F-0CB5-4693-A382-D00DD4A156D7}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "R-60"
          } },
        NatoName = "(AA-8)",
        Picture = "r60.png",
        Weight = 44,
        attribute = { 4, 4, 7, 10 },
        displayName = "R-60M"
      }, {
        CLSID = "{APU-60-1_R_60M}",
        Count = 1,
        Elements = { {
            IsAdapter = true,
            ShapeName = "APU-60-1"
          }, {
            Position = { 0.527352, -0.155526, 0 },
            ShapeName = "R-60"
          } },
        NatoName = "(AA-8)",
        Picture = "r60.png",
        Weight = 76,
        attribute = { 4, 4, 32, 181 },
        displayName = "APU-60-1 R-60M",
        wsTypeOfWeapon = { 4, 4, 7, 10 }
      }, {
        CLSID = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "R-73"
          } },
        NatoName = "(AA-11)",
        Picture = "r73.png",
        Weight = 110,
        attribute = { 4, 4, 7, 18 },
        displayName = "R-73"
      }, {
        CLSID = "{CBC29BFE-3D24-4C64-B81D-941239D12249}",
        Count = 1,
        Cx_pil = 0.001,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "APU-73"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, -0.178, 0 },
            ShapeName = "R-73"
          } },
        NatoName = "(AA-11)",
        Picture = "r73.png",
        Weight = 110,
        attribute = { 4, 4, 32, 102 },
        displayName = "R-73",
        wsTypeOfWeapon = { 4, 4, 7, 18 }
      }, {
        CLSID = "{B4C01D60-A8A3-4237-BD72-CA7655BC0FE9}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "R-77"
          } },
        NatoName = "(AA-12)",
        Picture = "R77.png",
        Weight = 175,
        attribute = { 4, 4, 7, 19 },
        displayName = "R-77"
      }, {
        CLSID = "{B4C01D60-A8A3-4237-BD72-CA7655BC0FEC}",
        Count = 1,
        Cx_pil = 0.001,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "APU-170"
          }, {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, -0.224, 0 },
            ShapeName = "R-77"
          } },
        NatoName = "(AA-12)",
        Picture = "R77.png",
        Required = { "{F4920E62-A99A-11d8-9897-000476191836}" },
        Weight = 250,
        attribute = { 4, 4, 32, 103 },
        displayName = "R-77",
        wsTypeOfWeapon = { 4, 4, 7, 19 }
      }, {
        CLSID = "{FC23864E-3B80-48E3-9C03-4DA8B1D7497B}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "MAGIC-R550"
          } },
        Picture = "r60.png",
        Weight = 89,
        attribute = { 4, 4, 7, 1 },
        displayName = "R.550 Magic 2"
      }, {
        CLSID = "{0DA03783-61E4-40B2-8FAE-6AEE0A5C5AAE}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "MICA-T"
          } },
        Picture = "micair.png",
        Weight = 110,
        attribute = { 4, 4, 7, 2 },
        displayName = "MICA IR"
      }, {
        CLSID = "{6D778860-7BB8-4ACB-9E95-BA772C6BBC2C}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "MICA-R"
          } },
        Picture = "micarf.png",
        Weight = 110,
        attribute = { 4, 4, 7, 3 },
        displayName = "MICA RF"
      }, {
        CLSID = "{FD21B13E-57F3-4C2A-9F78-C522D0B5BCE1}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "SUPER-530"
          } },
        Picture = "super530.png",
        Weight = 270,
        attribute = { 4, 4, 7, 4 },
        displayName = "Super 530D"
      }, {
        CLSID = "{8D399DDA-FF81-4F14-904D-099B34FE7918}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "AIM-7"
          } },
        Picture = "aim7m.png",
        Weight = 230,
        attribute = { 4, 4, 7, 21 },
        displayName = "AIM-7M"
      }, {
        CLSID = "{7575BA0B-7294-4844-857B-031A144B2595}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 } },
            Position = { 0, 0, 0 },
            ShapeName = "AIM-54"
          } },
        Picture = "aim54.png",
        Weight = 463,
        attribute = { 4, 4, 7, 23 },
        displayName = "AIM-54C"
      }, {
        CLSID = "{C8E06185-7CD6-4C90-959F-044679E90751}",
        Elements = { {
            DrawArgs = { { 1, 1 }, { 2, 1 }, { 3, 0 } },
            Position = { 0, 0, 0 },
            ShapeName = "AIM-120B"
          } },
        Picture = "aim120.png",
        Weight = 157.8,
        attribute = { 4, 4, 7, 24 },
        displayName = "AIM-120B"
      }, {
        CLSID = "{B0DBC591-0F52-4F7D-AD7B-51E67725FB81}",
        Count = 2,
        Cx_pil = 0.0006,
        Elements = { {
            ShapeName = "apu-60-2_L"
          }, {
            Position = { 0.48914, -0.301191, 0 },
            ShapeName = "R-60"
          }, {
            Position = { 0.48914, -0.094083, -0.151469 },
            Rotation = { 90, 0, 0 },
            ShapeName = "R-60"
          } },
        Picture = "r60.png",
        Weight = 148,
        attribute = { 4, 4, 32, 62 },
        displayName = "R-60M*2",
        wsTypeOfWeapon = { 4, 4, 7, 10 }
      }, {
        CLSID = "{275A2855-4A79-4B2D-B082-91EA2ADF4691}",
        Count = 2,
        Cx_pil = 0.0006,
        Elements = { {
            ShapeName = "apu-60-2_R"
          }, {
            Position = { 0.48914, -0.301191, 0 },
            ShapeName = "R-60"
          }, {
            Position = { 0.48914, -0.094083, 0.151469 },
            Rotation = { -90, 0, 0 },
            ShapeName = "R-60"
          } },
        Picture = "r60.png",
        Weight = 148,
        attribute = { 4, 4, 32, 63 },
        displayName = "R-60M*2",
        wsTypeOfWeapon = { 4, 4, 7, 10 }
      }, {
        CLSID = "{40EF17B7-F508-45de-8566-6FFECC0C1AB8}",
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "AIM-120C"
          } },
        Picture = "aim120c.png",
        Weight = 161.5,
        attribute = { 4, 4, 7, 106 },
        displayName = "AIM-120C"
      } },
    Name = "AIR-TO-AIR"
  }, {
    CLSID = "{859F6AD7-FCA4-45b8-A470-D3938EC33BFC}",
    DisplayName = "FUEL TANKS",
    Launchers = { {
        CLSID = "{414DA830-B61A-4F9E-B71B-C2F6832E1D7A}",
        Cx_pil = 0.002685547,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "M2000-PTB"
          } },
        Picture = "PTB.png",
        Weight = 1050,
        Weight_Empty = 50,
        attribute = { 1, 3, 43, 39 },
        displayName = "M2000 Fuel tank"
      }, {
        CLSID = "{EF124821-F9BB-4314-A153-E0E2FE1162C4}",
        Cx_pil = 0.002199707,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "T-PTB"
          } },
        Picture = "PTB.png",
        Weight = 1275,
        Weight_Empty = 50,
        attribute = { 1, 3, 43, 38 },
        displayName = "TORNADO Fuel tank"
      }, {
        CLSID = "{0395076D-2F77-4420-9D33-087A4398130B}",
        Cx_pil = 0.001953125,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "PTB-275"
          } },
        Picture = "PTB.png",
        Weight = 909,
        Weight_Empty = 104,
        attribute = { 1, 3, 43, 36 },
        displayName = "F-5 275Gal Fuel tank"
      }, {
        CLSID = "{7B4B122D-C12C-4DB4-834E-4D8BB4D863A8}",
        Cx_pil = 0.002197266,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "F4-BAK-L"
          } },
        Picture = "PTB.png",
        Weight = 1420,
        Weight_Empty = 50,
        attribute = { 1, 3, 43, 41 },
        displayName = "F-4 Fuel tank-W"
      }, {
        CLSID = "{8B9E3FD0-F034-4A07-B6CE-C269884CC71B}",
        Cx_pil = 0.003173828,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "F4-BAK-C"
          } },
        Picture = "PTB.png",
        Weight = 2345,
        Weight_Empty = 50,
        attribute = { 1, 3, 43, 42 },
        displayName = "F-4 Fuel tank-C"
      }, {
        CLSID = "{2BEC576B-CDF5-4B7F-961F-B0FA4312B841}",
        Cx_pil = 0.002199707,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "PTB-1500"
          } },
        Picture = "PTB.png",
        Weight = 1262.5,
        Weight_Empty = 100,
        attribute = { 1, 3, 43, 17 },
        displayName = "Fuel tank 1400L"
      }, {
        CLSID = "{414E383A-59EB-41BC-8566-2B5E0788ED1F}",
        Cx_pil = 0.002131348,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "PTB-1150"
          } },
        Picture = "PTB.png",
        Weight = 975.25,
        Weight_Empty = 84,
        attribute = { 1, 3, 43, 16 },
        displayName = "Fuel tank 1150L"
      }, {
        CLSID = "{C0FF4842-FBAC-11d5-9190-00A0249B6F00}",
        Cx_pil = 0.001479492,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "PTB-1150-29"
          } },
        Picture = "PTB.png",
        Weight = 975.25,
        Weight_Empty = 84,
        attribute = { 1, 3, 43, 61 },
        displayName = "Fuel tank 1150L MiG-29"
      }, {
        CLSID = "{A5BAEAB7-6FAF-4236-AF72-0FD900F493F9}",
        Cx_pil = 0.001208496,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "MIG-23-PTB"
          } },
        Picture = "PTB.png",
        Weight = 680,
        Weight_Empty = 140,
        attribute = { 1, 3, 43, 14 },
        displayName = "Fuel tank 800L"
      }, {
        CLSID = "{F376DBEE-4CAE-41BA-ADD9-B2910AC95DEC}",
        Cx_pil = 0.003173828,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "F-16-PTB-N1"
          } },
        Picture = "PTB.png",
        Weight = 1170.36,
        Weight_Empty = 50,
        attribute = { 1, 3, 43, 11 },
        displayName = "Fuel tank 370 gal"
      }, {
        CLSID = "{82364E69-5564-4043-A866-E13032926C3E}",
        Cx_pil = 0.002197266,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "F-16-PTB-N2"
          } },
        Picture = "PTB.png",
        Weight = 1161.276,
        Weight_Empty = 50,
        attribute = { 1, 3, 43, 56 },
        displayName = "Fuel tank 367 gal"
      }, {
        CLSID = "{8A0BE8AE-58D4-4572-9263-3144C0D06364}",
        Cx_pil = 0.002197266,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "F-16-PTB-N2"
          } },
        Picture = "PTB.png",
        Weight = 958.4,
        Weight_Empty = 50,
        attribute = { 1, 3, 43, 12 },
        displayName = "Fuel tank 300 gal"
      }, {
        CLSID = "{0855A3A1-FA50-4C89-BDBB-5D5360ABA071}",
        Cx_pil = 0.002346191,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "MIG-25-PTB"
          } },
        Picture = "PTB.png",
        Weight = 4420,
        Weight_Empty = 50,
        attribute = { 1, 3, 43, 15 },
        displayName = "Fuel tank 5000L"
      }, {
        CLSID = "{E1F29B21-F291-4589-9FD8-3272EEC69506}",
        Cx_pil = 0.002443848,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "F15-PTB"
          } },
        Picture = "PTB.png",
        Weight = 1897.08,
        Weight_Empty = 50,
        attribute = { 1, 3, 43, 10 },
        displayName = "Fuel tank 610 gal"
      }, {
        CLSID = "{7D7EC917-05F6-49D4-8045-61FC587DD019}",
        Cx_pil = 0.002255859,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "PTB-3000"
          } },
        Picture = "PTB.png",
        Weight = 2550,
        Weight_Empty = 50,
        attribute = { 1, 3, 43, 5 },
        displayName = "Fuel tank 3000L"
      }, {
        CLSID = "{16602053-4A12-40A2-B214-AB60D481B20E}",
        Cx_pil = 0.002255859,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "PTB-2000"
          } },
        Picture = "PTB.png",
        Weight = 1700,
        Weight_Empty = 50,
        attribute = { 1, 3, 43, 53 },
        displayName = "Fuel tank 2000L"
      }, {
        CLSID = "{E8D4652F-FD48-45B7-BA5B-2AE05BB5A9CF}",
        Cx_pil = 0.001208496,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "PTB-800"
          } },
        Picture = "PTB.png",
        Weight = 760,
        Weight_Empty = 140,
        attribute = { 1, 3, 43, 54 },
        displayName = "Fuel tank 800L Wing"
      }, {
        CLSID = "{A504D93B-4E80-4B4F-A533-0D9B65F2C55F}",
        Cx_pil = 0.002197266,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "S-3-PTB"
          } },
        Picture = "PTB.png",
        Weight = 964,
        Weight_Empty = 50,
        attribute = { 1, 3, 43, 55 },
        displayName = "Fuel tank S-3"
      }, {
        CLSID = "{B99EE8A8-99BC-4a8d-89AC-A26831920DCE}",
        Cx_pil = 0.001464844,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "PTB-KA-50"
          } },
        Picture = "PTB.png",
        Weight = 550,
        Weight_Empty = 110,
        attribute = { 1, 3, 43, 99 },
        displayName = "Fuel tank Ka-50"
      }, {
        CLSID = "Fuel_Tank_FT600",
        Cx_pil = 0.000959473,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "FT600"
          } },
        Picture = "PTB.png",
        Weight = 1925,
        Weight_Empty = 110,
        attribute = { 1, 3, 43, 103 },
        displayName = "Fuel tank FT600"
      }, {
        CLSID = "{PTB-150GAL}",
        Cx_pil = 0.001953125,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "PTB-150"
          } },
        Picture = "PTB2.png",
        Weight = 509,
        Weight_Empty = 67,
        attribute = { 1, 3, 43, 107 },
        displayName = "F-5 150Gal Fuel tank"
      }, {
        CLSID = "{DT75GAL}",
        Cx_pil = 0.0015,
        Picture = "PTB.png",
        Weight = 227.048087675,
        Weight_Empty = 20,
        attribute = { 1, 3, 43, 152 },
        displayName = "Drop Tank 75Gal"
      }, {
        CLSID = "{EFEC8200-B922-11d7-9897-000476191836}",
        Cx_pil = 0.002685547,
        Picture = "PTB.png",
        Weight = 1049.24,
        Weight_Empty = 50,
        attribute = { 1, 3, 43, 13 },
        displayName = "Fuel tank 330 gal"
      }, {
        CLSID = "{EFEC8201-B922-11d7-9897-000476191836}",
        Cx_pil = 0.002685547,
        Picture = "PTB.png",
        Weight = 1049.24,
        Weight_Empty = 50,
        attribute = { 1, 3, 43, 13 },
        displayName = "Fuel tank 330 gal"
      } },
    Name = "FUEL TANKS"
  }, {
    CLSID = "{0A5EE67D-3F2B-4cd3-8A15-26211CF19737}",
    DisplayName = "PODS",
    Launchers = { {
        CLSID = "{6D21ECEA-F85B-4E8D-9D51-31DC9B8AA4EF}",
        Cx_pil = 0.00083740234375,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "ALQ-131"
          } },
        Picture = "ALQ131.png",
        Weight = 305,
        attribute = { 4, 15, 45, 25 },
        displayName = "ALQ-131"
      }, {
        CLSID = "ALQ_184",
        Cx_pil = 0.00070256637315,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "ALQ-184"
          } },
        Picture = "ALQ184.png",
        Weight = 215,
        attribute = { 4, 15, 45, 142 },
        displayName = "ALQ-184"
      }, {
        CLSID = "{F75187EF-1D9E-4DA9-84B4-1A1A14A3973A}",
        Cx_pil = 0.000244140625,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "SPS-141"
          } },
        Picture = "SPS141.png",
        Weight = 150,
        attribute = { 4, 15, 45, 30 },
        displayName = "SPS-141"
      }, {
        CLSID = "{44EE8698-89F9-48EE-AF36-5FD31896A82F}",
        Cx_pil = 0.000244140625,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "SORBCIJA_L"
          } },
        Picture = "L005.png",
        Required = { "{44EE8698-89F9-48EE-AF36-5FD31896A82A}" },
        Weight = 150,
        attribute = { 4, 15, 45, 29 },
        displayName = "L005 Sorbtsiya ECM pod (left)"
      }, {
        CLSID = "{44EE8698-89F9-48EE-AF36-5FD31896A82A}",
        Cx_pil = 0.000244140625,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "SORBCIJA_R"
          } },
        Picture = "L005.png",
        Required = { "{44EE8698-89F9-48EE-AF36-5FD31896A82F}" },
        Weight = 150,
        attribute = { 4, 15, 45, 173 },
        displayName = "L005 Sorbtsiya ECM pod (right)"
      }, {
        CLSID = "{44EE8698-89F9-48EE-AF36-5FD31896A82D}",
        Cx_pil = 0.000244140625,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "MPS-410"
          } },
        Picture = "MPS410.png",
        Required = { "{44EE8698-89F9-48EE-AF36-5FD31896A82C}" },
        Weight = 150,
        attribute = { 4, 15, 45, 94 },
        displayName = "MPS-410"
      }, {
        CLSID = "{44EE8698-89F9-48EE-AF36-5FD31896A82C}",
        Cx_pil = 0.000244140625,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "MPS-410"
          } },
        Picture = "MPS410.png",
        Required = { "{44EE8698-89F9-48EE-AF36-5FD31896A82D}" },
        Weight = 150,
        attribute = { 4, 15, 45, 94 },
        displayName = "MPS-410"
      }, {
        CLSID = "{CAAC1CFD-6745-416B-AFA4-CB57414856D0}",
        Cx_pil = 0.000244140625,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "LANTIRN"
          } },
        Picture = "Lantirn.png",
        Weight = 445,
        attribute = { 4, 15, 44, 26 },
        displayName = "Lantirn F-16"
      }, {
        CLSID = "{D1744B93-2A8A-4C4D-B004-7A09CD8C8F3F}",
        Cx_pil = 0.000244140625,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "LANTIRN-F14-TARGET"
          } },
        Picture = "Lantirn.png",
        Weight = 200,
        attribute = { 4, 15, 44, 59 },
        displayName = "Lantirn Target Pod"
      }, {
        CLSID = "{199D6D51-1764-497E-9AE5-7D07C8D4D87E}",
        Cx_pil = 0.000244140625,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "PAVETACK"
          } },
        Picture = "L005.png",
        Weight = 200,
        attribute = { 4, 15, 44, 28 },
        displayName = "Pavetack F-111"
      }, {
        CLSID = "{8C3F26A1-FA0F-11d5-9190-00A0249B6F00}",
        Cx_pil = 0.000244140625,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "BOZ-100"
          } },
        Picture = "BOZ107.png",
        Weight = 200,
        attribute = { 4, 15, 48, 58 },
        displayName = "BOZ-107"
      }, {
        CLSID = "{8C3F26A2-FA0F-11d5-9190-00A0249B6F00}",
        Cx_pil = 0.000244140625,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "SKY_SHADOW"
          } },
        Picture = "skyshadow.png",
        Weight = 200,
        attribute = { 4, 15, 45, 37 },
        displayName = "Sky-Shadow ECM Pod"
      }, {
        CLSID = "{B1EF6B0E-3D91-4047-A7A5-A99E7D8B4A8B}",
        Cx_pil = 0.000244140625,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "KINGAL"
          } },
        Picture = "Mercury.png",
        Weight = 230,
        attribute = { 4, 15, 44, 19 },
        displayName = "Mercury LLTV Pod"
      }, {
        CLSID = "{0519A261-0AB6-11d6-9193-00A0249B6F00}",
        Cx_pil = 0.000244140625,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "ETHER"
          } },
        Picture = "ether.png",
        Weight = 200,
        attribute = { 4, 15, 44, 63 },
        displayName = "ETHER"
      }, {
        CLSID = "{0519A262-0AB6-11d6-9193-00A0249B6F00}",
        Cx_pil = 0.000244140625,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "TANGAZH"
          } },
        Picture = "L005.png",
        Weight = 200,
        attribute = { 4, 15, 44, 62 },
        displayName = "Tangazh ELINT pod"
      }, {
        CLSID = "{0519A263-0AB6-11d6-9193-00A0249B6F00}",
        Cx_pil = 0.000244140625,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "SHPIL"
          } },
        Picture = "Shpil.png",
        Weight = 200,
        attribute = { 4, 15, 44, 64 },
        displayName = "Shpil-2M Laser Intelligence Pod"
      }, {
        CLSID = "{0519A264-0AB6-11d6-9193-00A0249B6F00}",
        Cx_pil = 0.00143798828125,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "Fantasm"
          } },
        Picture = "L081.png",
        Weight = 300,
        attribute = { 4, 15, 44, 65 },
        displayName = "L-081 Fantasmagoria ELINT pod"
      }, {
        CLSID = "{D3F65166-1AB8-490f-AF2F-2FB6E22568B1}",
        Cx_pil = 0.00048828125,
        Elements = { {
            ShapeName = "R-73U"
          } },
        Picture = "smoke.png",
        PictureBlendColor = "0xf52828ff",
        Smoke = {
          alpha = 180,
          b = 40,
          dx = -1.455,
          dy = -0.062,
          g = 40,
          r = 245
        },
        Weight = 220,
        attribute = { 4, 15, 50, 66 },
        displayName = "Smoke Generator - red_smk"
      }, {
        CLSID = "{D3F65166-1AB8-490f-AF2F-2FB6E22568B2}",
        Cx_pil = 0.00048828125,
        Elements = { {
            ShapeName = "R-73U"
          } },
        Picture = "smoke.png",
        PictureBlendColor = "0x32a064ff",
        Smoke = {
          alpha = 180,
          b = 100,
          dx = -1.455,
          dy = -0.062,
          g = 160,
          r = 50
        },
        Weight = 220,
        attribute = { 4, 15, 50, 82 },
        displayName = "Smoke Generator - green"
      }, {
        CLSID = "{D3F65166-1AB8-490f-AF2F-2FB6E22568B3}",
        Cx_pil = 0.00048828125,
        Elements = { {
            ShapeName = "R-73U"
          } },
        Picture = "smoke.png",
        PictureBlendColor = "0x3264d2ff",
        Smoke = {
          alpha = 180,
          b = 210,
          dx = -1.455,
          dy = -0.062,
          g = 100,
          r = 50
        },
        Weight = 220,
        attribute = { 4, 15, 50, 83 },
        displayName = "Smoke Generator - blue_smk"
      }, {
        CLSID = "{D3F65166-1AB8-490f-AF2F-2FB6E22568B4}",
        Cx_pil = 0.00048828125,
        Elements = { {
            ShapeName = "R-73U"
          } },
        Picture = "smoke.png",
        PictureBlendColor = "0xffffffff",
        Smoke = {
          alpha = 180,
          b = 255,
          dx = -1.455,
          dy = -0.062,
          g = 255,
          r = 255
        },
        Weight = 220,
        attribute = { 4, 15, 50, 84 },
        displayName = "Smoke Generator - white"
      }, {
        CLSID = "{D3F65166-1AB8-490f-AF2F-2FB6E22568B5}",
        Cx_pil = 0.00048828125,
        Elements = { {
            ShapeName = "R-73U"
          } },
        Picture = "smoke.png",
        PictureBlendColor = "0xffe632ff",
        Smoke = {
          alpha = 180,
          b = 50,
          dx = -1.455,
          dy = -0.062,
          g = 230,
          r = 255
        },
        Weight = 220,
        attribute = { 4, 15, 50, 85 },
        displayName = "Smoke Generator - yellow"
      }, {
        CLSID = "{D3F65166-1AB8-490f-AF2F-2FB6E22568B6}",
        Cx_pil = 0.00048828125,
        Elements = { {
            ShapeName = "R-73U"
          } },
        Picture = "smoke.png",
        PictureBlendColor = "0xff9623ff",
        Smoke = {
          alpha = 180,
          b = 35,
          dx = -1.455,
          dy = -0.062,
          g = 150,
          r = 255
        },
        Weight = 220,
        attribute = { 4, 15, 50, 86 },
        displayName = "Smoke Generator - orange"
      }, {
        CLSID = "{A4BCC903-06C8-47bb-9937-A30FEDB4E741}",
        Cx_pil = 0.00048828125,
        Elements = { {
            ShapeName = "AIM-9S"
          } },
        Picture = "smoke.png",
        PictureBlendColor = "0xf52828ff",
        Smoke = {
          alpha = 180,
          b = 40,
          dx = -2,
          dy = -0.09,
          g = 40,
          r = 245
        },
        Weight = 200,
        attribute = { 4, 15, 50, 67 },
        displayName = "Smokewinder - red_smk"
      }, {
        CLSID = "{A4BCC903-06C8-47bb-9937-A30FEDB4E742}",
        Cx_pil = 0.00048828125,
        Elements = { {
            ShapeName = "AIM-9S"
          } },
        Picture = "smoke.png",
        PictureBlendColor = "0x32a064ff",
        Smoke = {
          alpha = 180,
          b = 100,
          dx = -2,
          dy = -0.09,
          g = 160,
          r = 50
        },
        Weight = 200,
        attribute = { 4, 15, 50, 87 },
        displayName = "Smokewinder - green"
      }, {
        CLSID = "{A4BCC903-06C8-47bb-9937-A30FEDB4E743}",
        Cx_pil = 0.00048828125,
        Elements = { {
            ShapeName = "AIM-9S"
          } },
        Picture = "smoke.png",
        PictureBlendColor = "0x3264d2ff",
        Smoke = {
          alpha = 180,
          b = 210,
          dx = -2,
          dy = -0.09,
          g = 100,
          r = 50
        },
        Weight = 200,
        attribute = { 4, 15, 50, 88 },
        displayName = "Smokewinder - blue_smk"
      }, {
        CLSID = "{A4BCC903-06C8-47bb-9937-A30FEDB4E744}",
        Cx_pil = 0.00048828125,
        Elements = { {
            ShapeName = "AIM-9S"
          } },
        Picture = "smoke.png",
        PictureBlendColor = "0xffffffff",
        Smoke = {
          alpha = 180,
          b = 255,
          dx = -2,
          dy = -0.09,
          g = 255,
          r = 255
        },
        Weight = 200,
        attribute = { 4, 15, 50, 89 },
        displayName = "Smokewinder - white"
      }, {
        CLSID = "{A4BCC903-06C8-47bb-9937-A30FEDB4E745}",
        Cx_pil = 0.00048828125,
        Elements = { {
            ShapeName = "AIM-9S"
          } },
        Picture = "smoke.png",
        PictureBlendColor = "0xffe632ff",
        Smoke = {
          alpha = 180,
          b = 50,
          dx = -2,
          dy = -0.09,
          g = 230,
          r = 255
        },
        Weight = 200,
        attribute = { 4, 15, 50, 90 },
        displayName = "Smokewinder - yellow"
      }, {
        CLSID = "{A4BCC903-06C8-47bb-9937-A30FEDB4E746}",
        Cx_pil = 0.00048828125,
        Elements = { {
            ShapeName = "AIM-9S"
          } },
        Picture = "smoke.png",
        PictureBlendColor = "0xff9623ff",
        Smoke = {
          alpha = 180,
          b = 35,
          dx = -2,
          dy = -0.09,
          g = 150,
          r = 255
        },
        Weight = 200,
        attribute = { 4, 15, 50, 91 },
        displayName = "Smokewinder - orange"
      }, {
        CLSID = "{INV-SMOKE-RED}",
        Cx_pil = 0,
        Picture = "smoke.png",
        PictureBlendColor = "0xf52828ff",
        Smoke = {
          alpha = 180,
          b = 40,
          dx = -1.455,
          dy = -0.062,
          g = 40,
          r = 245
        },
        Weight = 0,
        Weight_Empty = 0,
        attribute = { 4, 15, 50, 66 },
        displayName = "Smoke Generator - red_smk"
      }, {
        CLSID = "{INV-SMOKE-GREEN}",
        Cx_pil = 0,
        Picture = "smoke.png",
        PictureBlendColor = "0x32a064ff",
        Smoke = {
          alpha = 180,
          b = 100,
          dx = -1.455,
          dy = -0.062,
          g = 160,
          r = 50
        },
        Weight = 0,
        Weight_Empty = 0,
        attribute = { 4, 15, 50, 82 },
        displayName = "Smoke Generator - green"
      }, {
        CLSID = "{INV-SMOKE-BLUE}",
        Cx_pil = 0,
        Picture = "smoke.png",
        PictureBlendColor = "0x3264d2ff",
        Smoke = {
          alpha = 180,
          b = 210,
          dx = -1.455,
          dy = -0.062,
          g = 100,
          r = 50
        },
        Weight = 0,
        Weight_Empty = 0,
        attribute = { 4, 15, 50, 83 },
        displayName = "Smoke Generator - blue_smk"
      }, {
        CLSID = "{INV-SMOKE-WHITE}",
        Cx_pil = 0,
        Picture = "smoke.png",
        PictureBlendColor = "0xffffffff",
        Smoke = {
          alpha = 180,
          b = 255,
          dx = -1.455,
          dy = -0.062,
          g = 255,
          r = 255
        },
        Weight = 0,
        Weight_Empty = 0,
        attribute = { 4, 15, 50, 84 },
        displayName = "Smoke Generator - white"
      }, {
        CLSID = "{INV-SMOKE-YELLOW}",
        Cx_pil = 0,
        Picture = "smoke.png",
        PictureBlendColor = "0xffe632ff",
        Smoke = {
          alpha = 180,
          b = 50,
          dx = -1.455,
          dy = -0.062,
          g = 230,
          r = 255
        },
        Weight = 0,
        Weight_Empty = 0,
        attribute = { 4, 15, 50, 85 },
        displayName = "Smoke Generator - yellow"
      }, {
        CLSID = "{INV-SMOKE-ORANGE}",
        Cx_pil = 0,
        Picture = "smoke.png",
        PictureBlendColor = "0xff9623ff",
        Smoke = {
          alpha = 180,
          b = 35,
          dx = -1.455,
          dy = -0.062,
          g = 150,
          r = 255
        },
        Weight = 0,
        Weight_Empty = 0,
        attribute = { 4, 15, 50, 86 },
        displayName = "Smoke Generator - orange"
      }, {
        CLSID = "{6C0D552F-570B-42ff-9F6D-F10D9C1D4E1C}",
        Cx_pil = 0.000244140625,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "F-18-FLIR-POD"
          } },
        Picture = "ANAAS38.png",
        Weight = 200,
        attribute = { 4, 15, 44, 74 },
        displayName = "AN/AAS-38 FLIR"
      }, {
        CLSID = "{1C2B16EB-8EB0-43de-8788-8EBB2D70B8BC}",
        Cx_pil = 0.000244140625,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "F-18-LDT-POD"
          } },
        Picture = "ANASQ173.png",
        Weight = 250,
        attribute = { 4, 15, 44, 78 },
        displayName = "AN/ASQ-173 LST/SCAM"
      }, {
        CLSID = "{F4920E62-A99A-11d8-9897-000476191836}",
        Cx_pil = 0,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "Spear"
          } },
        Picture = "kopyo.png",
        Weight = 115,
        attribute = { 4, 15, 44, 95 },
        displayName = "Kopyo radar pod"
      }, {
        CLSID = "{A111396E-D3E8-4b9c-8AC9-2432489304D5}",
        Cx_pil = 0.000244140625,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "AAQ-28_LITENING"
          } },
        Picture = "AAQ-28.png",
        Weight = 300,
        attribute = { 4, 15, 44, 101 },
        displayName = "AN/AAQ-28 LITENING"
      }, {
        CLSID = "{05544F1A-C39C-466b-BC37-5BD1D52E57BB}",
        Cx_pil = 0.001220703125,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "UPK-23-250"
          } },
        Picture = "upk23250.png",
        Weight = 218,
        Weight_Empty = 78,
        attribute = { 4, 15, 46, 20 },
        displayName = "UPK-23-250",
        kind_of_shipping = 2,
        wsTypeOfWeapon = { 4, 6, 10, 134 }
      }, {
        CLSID = "{E92CBFE5-C153-11d8-9897-000476191836}",
        Cx_pil = 0.001220703125,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "SPPU-22"
          } },
        Picture = "SPPU22.png",
        Weight = 290,
        attribute = { 4, 15, 46, 18 },
        displayName = "SPPU-22-1 Gun pod",
        kind_of_shipping = 2,
        wsTypeOfWeapon = { 4, 6, 6, 134 }
      }, {
        CLSID = "oh-58-brauning",
        Cx_pil = 0.001220703125,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "OH-58D_Browning"
          } },
        Picture = "oh58brau.png",
        Weight = 290,
        attribute = { 4, 15, 46, 145 },
        displayName = "OH-58D Brauning",
        kind_of_shipping = 2
      }, {
        CLSID = "MXU-648-TP",
        Cx_pil = 0,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "MXU-648"
          } },
        Picture = "mxu-468.png",
        Weight = 300,
        attribute = { 4, 15, 47, 104 },
        displayName = "MXU-648 Travel Pod"
      }, {
        CLSID = "BRU-42_LS",
        Count = 0,
        Cx_pil = 0.002,
        Elements = { {
            ShapeName = "BRU-42_LS"
          } },
        Picture = "BRU-42LS.png",
        Weight = 65,
        attribute = { 4, 5, 32, 132 },
        displayName = "BRU-42LS",
        wsTypeOfWeapon = { 0, 0, 0, 0 }
      }, {
        CLSID = "LAU-105",
        Count = 0,
        Cx_pil = 0.002,
        Elements = { {
            ShapeName = "lau-105"
          } },
        Picture = "lau-105.png",
        Weight = 18,
        attribute = { 4, 4, 32, 133 },
        displayName = "LAU-105",
        wsTypeOfWeapon = { 0, 0, 0, 0 }
      }, {
        CLSID = "M134_L",
        Cx_pil = 0.001220703125,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "AB-212_m134_l"
          } },
        Picture = "M134.png",
        Weight = 146.4,
        Weight_Empty = 51.68,
        attribute = { 4, 15, 46, 160 },
        displayName = "M134 MiniGun Left",
        kind_of_shipping = 2,
        wsTypeOfWeapon = { 4, 6, 10 }
      }, {
        CLSID = "M134_R",
        Cx_pil = 0.001220703125,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "AB-212_m134_r"
          } },
        Picture = "M134.png",
        Weight = 146.4,
        Weight_Empty = 51.68,
        attribute = { 4, 15, 46, 161 },
        displayName = "M134 MiniGun Right",
        kind_of_shipping = 2,
        wsTypeOfWeapon = { 4, 6, 10 }
      }, {
        CLSID = "GUV_VOG",
        Cx_pil = 0.001220703125,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "GUV-7800_G"
          } },
        Picture = "guv_flame.png",
        Weight = 274,
        Weight_Empty = 140,
        attribute = { 4, 15, 46, 171 },
        displayName = "GUV AP-30",
        kind_of_shipping = 2,
        wsTypeOfWeapon = { 4, 6, 10 }
      }, {
        CLSID = "GUV_YakB_GSHP",
        Cx_pil = 0.001220703125,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "GUV-7800_M"
          } },
        Picture = "guv.png",
        Weight = 452,
        Weight_Empty = 140,
        attribute = { 4, 15, 46, 170 },
        displayName = "GUV YakB GSHP",
        kind_of_shipping = 2,
        wsTypeOfWeapon = { 4, 6, 10, 170 }
      }, {
        CLSID = "{HVAR_SMOKE_GENERATOR}",
        Cx_pil = 0.0004,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "hvar_SmokσGenerator"
          } },
        Picture = "HVAR_smoke.png",
        Smoke = {
          alpha = 180,
          b = 40,
          dx = -2,
          dy = 0,
          g = 40,
          r = 245
        },
        Weight = 64,
        attribute = { 4, 15, 50, 172 },
        displayName = "HVAR Smoke Generator"
      }, {
        CLSID = "M134_SIDE_L",
        Cx_pil = 0.001220703125,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "ab-212_m134gunner_l"
          } },
        Picture = "M134.png",
        Weight = 270.4,
        Weight_Empty = 175.68,
        attribute = { 4, 15, 46, 174 },
        displayName = "M134 MiniGun Left Door",
        kind_of_shipping = 2,
        wsTypeOfWeapon = { 4, 6, 10 }
      }, {
        CLSID = "M134_SIDE_R",
        Cx_pil = 0.001220703125,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "ab-212_m134gunner_r"
          } },
        Picture = "M134.png",
        Weight = 270.4,
        Weight_Empty = 175.68,
        attribute = { 4, 15, 46, 175 },
        displayName = "M134 MiniGun Right Door",
        kind_of_shipping = 2,
        wsTypeOfWeapon = { 4, 6, 10 }
      }, {
        CLSID = "M60_SIDE_L",
        Cx_pil = 0.001220703125,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "ab-212_m60gunner_l"
          } },
        Picture = "M60.png",
        Weight = 141.4,
        Weight_Empty = 134.4,
        attribute = { 4, 15, 46, 176 },
        displayName = "M60 Gun Left Door",
        kind_of_shipping = 2,
        wsTypeOfWeapon = { 4, 6, 10 }
      }, {
        CLSID = "M60_SIDE_R",
        Cx_pil = 0.001220703125,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "ab-212_m60gunner_r"
          } },
        Picture = "M60.png",
        Weight = 141.4,
        Weight_Empty = 134.4,
        attribute = { 4, 15, 46, 177 },
        displayName = "M60 Gun Right Door",
        kind_of_shipping = 2,
        wsTypeOfWeapon = { 4, 6, 10 }
      }, {
        CLSID = "KORD_12_7",
        Cx_pil = 0.001220703125,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "mi-8_gunner"
          } },
        Picture = "M60.png",
        Weight = 95,
        attribute = { 4, 15, 46, 183 },
        displayName = "KORD 12.7 Gun",
        kind_of_shipping = 2,
        wsTypeOfWeapon = { 4, 6, 10, 183 }
      }, {
        CLSID = "PKT_7_62",
        Cx_pil = 0.001220703125,
        Elements = { {
            Position = { 0, 0, 0 },
            ShapeName = "mi-8_gunner_b"
          } },
        Picture = "M60.png",
        Weight = 90,
        attribute = { 4, 15, 46, 184 },
        displayName = "PKT 7.62 Gun",
        kind_of_shipping = 2,
        wsTypeOfWeapon = { 4, 6, 10, 184 }
      } },
    Name = "PODS"
  },
  [10] = {
    DisplayName = "REMOVE PYLON",
    Launchers = { {
        CLSID = "<CLEAN>",
        Cx_pil = 0,
        Picture = "Weaponx.png",
        PictureBlendColor = "0xa0b4cdff",
        Weight = 0,
        Weight_Empty = 0,
        attribute = { 0, 0, 0, 0 },
        displayName = "Clean Wing"
      } },
    Name = "CLEAN"
  }
}
